module.exports = {
  "hello": {
    "hello": "Bonjour !"
  },
  "home": {
    "my_life_my_choice": "Ma Vie Mon Choix",
    "my_courses": "Mes parcours",
    "intro": "Bienvenue {0}! Choisis un thème; au fil des aventures de Assiba, Paulin et leurs amis, tu vas t’informer sur la santé sexuelle et reproductive en te divertissant. L’appli est gratuit!",
    "progress_message": "Chaque parcours comprend 4 topics. Vous avez déjà débloqué {0}% des contenus et obtenu {1} pièces d'or. Pas mal!"
  },
  "about": {
    "contentPartOne": "<p>Cette application, conçue par OneWorld avec les jeunes, donne des informations sur la santé sexuelle et le genre. Elle permet d’apprendre, de mieux connaître, et gérer sa sexualité.<br>" +
                      "Sur cette application, tu peux trouver:<br>" +
                      "<strong>•</strong> un parcours éducatif avec des leçons,<br>" +
                      "<strong>•</strong> des vidéos éducatives que tu peux partager avec tes amis,<br>" +
                      "<strong>•</strong> des quiz pour tester tes nouvelles connaissances,<br>" +
                      "<strong>•</strong> une carte sur les différents centres offrant des services conviviaux aux jeunes.</p>" +
                      "<p>Une fois que cette application est téléchargée, tu peux l’utiliser gratuitement et sans accès internet!</p>" +
                      "<h1>Bonus! Tro Tro Ga Ho! Un site mobile contre les violences faites aux femmes et aux filles</h1>",
    "contentPartTwo": "<p>Les créateurs de Ma Vie, Mon Choix vous proposent aussi le site mobile <a href='https://trotrogaho.com/?utm_source=mvmc-mobile-app&utm_medium=mobile-app&utm_campaign=trotrogaho'>Tro Tro Ga Ho</a>! (L’heure du changement a sonné).</p>" +
                      "<p>Ce site est destiné à <strong>tous ceux concernés ou touchés par les violences faites aux femmes et aux filles</strong>, qui veulent <strong>s’informer</strong> sur cette  thématique et <strong>aider</strong> à combattre ce fléau au Bénin.</p>" +
                      "<p><strong>Spécialement adapté aux mobiles mêmes les plus basiques</strong>, ainsi que les androides, ordinateurs ou tablettes - il a même été <strong>conçu pour consommer très peu de crédit</strong>!</p>" +
                      "<p>Sur <a href='https://trotrogaho.com/?utm_source=mvmc-mobile-app&utm_medium=mobile-app&utm_campaign=trotrogaho'>trotrogaho.com</a>, trouvez:<br>" +
                      "<strong>•</strong> Des <strong>informations</strong> sur les <strong>droits</strong> des femmes et des filles<br>" +
                      "<strong>•</strong> La liste des <strong>services</strong> disponibles aux victimes<br>" +
                      "<strong>•</strong> Des <strong>messages éducatifs</strong> à partager à vos contacts pour les sensibiliser<br>" +
                      "<strong>•</strong> Des <strong>outils</strong> développés par des sources fiables qui peuvent être utilisés lors des activités de sensibilisation</p>",
    "btnText": "Visiter trotrogaho.com*",
    "appExit": "* En cliquant tu vas sortir de  l’appli et ouvrir ton navigateur."
  },
  "demographic_data": {
    "mascot_welcome_message": "Remplissez toutes les informations pour commencer l'aventure!",
    "your_pseudo": "Pseudo",
    "your_age": "Âge",
    "your_gender": "Genre",
    "your_occupation": "Occupation",
    "your_department": "Département",
    "your_zone": "Dans quelle zone habitez-vous?",
    "your_speaking_language": "Quelle langue parlez-vous le mieux?",
    "your_reading_language": "Quelle langue lisez-vous le mieux?",
    "your_phone_number": "Quel est votre numéro de téléphone?",
    "your_referrer": "Où avez-vous eu l'application?",
    "alert_title": "Désolé !",
    "alert_message": "Vous n'avez pas rempli tous les champs du formulaire.",
    "alert_wrong_phone_number": "Vous n'avez pas entré un numéro de téléphone correct.",
    "alert_btn": "Ok",
    "get_started": "C'est parti!",
    "save": "Enregistrer"
  },
  "slides": {
    "welcome": "Bonjour !",
    "message": "As-tu envie d’en apprendre plus sur ton corps, ta sexualité ou tes droits ? Te voilà au bon endroit, Ici tu trouveras toutes sortes d’informations utiles et didactiques que tu pourras mettre en pratique.",
    "little message": "Commence par choisir ton personnage...",
    "start": "Démarrer !"
  },
  "mascotte": {
    "choose-mascot": "Choisissez votre mascotte !",
    "give-mascot-name": "Donnez moi un nom !",
    "submit": "C'EST BON ?",
    "save": "Enregistrer"
  },
  "lesson-content": {
    "current-path": "Parcours Actuel:",
    "video-menu": "Vidéo",
    "dialogue-menu": "Dialogue",
    "thematic-menu": "Théorie",
    "aretenir-menu": "A Retenir",
    "pacte-menu": "Pacte",
    "engagement": "Engagement",
    "discussions": "Discussions",
    "quiz-menu": "Quiz",
    "next": "Suite >",
    "langSwitchFon": "Ecouter en Fon",
    "langSwitchFrench": "Ecouter en Français"
  },
  "episode": {
    "of-track": "du parcours",
    "continue": "continuez!",
    "downloadInProgress": "Téléchargement de la leçon en cours: ",
    "downloadFailed": "Le téléchargement de la leçon a échoué."
  },
  "quiz" : {
    "check": "Vérifier",
    "next": "Suivant >",
    "submit": "Valider",
    "successTitle": "Bravo !",
    "successContent": "<p>Toutes les reponses sont justes.</p>",
    "failedTitle": "Oups! Toutes vos reponses ne sont pas bonnes.",
    "failedContent": "<p>Réessayer pour continuer l'aventure.</p>",
    "start": "Démarrer",
    "replay": "Rejouer",
    "unlock-next": "Debloquer la suite >",
    "change-parcours": "Changer de parcours",
    "discussions": "Discussions",
    "response": "Réponse"
  },
  "parametres" : {
    "change-mascotte": "Changer de mascotte",
    "change-info": "Modifier ses informations",
    "go-home": "Revenir à l'acceuil",
    "my-id": "Mon Identifiant: \"{0}\""
  },
  "faq": {
    "faq": "FAQ"
  },
  "help": {
    "howItWorks": "Comment ça marche?",
    "howItWorksMessage": "<p>C’est très simple: choisis le thème qui t’intéresse, et commence ton parcours.</p>" +
                         "<p>Chaque leçon comprend:<br>" +
                         "1. Une vidéo sur le thème, avec des jeunes comme toi<br>" +
                         "2. Un dialogue<br>" +
                         "3. Une leçon à retenir<br>" +
                         "4. Un quiz pour tester tes nouvelles connaissances.</p>" +
                         "<p>On te propose aussi des sujets à débattre avec tes amis pour témoigner de ton engagement sur le thème!</p>",
    "appAuthor": "Qui a créé l’appli?",
    "appAuthorMessage": "<p>L’appli a été créé par OneWorld, Etrilabs, et Butterfly Works, avec l’appui de UNFPA Benin, le gouvernement du Bénin, et le Royaume Belge. " +
                        "Ce n’est pas tout! Tout ce que tu vois a été fait en collaboration avec des jeunes comme toi, découvre les ici!</p>",
    "dataUse": "Est ce qu’utiliser l’appli va utiliser tout mon forfait?",
    "dataUseMessage": "<p>Non, une fois téléchargé, l’appli est gratuit à utiliser. Par contre nous te conseillons de te connecter de temps en temps pour accéder à la version la plus récente.</p>",
    "lessonsCompleted": "J’ai fini toutes les leçons. Et maintenant?",
    "lessonsCompletedMessage": "<p>Bravo! Tu peux commencer par partager l’appli avec tous tes amis et contribuer au bien être de tous. " +
                               "Nous avons surtout envie d’augmenter le nombre de filles qui utilisent l’appli, puisque souvent celles-ci ont moins accès à l’éducation que les garçons. A toi de le montrer à toutes tes copines!</p>" +
                               "<p>Tu peux aussi chatter avec d’autres utilisateurs en nous rejoignant sur Facebook et WhatsApp.</p>",
    "contactUs": "Si je veux vous contacter, je fais comment?",
    "contactUsMessage": "<p>Tu peux nous envoyer un email à l'adresse suivante: benin@oneworld.org</p>",
    "chat": "Comment je peux parler aux autres utilisateurs?",
    "chatMessage": "<p>Rejoins nous sur Facebook et WhatsApp en suivant les liens dans le menu!</p>"

  },
  "map": {
    "intro": "Cette carte te montre les services sympathiques aux jeunes vers lesquelles tu peux t’adresser pour avoir des informations et des services concernant ta santé sexuelle et reproductive."
  },
  "askQuestion": {
    "phoneNumber": "Votre n° de téléphone",
    "whyPhoneNumber": "Nous utiliserons ton numero pour t'envoyer une reponse par SMS.",
    "wrongPhoneNumber": "Veuillez entrer un numéro de téléphone correct.",
    "askQuestion": "Entrez votre question",
    "submit": "Envoyer",
    "sendingSucceed": "Votre question a bien été envoyée.",
    "sendingFailed": "Votre question n'a pas pu être envoyée.",
    "fillAllFields": "Veuillez remplir tous les champs."
  }
};

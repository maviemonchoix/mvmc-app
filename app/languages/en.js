module.exports = {
  "hello": {
    "hello": "Hello !"
  },
  "home": {
    "my_life_my_choice": "Ma Vie Mon Choix",
    "my_courses": "Mes parcours",
    "progress_message": "Chaque parcours comprend 4 topics. Vous avez déjà débloqué {0}% des contenus et obtenu {1} pièces d'or. Pas mal!"
  },
  "demographic_data": {
    "mascot_welcome_message": "Please fill in all the information to start the adventure!",
    "your_pseudo": "Pseudo",
    "your_age": "Age",
    "your_gender": "Gender",
    "your_occupation": "Occupation",
    "your_department": "Departement",
    "your_zone": "In which area do you live?",
    "your_speaking_language": "What is your speaking language?",
    "your_reading_language": "What is your reading language?",
    "your_referrer": "Where did you get the application from?",
    "alert_title": "Sorry !",
    "alert_message": "You didn't fill all the fields",
    "alert_btn": "Okay",
    "get_started": "Get started!"
  },
  "slides": {
	  "welcome": "Welcome !",
		"message": "Message CLI removes plugin files from your app’s node_modules folder in the root of your project. The CLI also removes any of the plugin’s dependencies jhghjh jhjkkj jhhhkkj hjhukk.",
		"little message": "Litle message process, the NativeScript. !",
		"start": "Start !"
  },
  "mascotte": {
    "choose-mascot": "Choose your mascot !",
    "give-mascot-name": "give me a name !",
    "submit": "IT'S OKAY ?"
  },
  "lesson-content": {
    "current-path": "Current Track:",
    "video-menu": "Video",
    "dialogue-menu": "Dialogue",
    "thematic-menu": "Thematic",
    "quiz-menu": "Quiz",
    "next": "Next >"
  },
  "episode": {
    "of-track": "of track",
    "continue": "continue!"
  },
  "quiz" : {
    "next": "Next >",
    "submit": "Submit",
    "successTitle": "Bravo !",
    "successContent": "<p>Toutes les reponses sont justes.</p>",
    "failedTitle": "Oups! Toutes vos reponses ne sont pas bonnes.",
    "failedContent": "<p>Réessayer pour continuer l'aventure.</p>",
    "replay": "Rejouer",
    "unlock-next": "Debloquer la suite >",
    "change-parcours": "Changer de parcours",
  }
};

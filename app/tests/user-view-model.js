var userViewModel = require('~/shared/view-models/user-view-model');
var user;

var randomAttributes = {
  pseudonym: "jack",
  age: 19,
  gender: "homme",
  occupation: "school-student",
  department: "atacora",
  zone: "rural",
  speaking_language: "en",
  reading_language: "fr",
  mascot_id: "m1",
  mascot_name: "Dr Assiba",
};

describe("A user ", function() {
  beforeEach(function () {
    user = new userViewModel();
  });

  afterEach(function () {
    user.remove();
  });

  it(" is well initiated !", function() {
    expect(user.pseudonym).toBe('');
  });

  it(" can be updated on one attribute.", function() {
    user.updateAttribute('pseudonym', 'baba');
    expect(user.pseudonym).toBe('baba');
  });

  it(" can be updated on many attributes at the same time.", function() {
    user.updateAttributes(randomAttributes);
    for (var prop in randomAttributes) {
      expect(user[prop]).toBe(randomAttributes[prop]);
    }
  });

  it(" can be saved and loaded correctly.", function() {
    user.updateAttributes(randomAttributes);
    oldUser = JSON.parse(JSON.stringify(user));
    user.save();
    user.load();
    expect(user._map).toEqual(oldUser._map);
  });

  it(" can have his total score updated.", function() {
    user.updateTotalScore(600);
    expect(user.progress.total_score).toBe(600);
  });

  it(" can have his total score increased.", function() {
    user.updateTotalScore(600);
    user.addToTotalScore(150);
    expect(user.progress.total_score).toBe(750);
  });

  it(" can start a new lesson.", function() {
    user.startLesson(1);
    expect(user.progress.lessons.length).toBe(1);
    expect(user.progress.lessons[0].id).toBe(1);
  });

  it(" can get initial lesson progress.", function() {
    user.startLesson(1);
    expect(user.getProgressByLesson(1)).toBe(0);
  });

  it(" can get initial overall lessons progress.", function() {
    user.startLesson(1);
    expect(user.getOverallProgress()).toBe(0);
  });

  it(" can start a new episode in a lesson.", function() {
    user.startLesson(1);
    user.startEpisode(1, 1);
    expect(user.progress.lessons.length).toBe(1);
    expect(user.progress.lessons[0].id).toBe(1);
    expect(user.progress.lessons[0].episodes.length).toBe(1);
    expect(user.progress.lessons[0].episodes[0].number).toBe(1);
  });

  it(" can complete a video from an episode of a lesson.", function() {
    user.startLesson(1);
    user.startEpisode(1, 1);

    expect(user.progress.lessons.length).toBe(1);
    expect(user.progress.lessons[0].id).toBe(1);
    expect(user.progress.lessons[0].episodes.length).toBe(1);
    expect(user.progress.lessons[0].episodes[0].number).toBe(1);

    user.completeVideo(1, 1);
    expect(user.progress.lessons[0].episodes[0].video).toBe(true);
  });

  it(" can answer a dialogue from an episode of a lesson.", function() {
    user.startLesson(1);
    user.startEpisode(1, 1);

    expect(user.progress.lessons.length).toBe(1);
    expect(user.progress.lessons[0].id).toBe(1);
    expect(user.progress.lessons[0].episodes.length).toBe(1);
    expect(user.progress.lessons[0].episodes[0].number).toBe(1);

    user.answerDialogue(1, 1, 1, 'yes', 'Oui, Je crois');
    expect(user.progress.lessons[0].episodes[0].dialogue.id).toBe(1);
    expect(user.progress.lessons[0].episodes[0].dialogue.answer.key).toBe('yes');
    expect(user.progress.lessons[0].episodes[0].dialogue.answer.text).toBe('Oui, Je crois');
  });

  it(" can complete a thematic from an episode of a lesson.", function() {
    user.startLesson(1);
    user.startEpisode(1, 1);

    expect(user.progress.lessons.length).toBe(1);
    expect(user.progress.lessons[0].id).toBe(1);
    expect(user.progress.lessons[0].episodes.length).toBe(1);
    expect(user.progress.lessons[0].episodes[0].number).toBe(1);

    user.completeThematic(1, 1);
    expect(user.progress.lessons[0].episodes[0].thematic).toBe(true);
  });

  it(" can answer a quiz's answer from an episode of a lesson.", function() {
    user.startLesson(1);
    user.startEpisode(1, 1);

    expect(user.progress.lessons.length).toBe(1);
    expect(user.progress.lessons[0].id).toBe(1);
    expect(user.progress.lessons[0].episodes.length).toBe(1);
    expect(user.progress.lessons[0].episodes[0].number).toBe(1);

    user.answerQuizQuestion(1, 1, 1, 'Qui a mangé le poulet ?', 1, 'C\'est Bintou', false);

    expect(user.progress.lessons[0].episodes[0].quiz[0].id).toBe(1);
    expect(user.progress.lessons[0].episodes[0].quiz[0].question).toBe('Qui a mangé le poulet ?');
    expect(user.progress.lessons[0].episodes[0].quiz[0].answers.key).toBe(1);
    expect(user.progress.lessons[0].episodes[0].quiz[0].answers.text).toBe('C\'est Bintou');
    expect(user.progress.lessons[0].episodes[0].quiz[0].answers.value).toBe(false);
  });

  it(" can get the right lesson progress after completing a number of steps.", function() {
    user.startLesson(1);
    user.startEpisode(1, 1);
    user.completeVideo(1, 1);
    user.answerDialogue(1, 1, 1, 'yes', 'Oui, Je crois');
    user.completeThematic(1, 1);
    user.answerQuizQuestion(1, 1, 1, 'Qui a mangé le poulet ?', 1, 'C\'est Bintou', false);
    expect(user.getProgressByLesson(1)).toBe(25);
  });

  it(" can get the right overall lesson progress after completing a number of steps.", function() {
    user.startLesson(1);
    user.startEpisode(1, 1);
    user.completeVideo(1, 1);
    user.answerDialogue(1, 1, 1, 'yes', 'Oui, Je crois');
    user.completeThematic(1, 1);
    user.answerQuizQuestion(1, 1, 1, 'Qui a mangé le poulet ?', 1, 'C\'est Bintou', false);
    user.startLesson(2);
    user.startEpisode(2, 1);
    user.completeVideo(2, 1);
    user.answerDialogue(2, 1, 1, 'yes', 'Oui, Je crois');
    expect(user.getOverallProgress()).toBe(19);
  });
});

var application = require("application");
var i18n = require("./utils/i18n");
var globals = require("./utils/globals");
require("nativescript-dom");
var utils = require("utils/utils");
var connectivity = require("tns-core-modules/connectivity");
var ObservableModule = require("data/observable");
var UserViewModel = require("./shared/view-models/user-view-model")();
var AnalyticViewModel = require("./shared/view-models/analytic-view-model")();
var platformModule = require("platform");
var jobScheduler = require("./job/job-scheduler");

var saveAnalytic = function() {
  if (global.activity && global.activity !== "undefined" && global.activity !==
    null) {
    global.activity.duration = global.stopTimer();

    if (global.activity.hasOwnProperty("answers")) {
      if (global.activity.answers.length > 0) {
        AnalyticViewModel.addActivityData(global.activity);
      } else {
        global.activity = null;
      }
    } else {
      AnalyticViewModel.addActivityData(global.activity);
    }
  }
};

if (application.android) {
  application.android.on(application.AndroidApplication.activityPausedEvent,
    function(args) {
      saveAnalytic();
    });

  application.android.on(application.AndroidApplication.activityBackPressedEvent,
    function(args) {
      saveAnalytic();
    });

}

if (platformModule.device.sdkVersion >= 21) {
  jobScheduler.scheduleJob(utils.ad.getApplicationContext());

  application.on(application.launchEvent, function(args) {
    connectivity.startMonitoring(function onConnectionTypeChanged(
      newConnectionType) {

      switch (newConnectionType) {
        case connectivity.connectionType.none:
          jobScheduler.scheduleJob(utils.ad.getApplicationContext());
          break;
        case connectivity.connectionType.wifi:
          AnalyticViewModel.load();
          AnalyticViewModel.send();
          break;
        case connectivity.connectionType.mobile:
          AnalyticViewModel.load();
          AnalyticViewModel.send();
          break;
      }
    });
  });
}

global.lang = 'fr';
globals.register();
i18n.init(application, global.lang);

UserViewModel.loadAndExec(function(isLoaded) {
  if (isLoaded) {
    if (UserViewModel.lastPage._map.moduleName != '') {
      application.start(UserViewModel.lastPage._map);
    } else {
      application.start({
        moduleName: "views/home/home"
      });
    }
  } else {
    application.start({
      moduleName: "views/slides/slides"
    });
  }
});

AnalyticViewModel.verifyVersion();


/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/

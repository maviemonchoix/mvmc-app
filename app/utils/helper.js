exports.findByProperty = function (arr, prop, value) {
  return arr.filter(function(obj) {
    return obj[prop] == value;
  })[0];
};

exports.findIndexByProperty = function (arr, prop, value) {
  for(var i = 0; i < arr.length; i++) {
    if(arr[i][prop] == value) {
      return i;
    }
  }
  return -1;
};
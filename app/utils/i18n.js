var fr = require("../languages/fr");
var en = require("../languages/en");
var stringFormat = require('nativescript-stringformat');

var currentLang = 'fr';

var L = function (page, key) {
  var s = currentLang == 'en' ? en[page][key] : fr[page][key]; 

  if (arguments.length > 2) {
    s = stringFormat.formatArray(s, Array.prototype.slice.call(arguments).slice(2));
  }

  return s;
};

exports.init = function (application, lang) {
  currentLang = lang || currentLang;
  var applicationResources = application.getResources();
  applicationResources.L = L;
  application.setResources(applicationResources);
  global.L = L;
};
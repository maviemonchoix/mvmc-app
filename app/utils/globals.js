var frameModule = require("ui/frame");
var appSettings = require("application-settings");
var appAvailability = require("nativescript-appavailability");
var utilsModule = require("tns-core-modules/utils/utils");
var phone = require("nativescript-phone");

var quitApp = function () {
  android.os.Process.killProcess(android.os.Process.myPid());
};

var goTo = function (args) {
  var page = args.object.dpage;

  if (args.object.dspecial == 'facebook') {
    appAvailability.available("com.facebook.katana").then(function(avail) {
      if (avail) {
        utilsModule.openUrl("fb://facewebmodal/f?href=https://www.facebook.com/AaVBENIN/");
      } else {
        utilsModule.openUrl("https://www.facebook.com/AaVBENIN/");
      }
    })
  } else if (args.object.dspecial == 'whatsapp') {
    utilsModule.openUrl("https://chat.whatsapp.com/invite/JysxxvUYn5AC1uT7jgNRDt");
  } else if (args.object.dspecial == 'messenger') {
    utilsModule.openUrl("https://www.messenger.com/t/AaVBENIN");
  } else if (args.object.dspecial == 'sharelink') {
    utilsModule.openUrl("https://play.google.com/store/apps/details?id=com.etrilabs.mvmc");
  } else if (args.object.dspecial == 'trotrogaho') {
    utilsModule.openUrl("https://trotrogaho.com/?utm_source=mvmc-mobile-app&utm_medium=mobile-app&utm_campaign=trotrogaho");
  } else if (args.object.dspecial == 'green-line') {
    phone.dial("7344",true);
  } else {

    if (page == "#") {
      return;
    }

    var context = {};

    if (args.object.dparam && args.object.dvalue) {
      context[args.object.dparam] = args.object.dvalue;
    }

    frameModule.topmost().navigate({
      moduleName: "views/" + page + "/" + page,
      context: context,
      animated: true
    });
  }
};

var initTimer = function () {
  global.startTime = new Date();
}

var stopTimer = function () {
  var currentTime = new Date();
  var duration = currentTime.getTime() - global.startTime.getTime();
  return duration/1000;
}

var dateFormat = function formatDate(d) {
  var month = String(d.getMonth() + 1);
  var day = String(d.getDate());
  var year = String(d.getFullYear());
  var hour = String(d.getHours());
  var minute = String(d.getMinutes());
  var second = String(d.getSeconds());

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  if (hour.length < 2) hour = '0' + hour;
  if (minute.length < 2) minute = '0' + minute;
  if (second.length < 2) second = '0' + second;
  return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
}

exports.register = function () {
  global.quitApp = quitApp;
  global.goTo = goTo;
  global.initTimer = initTimer;
  global.stopTimer = stopTimer;
  global.dateFormat = dateFormat;
  global.appVersion = "1.8.4";
  global.appNameSpace = "com.etrilabs.mvmc";
};

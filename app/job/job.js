android.app.job.JobService.extend("com.etrilabs.mvmc.AnalyticJobService", {
    onStartJob: function(params) { 
        var AnalyticViewModel = require("../shared/view-models/analytic-view-model")();
        AnalyticViewModel.load();
        AnalyticViewModel.send();
        var utils = require("utils/utils");
    
        return false;
    },
    
    onStopJob: function() {
    }
});
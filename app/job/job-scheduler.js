function scheduleJob(context) {
    var component = new android.content.ComponentName(context, com.etrilabs.mvmc.AnalyticJobService.class);
    const builder = new android.app.job.JobInfo.Builder(0, component);
    builder.setRequiredNetworkType(android.app.job.JobInfo.NETWORK_TYPE_ANY);

    const jobScheduler = context.getSystemService(android.content.Context.JOB_SCHEDULER_SERVICE);
    jobScheduler.schedule(builder.build());
}

module.exports.scheduleJob = scheduleJob;

var ObservableModule = require("data/observable");
var appSettings = require("application-settings");
var helper = require("~/utils/helper");
var firebase = require("nativescript-plugin-firebase");

var lessonsData = require("~/data/fr/lessons");

module.exports = function() {
  var pointPerSection = 6.25;
  var pointPerEpisode = 25;

  var lessonModel = {
    id: 0,
    episodes: []
  };

  var episodeModel = {
    number: 0,
    video: false,
    dialogue: {
      id: -1,
      answer: {
        key: "",
        text: ""
      }
    },
    aretenir: false,
    thematic: false,
    pacte: false,
    quiz: {
      completed: false,
      questions: []
    }
  };

  var questionModel = {
    id: 0,
    question: "",
    answers: {
      "key": "",
      "text": "",
      "value": ""
    }
  };

  var userModel = new ObservableModule.fromObjectRecursive({
    id: "",
    pseudonym: "",
    age: "",
    gender: "",
    occupation: "",
    department: "",
    zone: "",
    speaking_language: "",
    reading_language: "",
    referrer: "",
    mascot_id: "",
    mascot_name: "",
    phoneNumber: "",
    FCMToken: "",
    lastPage: {
      moduleName: '',
      context: {}
    },
    progress: {
      total_score: 0,
      lessons: []
    }
  });

  userModel.isFirstLoad = function() {
    if (!appSettings.hasKey('isFirstLoad')) {
      appSettings.setBoolean('isFirstLoad', false);
      return true;
    } else {
      return appSettings.getBoolean('isFirstLoad', false);
    }
  };

  userModel.initFirebase = function () {
    var self = this;
    firebase.init({
      showNotifications: true,
      showNotificationsWhenInForeground: true,
      onPushTokenReceivedCallback: function(token) {
        console.log("Firebase push token: " + token);
        self.updateAttribute('FCMToken', token);
      },
      onMessageReceivedCallback: function(message) {
        console.log("Title: " + message.title);
        console.log("Body: " + message.body);
        // if your server passed a custom property called 'foo', then do this:
        // console.log("Value of 'foo': " + message.data.foo);
      }
    });
  };

  userModel.areDemographicDataEmpty = function () {
    return userModel._map.pseudonym == "" ||
      userModel._map.age == "" ||
      userModel._map.gender == "" ||
      userModel._map.occupation == "" ||
      userModel._map.department == "" ||
      userModel._map.zone == "" ||
      userModel._map.speaking_language == "" ||
      userModel._map.reading_language == "" ||
      userModel._map.referrer == "";
  };

  userModel.remove = function() {
    appSettings.remove('userModel');
  };

  userModel.save = function() {
    appSettings.setString('userModel', JSON.stringify(this._map));
  };

  userModel.load = function() {
    if (!appSettings.hasKey('userModel')) {
      return;
    }
    var data = JSON.parse(appSettings.getString('userModel'));
    for (var prop in data) {
      this.set(prop, data[prop]);
    }
  };

  userModel.loadAndExec = function(callback) {
    if (this.id != "") {
      callback(true);
    } else {
      if (!appSettings.hasKey('userModel')) {
        return callback(false);
      }

      var data = JSON.parse(appSettings.getString('userModel'));

      for (var prop in data) {
        this.set(prop, data[prop]);
      }

      if (this.id != "") {
        callback(true);
      } else {
        callback(false);
      }
    }
  };

  userModel.saveLastPage = function(page, context) {
    this._map.lastPage._map.moduleName = 'views/' + page + '/' + page;
    if (context) {
      this._map.lastPage._map.context = context;
    }
    this.save();
  };

  userModel.getLastPage = function() {
    return {
      moduleName: this._map.lastPage.moduleName,
      context: this._map.lastPage.context,
      animated: true
    }
  };

  userModel.generateId = function() {
    this.set("id", constructId());
    this.save();
  };

  userModel.updateAttribute = function(prop, value) {
    this.set(prop, value);
    this.save();
  };

  userModel.updateAttributes = function(attributes) {
    for (var prop in attributes) {
      this.set(prop, attributes[prop]);
    }
    this.save();
  };

  userModel.formatPhoneNumber = function(phoneNumber) {
    phoneNumber = phoneNumber.trim().replace(/\s/g, '');

    if (phoneNumber.indexOf('229') == 0) {
      phoneNumber = phoneNumber.substring(3);
    } else if (phoneNumber.indexOf('+229') == 0) {
      phoneNumber = phoneNumber.substring(4);
    } else if (phoneNumber.indexOf('00229') == 0) {
      phoneNumber = phoneNumber.substring(5);
    }

    if (/^\d+$/.test(phoneNumber) && phoneNumber.length == 8) {
      this.updateAttribute('phoneNumber', phoneNumber);
    } else {
      phoneNumber = '';
    }

    return phoneNumber;
  };

  userModel.updateTotalScore = function(score) {
    this.progress.set('total_score', score);
    this.save();
  };

  userModel.addToTotalScore = function(score) {
    this.progress.total_score = this.progress.total_score + score;
    this.save();
  };

  userModel.startLesson = function(id) {
    if (helper.findByProperty(this.progress.lessons, 'id', id)) {
      return;
    }

    var lesson = JSON.parse(JSON.stringify(lessonModel));
    lesson.id = id;
    this.progress.lessons.push(lesson);
    this.save();
  };

  userModel.getLesson = function(id) {
    return helper.findByProperty(this.progress.lessons, 'id', id);
  };

  userModel.getProgressByEpisode = function(lessonId, episodeNumber) {
    var lesson = helper.findByProperty(this.progress.lessons, 'id',
      lessonId);
    var lessonData = helper.findByProperty(lessonsData, 'id', lessonId);

    if (!lesson) {
      return 0;
    }

    var episode = lesson.episodes[episodeNumber];

    if (!episode) {
      return 0;
    }

    var progress = 0;
    progress += episode.video ? pointPerEpisode : 0;
    progress += episode.dialogue.id != -1 ? pointPerEpisode : 0;

    if (episodeNumber == lessonData.episodes.length - 1) {
      progress += episode.aretenir ? pointPerSection : 0;
      progress += episode.pacte ? pointPerSection : 0;
    } else {
      progress += episode.thematic ? pointPerEpisode : 0;
      progress += episode.quiz && episode.quiz.completed ? pointPerEpisode :
        0;
    }
    return progress;
  };

  userModel.getProgressByLesson = function(lessonId) {
    var lesson = helper.findByProperty(this.progress.lessons, 'id',
      lessonId);
    var lessonData = helper.findByProperty(lessonsData, 'id', lessonId);

    if (!lesson) {
      return 0;
    }

    var progress = 0;

    for (var i = 0; i < lesson.episodes.length; i++) {
      var episode = lesson.episodes[i];
      progress += episode.video ? pointPerSection : 0;
      progress += episode.dialogue.id != -1 ? pointPerSection : 0;

      if (i == lessonData.episodes.length - 1) {
        progress += episode.aretenir ? pointPerSection : 0;
        progress += episode.pacte ? pointPerSection : 0;
      } else {
        progress += episode.thematic ? pointPerSection : 0;
        progress += episode.quiz && episode.quiz.completed ?
          pointPerSection : 0;
      }
    }

    return progress;
  };

  userModel.getOverallProgress = function() {
    var progress = 0;

    for (var i = 0; i < this.progress.lessons.length; i++) {
      var lessonId = this.progress.lessons[i].id;
      progress += this.getProgressByLesson(lessonId);
    }

    return this.progress.lessons.length > 0 ? Math.round(progress /
      lessonsData.length) : 0;
  };

  userModel.startEpisode = function(lessonId, number) {
    var lindex = helper.findIndexByProperty(this.progress.lessons, 'id',
      lessonId);

    if (lindex == -1) {
      return;
    }

    if (helper.findByProperty(this.progress.lessons[lindex].episodes,
        'number', number)) {
      return;
    }

    var episode = JSON.parse(JSON.stringify(episodeModel));
    episode.number = number;

    this.progress.lessons[lindex].episodes.push(episode);

    this.save();
    return;
  };

  userModel.getEpisode = function(lessonId, number) {
    var lindex = helper.findIndexByProperty(this.progress.lessons, 'id',
      lessonId);

    if (lindex == -1) {
      return null;
    }

    return helper.findByProperty(this.progress.lessons[lindex].episodes,
      'number', number);
  };

  userModel.completeVideo = function(lessonId, episodeNumber) {
    var lindex = helper.findIndexByProperty(this.progress.lessons, 'id',
      lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = helper.findByProperty(this.progress.lessons[lindex].episodes,
      'number', episodeNumber);

    if (!episode) {
      return;
    }

    episode.video = true;
    this.save();
  };

  userModel.answerDialogue = function(lessonId, episodeNumber, dialogueId,
    answerKey, answerText) {
    var lindex = helper.findIndexByProperty(this.progress.lessons, 'id',
      lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = helper.findByProperty(this.progress.lessons[lindex].episodes,
      'number', episodeNumber);

    if (!episode) {
      return;
    }

    episode.dialogue.id = dialogueId;
    episode.dialogue.answer.key = answerKey;
    episode.dialogue.answer.text = answerText;

    this.save();
  };

  userModel.completeThematic = function(lessonId, episodeNumber) {
    var lindex = helper.findIndexByProperty(this.progress.lessons, 'id',
      lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = helper.findByProperty(this.progress.lessons[lindex].episodes,
      'number', episodeNumber);

    if (!episode) {
      return;
    }

    episode.thematic = true;
    this.save();
  };

  userModel.completeAretenir = function(lessonId, episodeNumber) {
    var lindex = helper.findIndexByProperty(this.progress.lessons, 'id',
      lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = helper.findByProperty(this.progress.lessons[lindex].episodes,
      'number', episodeNumber);

    if (!episode) {
      return;
    }

    episode.aretenir = true;
    this.save();
  };

  userModel.completePacte = function(lessonId, episodeNumber) {
    var lindex = helper.findIndexByProperty(this.progress.lessons, 'id',
      lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = helper.findByProperty(this.progress.lessons[lindex].episodes,
      'number', episodeNumber);

    if (!episode) {
      return;
    }

    episode.pacte = true;
    this.save();
  };

  userModel.answerQuizQuestion = function(lessonId, episodeNumber, questionId,
    questionLabel, answerKey, answerText, answerValue) {
    var lindex = helper.findIndexByProperty(this.progress.lessons, 'id',
      lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = helper.findByProperty(this.progress.lessons[lindex].episodes,
      'number', episodeNumber);

    if (!episode) {
      return;
    }

    var question = helper.findByProperty(episode.quiz.questions, 'id',
      questionId)

    if (question) {
      question.question = questionLabel;
      question.answers.key = answerKey;
      question.answers.text = answerText;
      question.answers.value = answerValue;
    } else {
      var q = JSON.parse(JSON.stringify(questionModel));

      q.id = questionId;
      q.question = questionLabel;
      q.answers.key = answerKey;
      q.answers.text = answerText;
      q.answers.value = answerValue;

      episode.quiz.questions.push(q);
    }


    this.save();
  };

  userModel.completeQuiz = function(lessonId, episodeNumber) {
    var lindex = helper.findIndexByProperty(this.progress.lessons, 'id',
      lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = helper.findByProperty(this.progress.lessons[lindex].episodes,
      'number', episodeNumber);

    if (!episode) {
      return;
    }

    episode.quiz.completed = true;
    this.save();
  };

  userModel.load();

  return userModel;
};


function constructId() {
  var timestamp = new Date();
  timestamp = timestamp.getTime();
  id = base10_to_base64(timestamp);
  id = id.replace(new RegExp("/", 'g'), "A");
  id = id.replace(new RegExp("\\+", 'g'), "B");
  id = id.replace(new RegExp("=", 'g'), "");
  id = id.toUpperCase();
  var checkCharacter = id.charCodeAt(3) + 1;
  checkCharacter == 48 ? 58 : checkCharacter;
  checkCharacter == 65 ? 91 : checkCharacter;
  id = id + String.fromCharCode(checkCharacter);

  return id;
}

function base10_to_base64(num) {
  var order =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/";
  var base = order.length;
  var str = "",
    r;
  while (num) {
    r = num % base
    num -= r;
    num /= base;
    str = order.charAt(r) + str;
  }
  return str;
}

var ObservableModule = require("data/observable");
var appSettings = require("application-settings");
var helper = require("~/utils/helper");
var http = require("http");
var platformModule = require("platform");
var fs = require("file-system");
var connectivity = require("tns-core-modules/connectivity");
var UserViewModel = require("~/shared/view-models/user-view-model")();

module.exports = function () {

  var analyticModel = new ObservableModule.fromObject({
    id: "" ,
    start: "",
    end: "",
    appId: global.appNameSpace,
    appVersion: "",
    androidVersion: "",
    user: [],
    activities: []
  });

  analyticModel.init = function (data) {
    this.set("id", data.id);
    this.set("start", global.dateFormat(new Date()));
    this.set("appVersion", global.appVersion);
    this.set("androidVersion", platformModule.device.osVersion);
    this.user.push(formatUser(data, true));
    this.save();
    this.send();
  };

  analyticModel.reset = function (end) {
    this.set("start", end);
    this.set("end", "");
    this.user = [];
    this.activities = [];
    this.save();
  };

  analyticModel.remove = function () {
    appSettings.remove('analyticModel');
  };

  analyticModel.save = function () {
    appSettings.setString('analyticModel', JSON.stringify(this._map));
  };

  analyticModel.send = function () {
    if (isConnectionActive() && newDataExist(this._map)) {
      var end = global.dateFormat(new Date());
      this.set("end", end);
      var data = formatDataToSend(this._map);
      http.request({
          url: "http://data.oneworld.org/mlearning/api/v1/activity",
          method: "POST",
          headers: { "Content-Type": "application/json" },
          content: JSON.stringify(data)
      }).then(function (response) {
          analyticModel.reset(end);
          var utils = require("utils/utils");
      }, function (e) {
      });
    }

  };

  analyticModel.addUserData = function (data) {
    this.load();
    this.user.push(formatUser(data, false));
    this.save();
    this.send();
  };

  analyticModel.addActivityData = function (data) {
    this.load();
    UserViewModel.load();
    data.rewardScore = UserViewModel.progress.total_score;
    this.activities.push(data);
    global.activity = null;
    this.save();
    this.send();
  };

  analyticModel.verifyVersion = function () {
    this.load();
    var changed_data = {}, new_change = false;
    if (this.appVersion !== global.appVersion && this.appVersion !== "") {
      this.set("appVersion", global.appVersion);
      changed_data.appVersion = global.appVersion;
      new_change = true;
    }
    if (this.androidVersion !== platformModule.device.osVersion && this.androidVersion !== "") {
      this.set("androidVersion", platformModule.device.osVersion);
      changed_data.androidVersion = global.androidVersion;
      new_change = true;
    }
    if (new_change) {
      changed_data.datetime = global.dateFormat(new Date());
      this.user.push(changed_data);
      this.save();
      this.send();
    }
  };

  analyticModel.load = function () {
    if (!appSettings.hasKey('analyticModel')) {
      return;
    }
    var data = JSON.parse(appSettings.getString('analyticModel'));

    for (var prop in data) {
      this.set(prop, data[prop]);
    }
  };

  analyticModel.load();

  return analyticModel;
};

function formatUser(data, init) {
  var convertKey = {
    "Homme": "male",
    "Femme": "female",
    "Elève": "school-student",
    "Etudiant": "university-student",
    "Artisan - Apprenti": "craftsman-apprentice",
    "Pair Educateur": "peer-educator",
    "Enseignant": "teacher",
    "Prestataire de services pour Jeunes": "youth-service-provider",
    "Rurale": "rural",
    "Urbaine": "urban",
    "Anglais": "en",
    "Français": "fr",
    "Play Store": "app-store",
    "Parent": "relative",
    "Ami": "friend",
    "Autre": "other",
  };
  var user = {};

  if (init){
    user.datetime = global.dateFormat(new Date());
    user.appVersion = global.appVersion;
    user.androidVersion = platformModule.device.osVersion;
    user.uiLocale = "fr-BJ";
    user.distributorType = convertKey[data.referrer];
  }
  else {
    user.datetime = data.datetime;
  }

  if (data.hasOwnProperty("mascot_id")) {
    user.mascot = data.mascot_id;
  }

  if (data.hasOwnProperty("mascot_name")) {
    user.mascotName = data.mascot_name;
  }

  if (data.hasOwnProperty("pseudonym")) {
    user.nickname = data.pseudonym;
  }

  if (data.hasOwnProperty("age")) {
    user.age = parseInt(data.age);
  }

  if (data.hasOwnProperty("gender")) {
    user.gender = convertKey[data.gender];
  }

  if (data.hasOwnProperty("occupation")) {
    user.occupation = convertKey[data.occupation];
  }

  if (data.hasOwnProperty("department")) {
    user.dept = data.department;
  }

  if (data.hasOwnProperty("zone")) {
    user.zone = convertKey[data.zone];
  }

  if (data.hasOwnProperty("speaking_language")) {
    user.languageSpoken = convertKey[data.speaking_language];
  }

  if (data.hasOwnProperty("reading_language")) {
    user.languageRead = convertKey[data.reading_language];
  }

  return user;
}

function formatDataToSend(data) {
  delete data.appVersion;
  delete data.androidVersion;
  if (data.activities.length == 0){
    delete data.activities;
  }

  if (data.user.length == 0){
    delete data.user;
  }

  return data;
}

function newDataExist(data) {
  return data.activities.length !== 0 || data.user.length !== 0;
}

function isConnectionActive() {
  var status = false;
  var connectionType = connectivity.getConnectionType();
  switch (connectionType) {
      case connectivity.connectionType.none:
          break;
      case connectivity.connectionType.wifi:
          status = true;
          break;
      case connectivity.connectionType.mobile:
          status = true;
          break;
  }
  return status;
}

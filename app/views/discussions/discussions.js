var frameModule = require("ui/frame");
var labelModule = require("ui/label");
var ObservableModule = require("data/observable");
var ObservableArray = require("data/observable-array").ObservableArray;
var userViewModel = require("~/shared/view-models/user-view-model")();
var lessonsData = require("~/data/fr/lessons");
var helper = require("~/utils/helper");
var slides = require("nativescript-slides/nativescript-slides");
var AnalyticViewModel = require("../../shared/view-models/analytic-view-model");

var page;
var discussionsSectionContainer;
var lessonId;
var episodeNumber;
var discussions;
var currentPage = 0;
var drawer;
var analytic;

exports.pageLoaded = function (args) {
  currentPage = 0;
  global.initTimer();
  page = args.object;
  analytic = new AnalyticViewModel();
  lessonId = page.navigationContext.lessonId;
  episodeNumber = page.navigationContext.episodeNumber;

  initAnalytic();

  drawer = page.getViewById("sideDrawer");

  var lesson = helper.findByProperty(lessonsData, 'id', lessonId);

  discussions = new ObservableArray(lesson.episodes[episodeNumber].discussions);

  userViewModel.load();

  var isLastEpisode = false;
  var nextEpisodeProgress = 0;
  var nextEpisodeState;

  if (episodeNumber == lesson.episodes.length - 1) {
    isLastEpisode = true;
    nextEpisodeState = 'active';
  } else {
    nextEpisodeProgress = userViewModel.getProgressByEpisode(lessonId, episodeNumber + 1);
    nextEpisodeState = nextEpisodeProgress.progress > 0 ? '' : 'locked';
  }

  discussions.map(function (them, i) {
    them.current = false;
    them.index = i;

    if (i == discussions.length -1) {
      them.isLast = true;
    }

    discussions.setItem(i, them);

    if (i == currentPage) {
      them.current = true;
      discussions.setItem(i, them);
    }
    return them;
  });

  if (discussions.length > 1) {
    page.getViewById('nextBtn').visibility = 'visible';
  }

  page.bindingContext = new ObservableModule.fromObject({
    lessonIcon: lesson.icon,
    lessonTitle: lesson.title,
    episodeNumber: episodeNumber,
    discussions: discussions,
    episodesCount: lesson.episodes.length,
    episodeProgress: userViewModel.getEpisode(lessonId, episodeNumber),
    isLastEpisode: isLastEpisode,
    nextEpisodeState: nextEpisodeState,
    nextEpisodeProgress: nextEpisodeProgress,
    totalScore: userViewModel.progress.total_score,
    mascotSrc: "~/data/mascot/" + userViewModel.mascot_id + ".png",
  });
};

exports.prev = function prev(args) {
  currentPage--;
  page.getViewById('nextBtn').visibility = 'visible';

  if (currentPage <= 0) {
    currentPage = 0;
    page.getViewById('prevBtn').visibility = 'collapsed';
  }

  discussions.map(function (them, i) {
    them.current = false;
    them.index = i;

    discussions.setItem(i, them);

    if (i == currentPage) {
      them.current = true;
      discussions.setItem(i, them);
    }
    return them;
  });
}

exports.next = function next(args) {
  currentPage++;
  page.getViewById('prevBtn').visibility = 'visible';
  if (currentPage >= discussions.length -1) {
    currentPage = discussions.length -1;
    page.getViewById('nextBtn').visibility = 'collapsed';
  }

  discussions.map(function (them, i) {
    them.current = false;
    them.index = i;

    discussions.setItem(i, them);

    if (i == currentPage) {
      them.current = true;
      discussions.setItem(i, them);
    }
    return them;
  });
};

exports.goToQuiz = function () {
  saveAnalytic();
  frameModule.topmost().navigate({
    moduleName: "views/home/home",
    clearHistory: true,
    animated: true
  });
};

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

function initAnalytic () {
  global.activity = {
    datetime: global.dateFormat(global.startTime),
    type: "discussion",
    uiLocale: "fr-BJ",
    lessonNumber: parseInt(lessonId),
    partNumber: 1
  };
}

function saveAnalytic () {
  if (!global.activity) {
    return;
  }
  global.activity.duration = global.stopTimer();
  analytic.addActivityData(global.activity);
}

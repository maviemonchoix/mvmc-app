var frameModule = require("ui/frame");
var page;
var Observable = require("data/observable").Observable;
var userViewModel = require("../../shared/view-models/user-view-model")();

var drawer;

exports.loaded = function(args) {
  page = args.object;
  drawer = page.getViewById("sideDrawer");
  page.bindingContext = new Observable();
  if (userViewModel.id) {
    page.bindingContext.set('userId', userViewModel.id);
  }
};

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;
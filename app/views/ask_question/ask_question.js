var frameModule = require("ui/frame");
var observableModule = require("data/observable");
var observableArray = require("data/observable-array");
var connectivityModule = require("tns-core-modules/connectivity");
var UserViewModel = require("../../shared/view-models/user-view-model");
var fileSystemModule = require("tns-core-modules/file-system");
var permissions = require('nativescript-permissions');
var http = require("http");
var app = require("application");
var utils = require("utils/utils");


var context;
var page;
var pageData;
var drawer;
var user;
var phoneNumber;
var SMSSent;
var hidebutton=true;
var SmsManager = android.telephony.SmsManager;

exports.loaded = function(args) {
    permissions.requestPermission(android.Manifest.permission.SEND_SMS, "I need these permissions")
        .then( (args) => {
          hidebutton=true;
        })
        .catch( (args) => {
        hidebutton=false;
        });
  context = utils.ad.getApplicationContext();
  user = new UserViewModel();
  phoneNumber = user.phoneNumber;
  page = args.object;
  drawer = page.getViewById("sideDrawer");

  if(phoneNumber != '') {
    http.request({
      url: "https://smsbj.oneworld.org/api/conversation",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify({
        phone: '229' + phoneNumber
      })
    }).then(function (response) {
      if(response.statusCode == 200) {
        var documents = fileSystemModule.knownFolders.currentApp();
        var file = documents.getFile('conversationBackup.txt');

        file.writeText(JSON.stringify(response.content)).then(function(result) {
          file.readText().then(function(res) {
            bindConversation(res);
          });
        }).catch(function(err) {
          bindConversation(JSON.stringify(response.content));
        });
      } else {
        var documents = fileSystemModule.knownFolders.currentApp();
        var file = documents.getFile('conversationBackup.txt');
        file.readText().then(function(res) {
          bindConversation(res);
        });
      }
    }, function (e) {
      var documents = fileSystemModule.knownFolders.currentApp();
      var file = documents.getFile('conversationBackup.txt');
      file.readText().then(function(res) {
        bindConversation(res);
      });
    });
  } else {
    var documents = fileSystemModule.knownFolders.currentApp();
    var file = documents.getFile('conversationBackup.txt');
    file.readText().then(function(res) {
      bindConversation(res);
    });
  }

  pageData = new observableModule.fromObject({
    phoneNumber: phoneNumber,
    showPhoneNumberField: phoneNumber == '' ? true : false,
    conversation: []
  });

	page.bindingContext = pageData;
};


exports.sendQuestion = function(args) {
  var phoneNumberField = page.getViewById("phoneNumber");
  var wrongPhoneNumber = page.getViewById("wrongPhoneNumber");
  var questionField = page.getViewById("question");
  var submitBtn = page.getViewById("submitBtn");
  var sendingSucceed = page.getViewById("sendingSucceed");
  var sendingFailed = page.getViewById("sendingFailed");
  var fillAllFields = page.getViewById("fillAllFields");

  wrongPhoneNumber.visibility = 'collapsed';
  sendingSucceed.visibility = 'collapsed';
  sendingFailed.visibility = 'collapsed';
  fillAllFields.visibility = 'collapsed';

  if(phoneNumber == '') {
    phoneNumber = user.formatPhoneNumber(phoneNumberField.text);
  }

  var question = page.getViewById("question").text.trim();

  if(phoneNumber != '' && question != '') {
    var SMSSent = false;
    var connectionType = connectivityModule.getConnectionType();

    phoneNumberField.editable = false;
    questionField.editable = false;
    submitBtn.isEnabled = false;
    submitBtn.classList.add('disabled');

    switch (connectionType) {
      case connectivityModule.connectionType.none:
        //no Internet connection.
        var id = 'MVMC-message-sent';
        var pendingIntent = pendingIntnt(id);
        var sms = SmsManager.getDefault();
        sms.sendTextMessage('+22966527575', null, 'ID:' + user.id + ' ' + question, pendingIntent, null);
        broadcastReciever(id, function() {
          SMSSent = true;
          phoneNumberField.editable = true;
          questionField.editable = true;
          questionField.text = '';
          submitBtn.isEnabled = true;
          submitBtn.classList.remove('disabled');
          sendingFailed.visibility = 'collapsed';
          sendingSucceed.visibility = 'visible';
          app.android.unregisterBroadcastReceiver(id);
        });
        setTimeout(function() {
          if(!SMSSent) {
            phoneNumberField.editable = true;
            questionField.editable = true;
            submitBtn.isEnabled = true;
            submitBtn.classList.remove('disabled');
            sendingSucceed.visibility = 'collapsed';
            sendingFailed.visibility = 'visible';
            app.android.unregisterBroadcastReceiver(id);
          }
        }, 3000);
      break;

      default:
        http.request({
          url: "https://smsbj.oneworld.org/api/message",
          method: "POST",
          headers: { "Content-Type": "application/json" },
          content: JSON.stringify({
            to: '229' + phoneNumber,
            content: 'ID:' + user.id + ' ' + question

          })
        }).then(function (response) {
          if(response.statusCode == 200) {
            var documents = fileSystemModule.knownFolders.currentApp();
            var file = documents.getFile('conversationBackup.txt');
            file.readText().then(function(res) {
              res = JSON.parse(res);
              var date = new Date();
              res.push({
                "question": question,
                "answer": [],
                "datecreated": date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()
              });
              res = JSON.stringify(res);
              file.writeText(res).then(function(result) {}).catch(function(err) {});
              console.log(res);
              bindConversation(res);
            });
            SMSSent = true;
            phoneNumberField.editable = true;
            questionField.editable = true;
            questionField.text = '';
            submitBtn.isEnabled = true;
            submitBtn.classList.remove('disabled');
            sendingFailed.visibility = 'collapsed';
            sendingSucceed.visibility = 'visible';
          } else {
            phoneNumberField.editable = true;
            questionField.editable = true;
            submitBtn.isEnabled = true;
            submitBtn.classList.remove('disabled');
            sendingSucceed.visibility = 'collapsed';
            sendingFailed.visibility = 'visible';
          }
        }, function (e) {
          phoneNumberField.editable = true;
          questionField.editable = true;
          submitBtn.isEnabled = true;
          submitBtn.classList.remove('disabled');
          sendingSucceed.visibility = 'collapsed';
          sendingFailed.visibility = 'visible';
        });
      break;
    }
  } else {
    if(!/^\d+$/.test(phoneNumber) || phoneNumber.length != 8) {
      var wrongPhoneNumber = page.getViewById("wrongPhoneNumber");
      wrongPhoneNumber.visibility = 'visible';
    } else {
      var fillAllFields = page.getViewById("fillAllFields");
      fillAllFields.visibility = 'visible';
    }
  }
}

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

function bindConversation(conversation) {
  conversation = JSON.parse(conversation).map(function(obj) {
    return {
      id: new Date(obj.datecreated).getTime(),
      question: obj.question.indexOf('ID:') == 0 ? obj.question.substr(obj.question.indexOf(' ') + 1) : obj.question,
      answer: obj.answer,
      datecreated: obj.datecreated
    };
  });

  pageData.set('conversation', conversation);
}

function broadcastReciever(id, cb) {
  app.android.registerBroadcastReceiver(id, function() {
    cb();
  });
}

function pendingIntnt(id) {
  var intent = new android.content.Intent(id);
  return android.app.PendingIntent.getBroadcast(context, 0, intent, 0);
}

var frameModule = require("ui/frame");
var ObservableModule = require("data/observable");
var userViewModel = require("~/shared/view-models/user-view-model")();
var lessonsData = require("~/data/fr/lessons");
var helper = require("~/utils/helper");
var AnalyticViewModel = require("../../shared/view-models/analytic-view-model");

var page;
var answerElements;
var lessonId;
var episodeNumber;
var dialog;
var drawer;
var lesson;
var analytic;

exports.pageLoaded = function (args) {
  global.initTimer();
  page = args.object;
  analytic = new AnalyticViewModel();
  lessonId = page.navigationContext.lessonId;
  episodeNumber = page.navigationContext.episodeNumber;

  drawer = page.getViewById("sideDrawer");

  userViewModel.load();
  userViewModel.saveLastPage('dialogue', page.navigationContext);
  
  lesson = helper.findByProperty(lessonsData, 'id', lessonId);

  dialog = lesson.episodes[episodeNumber].dialogue;

  initAnalytic();

  var isLastEpisode = false;
  var nextEpisodeProgress = 0;
  var nextEpisodeState;

  if (episodeNumber == lesson.episodes.length - 1) {
    isLastEpisode = true;
    nextEpisodeState = 'active';
  } else {
    nextEpisodeProgress = userViewModel.getProgressByEpisode(lessonId, episodeNumber + 1);
    nextEpisodeState = nextEpisodeProgress.progress > 0 ? '' : 'locked';
  }

  page.bindingContext = new ObservableModule.fromObject({
    lessonIcon: lesson.icon,
    lessonTitle: lesson.title,
    episodeNumber: episodeNumber,
    dialog: dialog,
    episodesCount: lesson.episodes.length,
    episodeProgress: userViewModel.getEpisode(lessonId, episodeNumber),
    isLastEpisode: isLastEpisode,
    nextEpisodeState: nextEpisodeState,
    nextEpisodeProgress: nextEpisodeProgress,
    totalScore: userViewModel.progress.total_score,
    mascotSrc: "~/data/mascot/" + userViewModel.mascot_id + ".png",
  });

  answerElements = page.getViewById('answersBlock').getElementsByClassName('radio-input');
};

exports.onAnswerTap = function (args) {
  var element = args.object;

  answerElements.forEach(function(elm) {
    elm.classList.remove('active');
  }, this);

  element.classList.add('active');
  page.getViewById('btnNext').classList.remove('disabled');
};

exports.goToThematic = function () {
  answerElements.forEach(function(elm, i) {
    if (elm.classList.contains('active')) {
      userViewModel.answerDialogue(lessonId, episodeNumber, dialog.id, elm.dkey, elm.dtext);
      saveAnalytic(i+1);
      if (episodeNumber == lesson.episodes.length - 1) {
        frameModule.topmost().navigate({
          moduleName: "views/aretenir/aretenir",
          context: {
            lessonId: page.navigationContext.lessonId,
            episodeNumber: page.navigationContext.episodeNumber,
          },
          animated: true
        });
      } else {
        frameModule.topmost().navigate({
          moduleName: "views/thematic/thematic",
          context: {
            lessonId: page.navigationContext.lessonId,
            episodeNumber: page.navigationContext.episodeNumber,
          },
          animated: true
        });
      }
    }
  }, this);
}

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

function initAnalytic () {
  global.activity = {
    datetime: global.dateFormat(global.startTime),
    type: "opinion",
    uiLocale: "fr-BJ",
    lessonNumber: parseInt(lessonId),
    partNumber: parseInt(episodeNumber) + 1,
    questionNumber: 1,
    answers: []
  };
}

function saveAnalytic (answers) {
  if (!global.activity) {
    return;
  }
  global.activity.duration = global.stopTimer();
  global.activity.answers.push(answers);
  analytic.addActivityData(global.activity);
}

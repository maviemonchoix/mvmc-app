var frameModule = require("ui/frame");
var view = require("ui/core/view");
var pages = require("ui/page");
var dialogsModule = require("ui/dialogs");
var demographicsValues = require("../../data/" + global.lang + "/demographics");
var UserViewModel = require("../../shared/view-models/user-view-model");
var AnalyticViewModel = require("../../shared/view-models/analytic-view-model");

var user;
var page;
var mascotImage;
var analytic;
var editing = false;
var previousUserData;


exports.pageLoaded = function pageLoaded(args) {
  user = new UserViewModel();
  user.displayPhoneNumber = user.phoneNumber == '' ? false : true;
  analytic = new AnalyticViewModel();
  page = args.object;
  page.bindingContext = user;
  mascotImage = page.getViewById('mascotImage');
  mascotImage.src = "~/data/mascot/" + user.mascot_id + ".png"

  editing = page.navigationContext && page.navigationContext.editing;
  if (editing) {
    previousUserData = JSON.stringify(user._map);
    page.getViewById('referrer').visibility = 'collapsed';
    page.getViewById('saveButton').text = global.L('demographic_data', 'save');
  }
}

exports.ageInput = function () {
    var values = [];
    for (var i = 10; i < 61; i++) {
        values.push(i + "");
    }
    dialogsModule.action(demographicsValues.dialog_age, demographicsValues.action_cancel, values)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("age", result);
            }
        });
}

exports.genderInput = function () {
    dialogsModule.action(demographicsValues.dialog_gender, demographicsValues.action_cancel, demographicsValues.gender)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("gender", result);
            }
        });
}

exports.occupationInput = function () {
    dialogsModule.action(demographicsValues.dialog_occupation, demographicsValues.action_cancel, demographicsValues.occupation)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("occupation", result);
            }
        });
}

exports.departmentInput = function () {
    dialogsModule.action(demographicsValues.dialog_department, demographicsValues.action_cancel, demographicsValues.departments)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("department", result);
            }
        });
}

exports.zoneInput = function () {
    dialogsModule.action(demographicsValues.dialog_zone, demographicsValues.action_cancel, demographicsValues.zone)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("zone", result);
            }
        });
}

exports.speakingLanguageInput = function () {
    dialogsModule.action(demographicsValues.dialog_speaking_language, demographicsValues.action_cancel, demographicsValues.speaking_language)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("speaking_language", result);
            }
        });
}

exports.readingLanguageInput = function () {
    dialogsModule.action(demographicsValues.dialog_reading_language, demographicsValues.action_cancel, demographicsValues.reading_language)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("reading_language", result);
            }
        });
}

exports.referrerInput = function () {
    dialogsModule.action(demographicsValues.dialog_referrer, demographicsValues.action_cancel, demographicsValues.referrer)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("referrer", result);
            }
        });
}

exports.updateDemographicData = function () {
  var phoneNumber = page.getViewById("phoneNumber").text;
  var formatedPhoneNumber = user.formatPhoneNumber(phoneNumber);

  if (user.areDemographicDataEmpty()) {
      dialogsModule.alert({
          title: global.L('demographic_data', 'alert_title'),
          message: global.L('demographic_data', 'alert_message'),
          okButtonText: global.L('demographic_data', 'alert_btn')
      });
      return;
  } else if (phoneNumber != '' && formatedPhoneNumber == '') {
      dialogsModule.alert({
          title: global.L('demographic_data', 'alert_title'),
          message: global.L('demographic_data', 'alert_wrong_phone_number'),
          okButtonText: global.L('demographic_data', 'alert_btn')
      });
      return;
  }

  user.save();
  if (editing) {
      updateUserAnalytic();
      frameModule.topmost().navigate("views/parametres/parametres");
  } else {
      user.generateId();
      analytic.init(user);
      frameModule.topmost().navigate({
          moduleName: "views/home/home",
          clearHistory: true,
          animated: true
      });
  }
}

function updateUserAnalytic () {
  var mutableAttributes = ["pseudonym", "age", "gender", "occupation", "department", "zone", "speaking_language", "reading_language", "phoneNumber"];
  var editedAtributes = {"datetime": global.dateFormat(new Date())};
  var edited = false;
  previousUserData = JSON.parse(previousUserData);
  for(var i = 0; i < mutableAttributes.length; i++) {
    var key = mutableAttributes[i];
    if(previousUserData[key] !== user[key]) {
      edited = true;
      editedAtributes[key] = user[key];
    }
  }

  if (edited) {
    analytic.addUserData(editedAtributes);
  }
}

var frameModule = require("ui/frame");
var labelModule = require("ui/label");
var ObservableModule = require("data/observable");
var ObservableArray = require("data/observable-array").ObservableArray;
var userViewModel = require("~/shared/view-models/user-view-model")();
var lessonsData = require("~/data/fr/lessons");
var helper = require("~/utils/helper");
var slides = require("nativescript-slides/nativescript-slides");
var AnalyticViewModel = require("../../shared/view-models/analytic-view-model");

var page;
var thematicSectionContainer;
var lessonId;
var episodeNumber;
var thematic;
var currentPage = 0;
var drawer;
var analytic;

exports.pageLoaded = function (args) {
  global.initTimer();
  page = args.object;
  currentPage = 0;
  analytic = new AnalyticViewModel();
  lessonId = page.navigationContext.lessonId;
  episodeNumber = page.navigationContext.episodeNumber;

  initAnalytic();

  drawer = page.getViewById("sideDrawer");

  var lesson = helper.findByProperty(lessonsData, 'id', lessonId);

  thematic = new ObservableArray(lesson.episodes[episodeNumber].thematic);

  userViewModel.load();  
  userViewModel.saveLastPage('thematic', page.navigationContext);

  var isLastEpisode = false;
  var nextEpisodeProgress = 0;
  var nextEpisodeState;

  if (episodeNumber == lesson.episodes.length - 1) {
    isLastEpisode = true;
    nextEpisodeState = 'active';
  } else {
    nextEpisodeProgress = userViewModel.getProgressByEpisode(lessonId, episodeNumber + 1);
    nextEpisodeState = nextEpisodeProgress.progress > 0 ? '' : 'locked';
  }

  thematic.map(function (them, i) {
    them.current = false;
    them.index = i;

    if (i == thematic.length -1) {
      them.isLast = true;
    }

    thematic.setItem(i, them);

    if (i == currentPage) {
      them.current = true;
      thematic.setItem(i, them);
    }
    return them;
  });

  if (thematic.length > 1) {
    page.getViewById('nextBtn').visibility = 'visible';
  }

  page.bindingContext = new ObservableModule.fromObject({
    lessonIcon: lesson.icon,
    lessonTitle: lesson.title,
    episodeNumber: episodeNumber,
    thematic: thematic,
    episodesCount: lesson.episodes.length,
    episodeProgress: userViewModel.getEpisode(lessonId, episodeNumber),
    isLastEpisode: isLastEpisode,
    nextEpisodeState: nextEpisodeState,
    nextEpisodeProgress: nextEpisodeProgress,
    totalScore: userViewModel.progress.total_score,
    mascotSrc: "~/data/mascot/" + userViewModel.mascot_id + ".png",
  });

};

exports.prev = function prev(args) {
  currentPage--;
  page.getViewById('nextBtn').visibility = 'visible';

  if (currentPage <= 0) {
    currentPage = 0;
    page.getViewById('prevBtn').visibility = 'collapsed';
  }

  thematic.map(function (them, i) {
    them.current = false;
    them.index = i;

    thematic.setItem(i, them);

    if (i == currentPage) {
      them.current = true;
      thematic.setItem(i, them);
    }
    return them;
  });
}

exports.next = function next(args) {
  currentPage++;
  page.getViewById('prevBtn').visibility = 'visible';
  if (currentPage >= thematic.length -1) {
    currentPage = thematic.length -1;
    page.getViewById('nextBtn').visibility = 'collapsed';
  }

  thematic.map(function (them, i) {
    them.current = false;
    them.index = i;

    thematic.setItem(i, them);

    if (i == currentPage) {
      them.current = true;
      thematic.setItem(i, them);
    }
    return them;
  });
};

exports.goToQuiz = function () {
  userViewModel.completeThematic(lessonId, episodeNumber);
  saveAnalytic();
  frameModule.topmost().navigate({
    moduleName: "views/quiz/quiz",
    context: {
      lessonId: page.navigationContext.lessonId,
      episodeNumber: page.navigationContext.episodeNumber,
    },
    animated: true
  });
};

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

function initAnalytic () {
  global.activity = {
    datetime: global.dateFormat(global.startTime),
    type: "themes",
    uiLocale: "fr-BJ",
    lessonNumber: parseInt(lessonId),
    partNumber: parseInt(episodeNumber) + 1
  };
}

function saveAnalytic () {
  if (!global.activity) {
    return;
  }
  global.activity.duration = global.stopTimer();
  analytic.addActivityData(global.activity);
}

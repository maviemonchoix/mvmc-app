var frameModule = require("ui/frame");
var observableModule = require("data/observable");
var webViewModule = require("tns-core-modules/ui/web-view");
var platformModule = require("tns-core-modules/platform");
var page;
var drawer;

exports.loaded = function(args) {
  page = args.object;
  drawer = page.getViewById("sideDrawer");
  pageData = new observableModule.fromObject({
    screenHeight:  platformModule.screen.mainScreen.heightPixels - platformModule.screen.mainScreen.heightDIPs
  });
	page.bindingContext = pageData;
};

exports.webViewLoaded = function(args) {
  var webView = args.object;
  if (webView.android) {
    webView.android.getSettings().setDomStorageEnabled(true);
    webView.android.getSettings().setDatabaseEnabled(true);
    webView.android.getSettings().setBuiltInZoomControls(false);
  }
};

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

var frameModule = require("ui/frame");
var observableModule = require("data/observable");
var userViewModel = require("../../shared/view-models/user-view-model")();
var faqData = require("~/data/fr/faq");
var observableArray = require("data/observable-array");

var page;
var pageData = new observableModule.Observable();
var drawer;
var faqThematics;
exports.loaded = function(args) {
  page = args.object;
  drawer = page.getViewById("sideDrawer");
  faqThematics = faqData.map(function(obj) {
    return {
      id: obj.id,
      title: obj.title,
      questions: obj.questions,
      showQuestions: false
    };
  });
  pageData.set('faqThematics', new observableArray.ObservableArray(faqThematics));
  page.bindingContext = pageData;
};

exports.toggle = function(args) {
  for (var i = 0; i < faqThematics.length; i++) {
    if(i != args.object.thematicId-1) faqThematics[i].showQuestions = false;
  }
  faqThematics[args.object.thematicId-1].showQuestions = !faqThematics[args.object.thematicId-1].showQuestions;
  pageData.set('faqThematics', new observableArray.ObservableArray(faqThematics));
}

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

var frameModule = require("ui/frame");
var labelModule = require("ui/label");
var ObservableModule = require("data/observable");
var ObservableArray = require("data/observable-array").ObservableArray;
var userViewModel = require("~/shared/view-models/user-view-model")();
var lessonsData = require("~/data/fr/lessons");
var helper = require("~/utils/helper");
var slides = require("nativescript-slides/nativescript-slides");
var AnalyticViewModel = require("../../shared/view-models/analytic-view-model");

var page;
var thematicSectionContainer;
var lessonId;
var episodeNumber;
var quiz;
var currentPage = 0;
var answeredQuestions = [];
var drawer;
var analytic;


exports.pageLoaded = function (args) {
  currentPage = 0;
  answeredQuestions = [];

  page = args.object;
  analytic = new AnalyticViewModel();
  lessonId = page.navigationContext.lessonId;
  episodeNumber = page.navigationContext.episodeNumber;

  drawer = page.getViewById("sideDrawer");

  var lesson = helper.findByProperty(lessonsData, 'id', lessonId);

  var quizTitle = lesson.episodes[episodeNumber].quiz.title;
  var quizData = lesson.episodes[episodeNumber].quiz.questions;

  quizData.map(function (question, i) {
    question.rightAnswerMascotSrc = "~/data/mascot/" + userViewModel.mascot_id + "-right-answer.png";
    question.wrongAnswerMascotSrc = "~/data/mascot/" + userViewModel.mascot_id + "-wrong-answer.png";
    if (i == quizData.length -1) {
      question.isLast = true;
    }

    return question;
  });

  quiz = new ObservableArray(quizData);

  userViewModel.load();
  userViewModel.saveLastPage('quiz', page.navigationContext);

  var isLastEpisode = false;
  var nextEpisodeProgress = 0;
  var nextEpisodeState;

  if (episodeNumber == lesson.episodes.length - 1) {
    isLastEpisode = true;
    nextEpisodeState = 'active';
  } else {
    nextEpisodeProgress = userViewModel.getProgressByEpisode(lessonId, episodeNumber + 1);
    nextEpisodeState = nextEpisodeProgress.progress > 0 ? '' : 'locked';
  }

  page.bindingContext = new ObservableModule.fromObject({
    lessonIcon: lesson.icon,
    lessonTitle: lesson.title,
    episodeNumber: episodeNumber,
    quizTitle: quizTitle,
    quiz: quiz,
    episodesCount: lesson.episodes.length,
    episodeProgress: userViewModel.getEpisode(lessonId, episodeNumber),
    isLastEpisode: isLastEpisode,
    nextEpisodeState: nextEpisodeState,
    nextEpisodeProgress: nextEpisodeProgress,
    totalScore: userViewModel.progress.total_score,
    mascotSrc: "~/data/mascot/" + userViewModel.mascot_id + ".png",
  });

  page.getElementsByClassName('quiz-section-slide')[0].visibility = 'visible';
};

var prev = function (args) {
  page.getElementsByClassName('quiz-section-slide')[currentPage].visibility = 'collapsed';
  currentPage--;

  page.getViewById('nextBtn').visibility = 'collapsed';
  if (currentPage <= 0) {
    currentPage = 0;
    page.getViewById('prevBtn').visibility = 'collapsed';
  }

  page.getElementsByClassName('quiz-section-slide')[currentPage].visibility = 'visible';
}

var next = function (args) {
  if (page.getElementsByClassName('quiz-section-slide')
    [currentPage].getElementsByClassName('active').length <= 0) {
      return;
  };
  page.getElementsByClassName('quiz-section-slide')[currentPage].visibility = 'collapsed';
  currentPage++;

  page.getViewById('prevBtn').visibility = 'collapsed';
  if (currentPage >= quiz.length -1) {
    currentPage = quiz.length -1;
    page.getViewById('nextBtn').visibility = 'collapsed';
  }

  page.getElementsByClassName('quiz-section-slide')[currentPage].visibility = 'visible';
  global.initTimer();
};

exports.prev = prev;
exports.next = next;

exports.check = function (args) {
  saveAnalytic(currentPage, quizAnswer(currentPage));
  if (page.getElementsByClassName('quiz-section-slide')
    [currentPage].getElementsByClassName('active').length <= 0) {
      return;
  };

  page.getElementsByClassName('quiz-section-slide')
  [currentPage].getElementsByClassName('active').forEach(function (elm) {
    if (elm.dvalue == "true") {
      elm.classList.add('true-answer');
      page.getElementsByClassName('quiz-section-slide')
      [currentPage].getViewById('rightAnswerMascot').visibility = "visible";
    } else {
      elm.classList.add('false-answer');
      page.getElementsByClassName('quiz-section-slide')
      [currentPage].getViewById('wrongAnswerMascot').visibility = "visible";
    }
  });

  page.getElementsByClassName('quiz-section-slide')
  [currentPage].getViewById('response-block').visibility = "visible";

  args.object.visibility = "collapsed";

  if (args.object.parent.isLast) {
    args.object.parent.getViewById('btnSubmit').visibility = 'visible';
  } else {
    args.object.parent.getViewById('btnNext').visibility = 'visible';
    args.object.parent.getViewById('btnNext').classList.remove('disabled');
  }
  global.initTimer();
  initAnalytic();
};

exports.onAnswerTap = function (args) {
  var element = args.object;
  page.getElementsByClassName('quiz-section-slide')
  [currentPage].getElementsByClassName('true-answer').forEach(function (elm) {
    elm.classList.remove('true-answer');
  });
  page.getElementsByClassName('quiz-section-slide')
  [currentPage].getElementsByClassName('false-answer').forEach(function (elm) {
    elm.classList.remove('false-answer');
  });

  element.parent.parent.parent.parent.parent.getViewById('btnSubmit').visibility = 'collapsed';
  element.parent.parent.parent.parent.parent.getViewById('btnNext').visibility = 'collapsed';
  element.parent.parent.parent.parent.parent.getViewById('btnCheck').visibility = 'visible';
  element.parent.parent.parent.parent.parent.getViewById('btnCheck').classList.remove('disabled');

  page.getElementsByClassName('quiz-section-slide')
  [currentPage].getViewById('rightAnswerMascot').visibility = "collapsed";
  page.getElementsByClassName('quiz-section-slide')
  [currentPage].getViewById('wrongAnswerMascot').visibility = "collapsed";

  element.parent.getElementsByClassName('active').forEach(function(elm) {
    elm.classList.remove('active');
  }, this);

  element.classList.add('active');

  if (answeredQuestions[parseInt(element.parentId) - 1]) {
    var pos = parseInt(element.parentId) - 1;
    answeredQuestions[pos].qLabel= element.dlabel;
    answeredQuestions[pos].ansKey = element.dkey;
    answeredQuestions[pos].ansText = element.dtext;
    answeredQuestions[pos].ansValue = element.dvalue;
  } else {
    answeredQuestions.push({
      qId: element.parentId,
      qLabel: element.dlabel,
      ansKey: element.dkey,
      ansText: element.dtext,
      ansValue: element.dvalue,
    });
  }
};

exports.submitQuestion = function () {

  var isAllCorrect = true;
  for (var i = 0; i < answeredQuestions.length; i++) {
    userViewModel.answerQuizQuestion(lessonId, episodeNumber,
      answeredQuestions[i].qId,
      answeredQuestions[i].qLabel,
      answeredQuestions[i].ansKey,
      answeredQuestions[i].ansText,
      answeredQuestions[i].ansValue);
      if (answeredQuestions[i].ansValue == "false") {
        isAllCorrect = false;
      }
  };

  page.getViewById('quizView').visibility = 'collapsed';
  page.getViewById('feedbackView').visibility = 'visible';
  page.getViewById('titleQuiz').visibility = 'collapsed';

  if (isAllCorrect) {
    userViewModel.addToTotalScore(50);
    userViewModel.completeQuiz(lessonId, episodeNumber);
    page.getViewById('failFeedback').visibility = 'collapsed';
    page.getViewById('successFeedback').visibility = 'visible';
  } else {
    page.getViewById('failFeedback').visibility = 'visible';
    page.getViewById('successFeedback').visibility = 'collapsed';
  }
};

exports.start = function () {
  page.getViewById('prevBtn').visibility = 'collapsed';
  page.getViewById('nextBtn').visibility = 'visible';
  page.getViewById('quizView').visibility = 'visible';
  page.getViewById('feedbackView').visibility = 'collapsed';
  page.getViewById('failFeedback').visibility = 'collapsed';
  page.getViewById('successFeedback').visibility = 'collapsed';
  global.initTimer();
  initAnalytic();
};

exports.replay = function () {
  // answeredQuestions = [];
  currentPage = 0;
  page.getElementsByClassName('quiz-section-slide').forEach(function (elm, i) {
    if (i == 0) {
      elm.visibility = 'visible';
    } else {
      elm.visibility = 'collapsed';
    }
  });

  page.getElementsByClassName('quiz-section-slide').forEach(function (elm) {
    elm.getViewById('response-block').visibility = "collapsed";
  });

  page.getViewById('prevBtn').visibility = 'collapsed';
  page.getViewById('nextBtn').visibility = 'collapsed';
  page.getViewById('quizView').visibility = 'collapsed';
  page.getViewById('feedbackView').visibility = 'visible';
  page.getViewById('titleQuiz').visibility = 'visible';
  page.getViewById('failFeedback').visibility = 'collapsed';
  page.getViewById('successFeedback').visibility = 'collapsed';
};

exports.goToEpisode = function (args) {
  userViewModel.completeQuiz(lessonId, episodeNumber);
  frameModule.topmost().navigate({
    moduleName: "views/episode/episode",
    context: {
      lessonId: lessonId,
    },
    animated: true
  });
}

exports.goToHome = function (args) {
  frameModule.topmost().navigate("views/home/home");
}

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

function initAnalytic () {
  global.activity = {
    datetime: global.dateFormat(global.startTime),
    type: "quiz",
    uiLocale: "fr-BJ",
    lessonNumber: parseInt(lessonId),
    partNumber: parseInt(episodeNumber) + 1,
    answers: []
  };
}

function saveAnalytic (questionNumber, answers) {
  if (!global.activity) {
    return;
  }
  global.activity.duration = global.stopTimer();
  global.activity.questionNumber = parseInt(questionNumber) + 1;
  global.activity.answers.push(parseInt(answers));
  analytic.addActivityData(global.activity);
}

function quizAnswer (qId) {
  if (!answeredQuestions[parseInt(qId)]){
    return;
  }
  return answeredQuestions[parseInt(qId)]["ansKey"];
}

var frameModule = require("ui/frame");
var Observable = require("data/observable").Observable;
var userViewModel = require("../../shared/view-models/user-view-model")();
var AnalyticViewModel = require("../../shared/view-models/analytic-view-model")();

var slideContainer;
var activechoice;
var mascots = ["m1","m2"];
var index = 0;
var editing = false;
var previousUserData;

exports.pageLoaded = function pageLoaded(args) {
  page = args.object;
  page.bindingContext = new Observable();
  slideContainer = page.getViewById('mascot-slideContainer');
  activechoice = page.getViewById(mascots[index]);
  editing = page.navigationContext && page.navigationContext.editing;
  if (editing) {
    activechoice = page.getViewById(userViewModel.mascot_id);
    slideContainer.goToSlide(mascots.indexOf(userViewModel.mascot_id));
    previousUserData = JSON.stringify(userViewModel._map);
    page.getViewById('mascotName').text = userViewModel.mascot_name;
    page.getViewById('mascotChoice').text = global.L('mascotte', 'save');
  }
}

exports.next = function next(args) {
  if(index == mascots.length - 1){
    index = -1;
  }

  index++;
  activechoice = page.getViewById(mascots[index]);
  activechoice.parent.classList.remove("selected");
  slideContainer.nextSlide();
  page.getViewById('nameField').visibility = 'collapsed';
}

exports.prev = function next(args) {
  if(index == 0){
    index = mascots.length;
  }

  index--;
  activechoice = page.getViewById(mascots[index]);
  activechoice.parent.classList.remove("selected");
  slideContainer.previousSlide();
  page.getViewById('nameField').visibility = 'collapsed';
}

exports.choice = function choice(args) {
  activechoice = args.object;
  if (!activechoice.parent.classList.contains("selected")) {
    activechoice.parent.classList.add("selected");
    page.getViewById('nameField').visibility = 'visible';
  }
  else {
    activechoice.parent.classList.remove("selected");
    page.getViewById('nameField').visibility = 'collapsed';
  }
}

exports.choiceSubmit = function choiceSubmit() {
  var values = page.getViewById('mascotName').text, mascotId = activechoice.id;

  if (values === "" || page.getViewById('nameField').visibility != 'visible') {
    activechoice.parent.classList.add("selected");
    page.getViewById('nameField').visibility = 'visible';
  }
  else {
    userViewModel.updateAttribute('mascot_id', mascotId);
    userViewModel.updateAttribute('mascot_name', values);
    if (editing) {
      updateUserAnalytic();
      frameModule.topmost().navigate("views/parametres/parametres");
    } else {
      frameModule.topmost().navigate("views/demographic_data/demographic_data");
    }
  }
}

function updateUserAnalytic () {

  var mutableAttributes = ["mascot_id", "mascot_name"];
  var editedAtributes = {"datetime": global.dateFormat(new Date())};
  var edited = false;
  previousUserData = JSON.parse(previousUserData);
  for(var i = 0; i < mutableAttributes.length; i++) {
    var key = mutableAttributes[i];
    if(previousUserData[key] !== userViewModel[key]) {
      edited = true;
      editedAtributes[key] = userViewModel[key];
    }
  }

  if (edited) {
    AnalyticViewModel.addUserData(editedAtributes);
  }
}

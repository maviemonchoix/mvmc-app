var frameModule = require("ui/frame");
var observableModule = require("data/observable");
var userViewModel = require("../../shared/view-models/user-view-model")();
var lessonsData = require("~/data/fr/lessons");
var page;

var lessonList;
var pageData;
var drawer;

exports.loaded = function(args) {
  page = args.object;
  userViewModel.load();
  userViewModel.saveLastPage('home', page.navigationContext);

  if(userViewModel.FCMToken == '') {
    userViewModel.initFirebase();
  }

  drawer = page.getViewById("sideDrawer");

  lessonList = lessonsData.map(function(obj) {
    return {
      id: obj.id,
      title: obj.title,
      icon: obj.icon,
      progress: userViewModel.getProgressByLesson(obj.id)
    };
  });

  pageData = new observableModule.fromObject({
    isFirstLoad: userViewModel.isFirstLoad(),
    pseudo: userViewModel.pseudonym,
    lessonList: lessonList,
    totalScore: userViewModel.progress.total_score,
    overallProgress: userViewModel.getOverallProgress(),
    mascotSrc: "~/data/mascot/"+userViewModel.mascot_id+".png"
  });

  page.bindingContext = pageData;
};

exports.goToEpisode = function (args) {
  frameModule.topmost().navigate({
    moduleName: "views/episode/episode",
    context: {
      lessonId: args.object.lessonId,
    },
    animated: true
  });
}

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

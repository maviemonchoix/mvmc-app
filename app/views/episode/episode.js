var frameModule = require("ui/frame");
var ItemSpec = require("ui/layouts/grid-layout").ItemSpec;
var ObservableModule = require("data/observable");
var userViewModel = require("~/shared/view-models/user-view-model")();
var lessonsData = require("~/data/fr/lessons");
var helper = require("~/utils/helper");
var fileSystemModule = require("tns-core-modules/file-system");
var DownloadProgress = require("nativescript-download-progress").DownloadProgress;

var page;
var episodesList;
var lessonId;
var lesson

exports.pageLoaded = function (args) {
  page = args.object;
  lessonId = page.navigationContext.lessonId;
  lesson = helper.findByProperty(lessonsData, 'id', lessonId);

  drawer = page.getViewById("sideDrawer");

  userViewModel.load();
  userViewModel.startLesson(lessonId);

  userViewModel.saveLastPage('episode', page.navigationContext);

  var prevProgress;
  var prevClassThumbs;

  var episodesList = lesson.episodes.map(function(obj, i, arr) {
    var prog = userViewModel.getProgressByEpisode(lessonId, i);
    var nextProg = userViewModel.getProgressByEpisode(lessonId, i + 1);
    var classProgress = 'progress-episode';
    var classThumbs = 'video-thumbs-wrapper';
    var imageLink = obj.imageLink;


    if (prog == 0 && i != 0 && prevProgress != 100) {
      classProgress += ' locked';
      classThumbs += ' locked';
      imageLink = obj.imageLinkLocked;
    }

    if (nextProg == 0 && prog < 100 &&
        prevClassThumbs != 'video-thumbs-wrapper locked' &&
        prevClassThumbs != 'video-thumbs-wrapper active' &&
        prevClassThumbs != 'video-thumbs-wrapper locked active') {
      classProgress += ' active';
      classThumbs += ' active';
    }

    prevProgress = prog;
    prevClassThumbs = classThumbs;

    return {
      number: i,
      id: i + 1,
      image: imageLink,
      rowLength: '*,*',
      classProgress: classProgress,
      classThumbs: classThumbs,
      progress: prog
    };
  });

  var episodesListLayout = page.getViewById('episodesListLayout');

  for (var i = 0; i < Math.round(episodesList.length / 2); i++) {
    episodesListLayout.addRow(new ItemSpec(1, "auto"));
  }

  page.bindingContext = new ObservableModule.fromObject({
    lessonIcon: lesson.icon,
    lessonTitle: lesson.title,
    isCompleted: Math.round(userViewModel.getProgressByLesson(lessonId)) == 100,
    episodesList: episodesList,
    totalScore: userViewModel.progress.total_score,
    mascotSrc: "~/data/mascot/" + userViewModel.mascot_id + ".png",
    progress: Math.round(userViewModel.getProgressByLesson(lessonId))
  });
};

exports.showVideo = function (args) {
  var obj = args.object;
  var videoPath = fileSystemModule.path.join(fileSystemModule.knownFolders.currentApp().path, lesson.episodes[args.object.episodeNumber].video.file.replace('~', ''));
  if (!obj.classList.contains('locked')) {
    if(fileSystemModule.File.exists(videoPath)) {
      frameModule.topmost().navigate({
        moduleName: "views/video/video",
        context: {
          lessonId: page.navigationContext.lessonId,
          episodeNumber: args.object.episodeNumber,
        },
        animated: true
      });
    } else {
      var pathParts = lesson.episodes[args.object.episodeNumber].video.file.split('/');
      // var videoName = pathParts.pop();
      pathParts.shift();

      var download = new DownloadProgress();
      var url = "https://s3.amazonaws.com/mobile-app-mvmc/lessons/" + lesson.episodes[args.object.episodeNumber].video.file.replace('~/data/', '');
      var path = fileSystemModule.path.join(fileSystemModule.knownFolders.currentApp().path, pathParts.join('/'));
      download.addProgressCallback(function(progress) {
        var percentage = Math.ceil(progress*100);
        if(percentage > 100) percentage = 100;
        page.getViewById('downloadProgression').visibility = "visible";
        page.getViewById('downloadPercentage').text = percentage + "%";
      });
      download.downloadFile(url, path).then(function(f){
        frameModule.topmost().navigate({
          moduleName: "views/video/video",
          context: {
            lessonId: page.navigationContext.lessonId,
            episodeNumber: args.object.episodeNumber,
          },
          animated: true
        });
      }).catch(function(e){});
    }
  }
}

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

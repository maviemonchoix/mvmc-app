var frameModule = require("ui/frame");  
var page;
var UserViewModel = require("../../shared/view-models/user-view-model")();
var Observable = require("data/observable");

exports.pageLoaded = function pageLoaded(args) {
  page = args.object;
    UserViewModel.loadAndExec(function (isLoaded) {
        page.bindingContext = new Observable.fromObject({
            showPageContent: UserViewModel.id ? false : true
        });
        if (UserViewModel.id){
            frameModule.topmost().navigate({
                moduleName: "views/home/home",
                animated: true,
                clearHistory: true,
            });
        }

    });
}


// Event handler for Page "navigatingTo" event attached in main-page.xml

exports.goToMascotte = function() {
  frameModule.topmost().navigate("views/mascotte/mascotte");
};
var frameModule = require("ui/frame");
var ObservableModule = require("data/observable");
var userViewModel = require("~/shared/view-models/user-view-model")();
var lessonsData = require("~/data/fr/lessons");
var application = require('application');
var orientation = require('nativescript-orientation');
var appAvailability = require("nativescript-appavailability");
var helper = require("~/utils/helper");
var sharer = require("~/utils/sharer");
var AnalyticViewModel = require("../../shared/view-models/analytic-view-model");
var platformModule = require("platform");

var page;
var lessonId;
var episodeNumber;
var videoPlayer;
var playButton;
var drawer;
var analytic;
var currentTime = 0;
var videoRatio = 1.769709544;
var langSwitcher;
var labelLangSwitch;
var frenchVideoFile;
var fonVideoFile;
var currentSpeakingLang = 'fr';

var onPaused = function () {
  currentTime = videoPlayer.getCurrentTime();
};

var onPlayed = function () {
  if (currentTime > 1000) {
    videoPlayer.seekToTime(currentTime);
    videoPlayer.play();
    playButton.visibility = 'hidden';
  }
};

function setOrientation(args) {
  if (args.newValue == 'landscape') {
    page.bindingContext.set('isFullscreen', true);
    orientation.setFullScreen(true);
    page.getViewById('actionBar').visibility = 'collapsed';
    page.getViewById('nativeVideoPlayer').top = -40;
    page.getViewById('nativeVideoPlayerWrapper').paddingTop = 0;
    page.getViewById('nativeVideoPlayer').height = (platformModule.screen.mainScreen.heightDIPs * 280) / platformModule.screen.mainScreen.widthDIPs;
    page.getViewById('nativeVideoPlayerWrapper').width = platformModule.screen.mainScreen.heightDIPs;
    page.getViewById('nativeVideoPlayerWrapper').height = platformModule.screen.mainScreen.widthDIPs;
  } else {
    page.bindingContext.set('isFullscreen', false);
    orientation.setFullScreen(false);
    page.getViewById('nativeVideoPlayer').top = 0;
    page.getViewById('actionBar').visibility = 'visible';
    page.getViewById('nativeVideoPlayer').height = 280;
    page.getViewById('nativeVideoPlayerWrapper').width = platformModule.screen.mainScreen.widthDIPs;
    page.getViewById('nativeVideoPlayerWrapper').height = 280;
  }
}

exports.pageLoaded = function (args) {

  page = args.object;
  analytic = new AnalyticViewModel();
  lessonId = page.navigationContext.lessonId;
  episodeNumber = page.navigationContext.episodeNumber;

  drawer = page.getViewById("sideDrawer");

  playButton = page.getViewById('playButton');
  langSwitcher = page.getViewById('langSwitcher');
  labelLangSwitch = page.getViewById('labelLangSwitch');
  videoPlayer = page.getViewById('nativeVideoPlayer');
  videoPlayer.controls = false;

  if (currentSpeakingLang == 'fr') {
    labelLangSwitch.text = global.L('lesson-content', 'langSwitchFon');
  } else {
    labelLangSwitch.text = global.L('lesson-content', 'langSwitchFrench');
  }

  userViewModel.load();
  userViewModel.startEpisode(lessonId, episodeNumber);
  userViewModel.saveLastPage('video', page.navigationContext);

  var lesson = helper.findByProperty(lessonsData, 'id', lessonId);
  var isLastEpisode = false;
  var nextEpisodeProgress = 0;
  var nextEpisodeState;

  if (episodeNumber == lesson.episodes.length - 1) {
    isLastEpisode = true;
    nextEpisodeState = 'active';
  } else {
    nextEpisodeProgress = userViewModel.getProgressByEpisode(lessonId, episodeNumber + 1);
    nextEpisodeState = nextEpisodeProgress.progress > 0 ? '' : 'locked';
  }

  if(userViewModel.getEpisode(lessonId, episodeNumber).video) {
    page.getViewById('btnNext').classList.remove('disabled');
  }

  page.bindingContext = new ObservableModule.fromObject({
    isFullscreen: false,
    lessonIcon: lesson.icon,
    lessonTitle: lesson.title,
    episodeNumber: episodeNumber,
    episodeData: lesson.episodes[episodeNumber],
    episodesCount: lesson.episodes.length,
    episodeProgress: userViewModel.getEpisode(lessonId, episodeNumber),
    isLastEpisode: isLastEpisode,
    nextEpisodeState: nextEpisodeState,
    nextEpisodeProgress: nextEpisodeProgress,
    totalScore: userViewModel.progress.total_score,
    mascotSrc: "~/data/mascot/" + userViewModel.mascot_id + ".png",
  });

  if (lesson.episodes[episodeNumber].video.fileFon) {
    frenchVideoFile = lesson.episodes[episodeNumber].video.file;
    fonVideoFile = lesson.episodes[episodeNumber].video.fileFon;
    langSwitcher.visibility = 'visible';
  } else {
    langSwitcher.visibility = 'collapsed';
  }

  appAvailability.available("com.facebook.katana").then(function(avail) {
    if (!avail) {
      page.getViewById('fbShare').visibility = 'collapsed';
    }
  })

  appAvailability.available("com.whatsapp").then(function(avail) {
    if (!avail) {
      page.getViewById('whaShare').visibility = 'collapsed';
    }
  });

  appAvailability.available("com.facebook.orca").then(function(avail) {
    if (!avail) {
      page.getViewById('msShare').visibility = 'collapsed';
    }
  });

  if (application.android) {
    application.android.on(application.AndroidApplication.activityPausedEvent, onPaused);
    application.android.on(application.AndroidApplication.activityResumedEvent, onPlayed);
  }

  application.on(application.orientationChangedEvent, setOrientation);
};

exports.pageUnloaded = function() {
  application.android.off(application.AndroidApplication.activityPausedEvent, onPaused);
  application.android.off(application.AndroidApplication.activityResumedEvent, onPlayed);
  application.off(application.orientationChangedEvent, setOrientation);
}

exports.goToDialogue = function (args) {
  if (args.object.classList.contains('disabled')) {
    return;
  }
  userViewModel.completeVideo(lessonId, episodeNumber);
  saveAnalytic();
  frameModule.topmost().navigate({
    moduleName: "views/dialogue/dialogue",
    context: {
      lessonId: page.navigationContext.lessonId,
      episodeNumber: page.navigationContext.episodeNumber,
    },
    animated: true
  });
};

exports.videoFinished = function () {
  playButton.visibility = 'visible';
  page.getViewById('btnNext').classList.remove('disabled');
};

exports.playVideo = function (args) {
  initAnalytic();
  playButton.visibility = 'hidden';
  videoPlayer.play();
};

exports.pauseVideo = function (args) {
  playButton.visibility = 'visible';
  videoPlayer.pause();
};

exports.shareToFacebook = function () {
  var type = currentSpeakingLang == 'fr' ? "facebookshare" : "fonfacebookshare";
  var activity = {
    datetime: global.dateFormat(new Date()),
    type: type,
    uiLocale: "fr-BJ",
    duration: 0,
    lessonNumber: parseInt(lessonId),
    partNumber: parseInt(episodeNumber) + 1
  };
  analytic.addActivityData(activity);
  var url = page.bindingContext.episodeData.video.file.replace('~', 'app');
  sharer.shareVideo(url, "com.facebook.katana");
};

exports.shareToWhatsapp = function () {
  var type = currentSpeakingLang == 'fr' ? "whatsappshare" : "fonwhatsappshare";
  var activity = {
    datetime: global.dateFormat(new Date()),
    type: type,
    uiLocale: "fr-BJ",
    duration: 0,
    lessonNumber: parseInt(lessonId),
    partNumber: parseInt(episodeNumber) + 1
  };
  analytic.addActivityData(activity);
  var url = page.bindingContext.episodeData.video.file.replace('~', 'app');
  sharer.shareVideo(url, "com.whatsapp");
};

exports.shareToMessenger = function () {
  var type = currentSpeakingLang == 'fr' ? "messengershare" : "fonmessengershare";
  var activity = {
    datetime: global.dateFormat(new Date()),
    type: type,
    uiLocale: "fr-BJ",
    duration: 0,
    lessonNumber: parseInt(lessonId),
    partNumber: parseInt(episodeNumber) + 1
  };
  analytic.addActivityData(activity);
  var url = page.bindingContext.episodeData.video.file.replace('~', 'app');
  sharer.shareVideo(url, "com.facebook.orca");
};

exports.shareToAll = function () {
  var type = currentSpeakingLang == 'fr' ? "share" : "fonshare";
  var activity = {
    datetime: global.dateFormat(new Date()),
    type: type,
    uiLocale: "fr-BJ",
    duration: 0,
    lessonNumber: parseInt(lessonId),
    partNumber: parseInt(episodeNumber) + 1
  };
  analytic.addActivityData(activity);
  var url = page.bindingContext.episodeData.video.file.replace('~', 'app');
  sharer.shareVideo(url);
};

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

exports.switchLang = function () {
  saveAnalytic();
  if (currentSpeakingLang == 'fr') {
    labelLangSwitch.text = global.L('lesson-content', 'langSwitchFrench');
    currentSpeakingLang = 'fon';
    videoPlayer.src = fonVideoFile;
  } else {
    labelLangSwitch.text = global.L('lesson-content', 'langSwitchFon');
    currentSpeakingLang = 'fr';
    videoPlayer.src = frenchVideoFile;
  }
  initAnalytic();
};

function initAnalytic () {
  global.initTimer();
  var type = currentSpeakingLang == 'fr' ? "video" : "fonvideo";
  global.activity = {
    datetime: global.dateFormat(global.startTime),
    type: type,
    uiLocale: "fr-BJ",
    lessonNumber: parseInt(lessonId),
    partNumber: parseInt(episodeNumber) + 1
  };
}

function saveAnalytic () {
  if (global.activity && global.activity !== "undefined" && global.activity !== null) {
   global.activity.duration = global.stopTimer();
   analytic.addActivityData(global.activity);
  }
}

var frameModule = require("ui/frame");
var page;
var Observable = require("data/observable").Observable;

var drawer;

exports.loaded = function(args) {
  page = args.object;
  drawer = page.getViewById("sideDrawer");
	page.bindingContext = new Observable();
};

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

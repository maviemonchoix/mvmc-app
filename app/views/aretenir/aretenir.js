var frameModule = require("ui/frame");
var labelModule = require("ui/label");
var ObservableModule = require("data/observable");
var ObservableArray = require("data/observable-array").ObservableArray;
var userViewModel = require("~/shared/view-models/user-view-model")();
var lessonsData = require("~/data/fr/lessons");
var helper = require("~/utils/helper");
var slides = require("nativescript-slides/nativescript-slides");
var AnalyticViewModel = require("../../shared/view-models/analytic-view-model");

var page;
var aretenirSectionContainer;
var lessonId;
var episodeNumber;
var aretenir;
var currentPage = 0;
var drawer;
var analytic;


exports.pageLoaded = function (args) {
  currentPage = 0;
  global.initTimer();
  page = args.object;
  analytic = new AnalyticViewModel();
  lessonId = page.navigationContext.lessonId;
  episodeNumber = page.navigationContext.episodeNumber;
  initAnalytic();

  drawer = page.getViewById("sideDrawer");

  var lesson = helper.findByProperty(lessonsData, 'id', lessonId);

  aretenir = new ObservableArray(lesson.episodes[episodeNumber].aretenir);

  userViewModel.load();
  userViewModel.saveLastPage('aretenir', page.navigationContext);  

  var isLastEpisode = false;
  var nextEpisodeProgress = 0;
  var nextEpisodeState;

  if (episodeNumber == lesson.episodes.length - 1) {
    isLastEpisode = true;
    nextEpisodeState = 'active';
  } else {
    nextEpisodeProgress = userViewModel.getProgressByEpisode(lessonId, episodeNumber + 1);
    nextEpisodeState = nextEpisodeProgress.progress > 0 ? '' : 'locked';
  }

  aretenir.map(function (them, i) {
    them.current = false;
    them.index = i;

    if (i == aretenir.length -1) {
      them.isLast = true;
    }

    aretenir.setItem(i, them);

    if (i == currentPage) {
      them.current = true;
      aretenir.setItem(i, them);
    }
    return them;
  });

  if (aretenir.length > 1) {
    page.getViewById('nextBtn').visibility = 'visible';
  }

  page.bindingContext = new ObservableModule.fromObject({
    lessonIcon: lesson.icon,
    lessonTitle: lesson.title,
    episodeNumber: episodeNumber,
    aretenir: aretenir,
    episodesCount: lesson.episodes.length,
    episodeProgress: userViewModel.getEpisode(lessonId, episodeNumber),
    isLastEpisode: isLastEpisode,
    nextEpisodeState: nextEpisodeState,
    nextEpisodeProgress: nextEpisodeProgress,
    totalScore: userViewModel.progress.total_score,
    mascotSrc: "~/data/mascot/" + userViewModel.mascot_id + ".png",
  });

};

exports.prev = function prev(args) {
  currentPage--;
  page.getViewById('nextBtn').visibility = 'visible';

  if (currentPage <= 0) {
    currentPage = 0;
    page.getViewById('prevBtn').visibility = 'collapsed';
  }

  aretenir.map(function (them, i) {
    them.current = false;
    them.index = i;

    aretenir.setItem(i, them);

    if (i == currentPage) {
      them.current = true;
      aretenir.setItem(i, them);
    }
    return them;
  });
}

exports.next = function next(args) {
  currentPage++;
  page.getViewById('prevBtn').visibility = 'visible';
  if (currentPage >= aretenir.length -1) {
    currentPage = aretenir.length -1;
    page.getViewById('nextBtn').visibility = 'collapsed';
  }

  aretenir.map(function (them, i) {
    them.current = false;
    them.index = i;

    aretenir.setItem(i, them);

    if (i == currentPage) {
      them.current = true;
      aretenir.setItem(i, them);
    }
    return them;
  });
};

exports.goToQuiz = function () {
  userViewModel.completeAretenir(lessonId, episodeNumber);
  saveAnalytic();
  frameModule.topmost().navigate({
    moduleName: "views/pacte/pacte",
    context: {
      lessonId: page.navigationContext.lessonId,
      episodeNumber: page.navigationContext.episodeNumber,
    },
    animated: true
  });
};

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;

function initAnalytic () {
  global.activity = {
    datetime: global.dateFormat(global.startTime),
    type: "review",
    uiLocale: "fr-BJ",
    lessonNumber: parseInt(lessonId),
    partNumber: 1
  };
}

function saveAnalytic () {
  if (!global.activity) {
    return;
  }
  global.activity.duration = global.stopTimer();
  analytic.addActivityData(global.activity);
}

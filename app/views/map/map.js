var frameModule = require("ui/frame");
var page;
var Observable = require("data/observable").Observable;
var mapsModule = require("nativescript-google-maps-sdk");

var markers = require("~/data/markers");

var drawer;

exports.loaded = function(args) {
  page = args.object;
  drawer = page.getViewById("sideDrawer");
  page.bindingContext = new Observable();
  page.bindingContext.set('latitude', 9.2295382);
  page.bindingContext.set('longitude', 2.300262);
  page.bindingContext.set('zoom', 6.8);
};

exports.onMapReady = function (args) {
  var mapView = args.object;
  for (var i = 0; i < markers.length; i++) {
    var markerData = markers[i];
    var marker = new mapsModule.Marker();
    marker.position = mapsModule.Position.positionFromLatLng(markerData.latitude, markerData.longitude);
    marker.title = markerData.title;
    marker.services = markerData.services;
    marker.contact = markerData.contact;
    marker.userData = { index : i + 1};
    mapView.addMarker(marker); 
  }
};

exports.quitApp = global.quitApp;

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.goTo = global.goTo;
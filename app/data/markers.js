module.exports = [
  {
    latitude: 11.2970599,
    longitude: 2.4196098,
    title: "SAINT MICHEL",
    services: "Infirmerie, Maternité, Pharmacie, Dépistage IST/VIH/SIDA, Planning Familial",
    contact: "Tél: 94111391 - 64860230"
  },
  {
    latitude: 10.2940373,
    longitude: 1.3783979,
    title: "CJAV CEG1 NATITINGOU",
    services: "Prévention et Dépistage des IST/VIH, Dépistage du cancer du col de l'utérus.",
    contact: "Tél: 94807398 - 61933611"
  },
  {
    latitude: 6.4313388,
    longitude: 2.3251104,
    title: "STE THERESE DE L'ENFANT JESUS",
    services: "Consultation générales, Consultation pré et post natales, Accouchement, Echographie.",
    contact: "Tél: 96343651-95754316"
  },
  {
    latitude: 9.9272783,
    longitude: 3.2040591,
    title: "Hôpital de zone SOUN",
    services: "Médécine générale, Gynécologie, ophtamologie, Odonto. Stomatologie, Pédiatrie.",
    contact: "Tél: 95892446-95299325"
  },
  {
    latitude: 7.7736127,
    longitude: 2.1825862,
    title: "CJAV DASSA",
    services: "Prévention et Dépistage des IST/VIH, Dépistage du cancer du col de l'utérus.",
    contact: "Tél: 96235006 - 61933606"
  },
  {
    latitude: 6.9473132,
    longitude: 1.7019404,
    title: "CMS ESPOIR VIE",
    services: "Médécine générale, Gynécologie, Pédiatrie, Petite chirugie, Service de santé en reproduction",
    contact: "Tél: 97259898-64806372"
  },
  {
    latitude: 11.2970599,
    longitude: 2.4196098,
    title: "SAINT MICHEL",
    services: "Consultations pré et post natales, Accouchement, Planning Familial, CD VIH/SIDA et IST.",
    contact: "Tél: 96141855-96233278"
  },
  {
    latitude: 9.711611,
    longitude: 1.6715956,
    title: "PSAMAO",
    services: "Infirmerie, Maternité, Pharmacie, Dépistage IST/VIH/SIDA, Planning Familial",
    contact: "Tél: 94111391 - 64860230"
  },
  {
    latitude: 6.3574395,
    longitude: 2.3699346,
    title: "CLINIQUE FIDJROSSE",
    services: "Consultations générales, Petites chirugies, Maternité, Pédiatrie, Laboratoire.",
    contact: "Tél: 95281311"
  },
  {
    latitude: 6.6398637,
    longitude: 1.7177956,
    title: "PADRE PIO",
    services: "Médécine générale, Gynécologie, ophtamologie, Odonto. Stomatologie, Pédiatrie.",
    contact: "Tél: 95564540"
  },
  {
    latitude: 6.3793652,
    longitude: 2.6178224,
    title: "LA REDEMPTION",
    services: "Soins curatifs, Maternité, Activités préventives, Pharmacie, Planning Familial",
    contact: "Tél: 97123901-62666602"
  },
  {
    latitude: 7.1717769,
    longitude: 2.0635854,
    title: "CLINIQUE SANITAS",
    services: "CPN, CPoN, Consultations générales, Pédiatrie, Echographie, Radiologie, Laboratoire.",
    contact: "Tél: 95981177 - 95843073"
  },
  {
    latitude: 6.3790877,
    longitude: 2.4628312,
    title: "ABMS/PSI (CPS Suru Léré Akpakpa)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 94 31 12 31"
  },
  {
    latitude: 6.3810421,
    longitude: 2.3617541,
    title: "ABMS/PSI (CEG les Pylônes d’Agla)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 96 34 42 95"
  },
  {
    latitude: 6.4416883,
    longitude: 2.3508274,
    title: "ABMS/PSI (CEG1 Abomey-Calavi)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 96 34 42 94"
  },
  {
    latitude: 6.4297556,
    longitude: 2.3260331,
    title: "ABMS/PSI (CEG plateau de Womey)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 96 34 42 90"
  },
  {
    latitude: 6.4960355,
    longitude: 2.5872795,
    title: "ABMS/PSI (CEG DKP de Porto-Novo)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 96 35 34 39"
  },
  {
    latitude: 6.5064664,
    longitude: 2.609985,
    title: "ABMS/PSI (Marché Ouando)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 61 93 36 04"
  },
  {
    latitude: 6.5653554,
    longitude: 2.4240271,
    title: "ABMS/PSI (Maison des jeunes de Dangbo)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 96 35 34 30"
  },
  {
    latitude: 8.0290028,
    longitude: 2.4785857,
    title: "ABMS/PSI (CEG 1 Savè)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 61 93 36 07"
  },
  {
    latitude: 7.7800922,
    longitude: 2.1795043,
    title: "ABMS/PSI (Maison du peuple de Dassa)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 61 93 36 06"
  },
  {
    latitude: 10.2940373,
    longitude: 1.3783979,
    title: "ABMS/PSI (CEG 1 Natitingou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 61 93 36 11"
  },
  {
    latitude: 10.3186836,
    longitude: 1.3839286,
    title: "ABMS/PSI (CPS de Natitingou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 61 93 36 12"
  },
  {
    latitude: 9.3840755,
    longitude: 2.6213027,
    title: "ABMS/PSI (CEG Guèma de Parakou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 61 93 36 09"
  },
  {
    latitude: 9.3492707,
    longitude: 2.5834672,
    title: "ABMS/PSI (CEG Albarika de Parakou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 61 93 36 08"
  },
  {
    latitude: 9.3368013,
    longitude: 2.6371545,
    title: "ABMS/PSI (Université de Parakou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 67 67 35 13"
  },
  {
    latitude: 9.7081748,
    longitude: 1.6487284,
    title: "ABMS/PSI (CEG 1 Djougou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 61 93 36 10"
  },
  {
    latitude: 8.9988539,
    longitude: 1.6692178,
    title: "ABMS/PSI (Arrondissement Bassila)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 62 38 93 75"
  },
  {
    latitude: 8.886894,
    longitude: 2.5744699,
    title: "ABMS/PSI (CPS Tchaourou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 61 00 92 28"
  },
  {
    latitude: 7.9306087,
    longitude: 1.9638919,
    title: "ABMS/PSI (CPS Savalou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 62 57 35 26"
  },
  {
    latitude: 9.8592172,
    longitude: 2.6998899,
    title: "ABMS/PSI (Derrière station SONACOP N’Dali)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 67 22 57 26"
  },
  {
    latitude: 10.880863,
    longitude: 2.2617591,
    title: "ABMS/PSI (Maison des Jeunes Gogounou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 96 78 87 33"
  },
  {
    latitude: 7.179178,
    longitude: 1.996078,
    title: "ABMS/PSI (En face du Lycée Houffon à Abomey)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: ""
  },
  {
    latitude: 7.180734,
    longitude: 2.070107,
    title: "ABMS/PSI (Sur le terrain de sport du CEG 1 de Bohicon)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: ""
  },
  {
    latitude: 7.066667,
    longitude: 1.966667,
    title: "ABMS/PSI (Agbanhizoun, Derrière la mairie, dans l'enceinte du CPS)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: ""
  },
  {
    latitude: 8.585349,
    longitude: 2.424932,
    title: "ABMS/PSI (A côté du CEG1 de Ouessè)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: ""
  },
  {
    latitude: 8.585349,
    longitude: 2.424932,
    title: "ABMS/PSI (Juste à 50 m de la mairie de Ségbana)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: ""
  },
  {
    latitude: 6.711481,
    longitude: 2.4255721,
    title: "OSV Jordan (Centre multifonctionnel d'Adjohoun)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 94842355 - 67861504 - 96836826"
  },
  {
    latitude: 6.8987901,
    longitude: 2.4515402,
    title: "OSV Jordan (Centre Convivial d'Affamè)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 95057113"
  },
  {
    latitude: 6.5660376,
    longitude: 2.4240271,
    title: "OSV Jordan (Centre Convivial de Gbéko)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 65408855"
  },
  {
    latitude: 6.9430625,
    longitude: 1.6679906,
    title: "OSV Jordan (Centre Convivial d'Aplahoué)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "96692341 - 95804380"
  },
  {
    latitude: 6.9064753,
    longitude: 1.5974776,
    title: "OSV Jordan (Centre Convivial de Djakotomey)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 97260186 - 95705040"
  },
  {
    latitude: 6.7997073,
    longitude: 1.7647217,
    title: "OSV Jordan (Centre Convivial de Dogbo)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 64659145 - 66451510"
  },
  {
    latitude: 6.3670384,
    longitude: 2.4467919,
    title: "ABPF (OHEE Derrière la Stade René Pleven)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 21 33 51 70"
  },
  {
    latitude: 6.3680489,
    longitude: 2.4116281,
    title: "ABPF (Clinique de Référence, Sikecodji)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 21 33 51 70"
  },
  {
    latitude: 6.7997073,
    longitude: 1.7647217,
    title: "ABPF (Dogbo)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: ""
  },
  {
    latitude: 10.2903057,
    longitude: 1.3810925,
    title: "ABPF (Yokossi)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 23 82 11 92"
  },
  {
    latitude: 6.5066093,
    longitude: 2.6089861,
    title: "ABPF (Ouando-Djado)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 20 24 78 14"
  },
  {
    latitude: 7.1842582,
    longitude: 2.0056271,
    title: "ABPF (GOHO)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 22 50 04 84"
  },
  {
    latitude: 7.1813726,
    longitude: 2.0533703,
    title: "ABPF (Bohicon)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: ""
  },
  {
    latitude: 9.3655971,
    longitude: 2.6280062,
    title: "ABPF (Tranza)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 23 61 05 59"
  },
  {
    latitude: 6.6583459,
    longitude: 1.7242175,
    title: "ABPF (Agnivedji)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: "Tél: 22 41 11 54"
  },
  {
    latitude: 7.9306087,
    longitude: 1.9638919,
    title: "ABPF (Savalou)",
    services: "Traitements des ISTs, Fourniture de services de contraception dont l'accès aux préservatifs, Jeux (Play-Station, Cyber-café; Jeux traditionnels).",
    contact: ""
  },
  {
    latitude: 6.909151,
    longitude: 2.452951,
    title: "OSV Jordan (Centre Convivial d'Affamè, Bonou)",
    services: "Sensibilisations sur les thématiques de la SRAJ et PF, Counseling et dépistage du VIH/sida, Counseling et l’offre des services de PF, Suivi post services, Jeux ludiques, Jeux sportifs, Projections vidéo, Cyber.",
    contact: "Tél: 95 05 71 13"
  },
  {
    latitude: 6.579980,
    longitude: 2.546794,
    title: "OSV Jordan (Centre Convivial de Gbéko, Dangbo)",
    services: "Sensibilisations sur les thématiques de la SRAJ et PF, Counseling et dépistage du VIH/sida, Counseling et l’offre des services de PF, Suivi post services, Jeux ludiques, Jeux sportifs, Projections vidéo, Cyber.",
    contact: "Tél: 96 30 17 86"
  },
  {
    latitude: 6.446099,
    longitude: 2.340142,
    title: "OSV Jordan (Centre Convivial de Kpanroun, Abomey-Calavi)",
    services: "Sensibilisations sur les thématiques de la SRAJ et PF, Counseling et dépistage du VIH/sida, Counseling et l’offre des services de PF, Suivi post services, Jeux ludiques, Jeux sportifs, Projections vidéo, Cyber.",
    contact: "Tél: 91 15 89 34 - 97 76 04 49"
  },
  {
    latitude: 6.522927,
    longitude: 2.409093,
    title: "OSV Jordan (Centre Convivial de Sô-Ava)",
    services: "Sensibilisations sur les thématiques de la SRAJ et PF, Counseling et dépistage du VIH/sida, Counseling et l’offre des services de PF, Suivi post services, Jeux ludiques, Jeux sportifs, Projections vidéo, Cyber.",
    contact: "Tél: 97 44 23 87"
  }
];

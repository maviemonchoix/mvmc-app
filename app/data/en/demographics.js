module.exports = {
    gender: ["Man", "Woman"],
    departments: ["Alibori", "Atakora", "Atlantique", "Borgou", "Collines", "Couffo",
        "Donga", "Littoral", "Mono", "Oueme", "Plateau", "Zou", "Outside of Benin"],
    occupation: ["School student", "University student", "Craftsman - Apprentice",
        "Peer educator", "Teacher", "Youth service providerJeunes", "Other"],
    zone: ["Urban", "Rural"],
    speaking_language: ["French", "English"],
    reading_language: ["French", "English"],
    dialog_gender:"Your gender",
    dialog_occupation:"Your occupation",
    dialog_department:"Your department",
    dialog_zone:"Your area",
    dialog_speaking_language:"Your speaking language",
    dialog_reading_language:"Your reading language",
    action_cancel:"Cancel"
};

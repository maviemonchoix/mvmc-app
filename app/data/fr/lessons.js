module.exports = [
  {
    "id": 1,
    "title": "Genre",
    "icon": "~/data/icons/icon-lesson-1.png",
    "episodes": [
      {
        "imageLink": "~/data/videos/E1/1-1.jpg",
        "imageLinkLocked": "~/data/videos/E1/1-1g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E1_low/FR_E1_S1.mp4",
          "fileFon": "~/data/fon/videos/FO_E1_low/FO_E1_S1.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "Que ferais-tu si tu étais à la place d’Assiba ?",
          "answers": [{
              "key": "complain-familly",
              "text": "Me plaindre au délégué du quartier ou à un membre de ma famille"
            },
            {
              "key": "get-help",
              "text": "Chercher de l’aide auprès d’un de mes professeurs"
            },
            {
              "key": "dont-know",
              "text": "Je ne sais pas"
            },
            {
              "key": "nothing",
              "text": "Je ne pourrais rien faire contre la volonté de mon père"
            }
          ]
        },
        "thematic": [{
            "title": "On entend le papa de Assiba dire ; « Dommage que tu n’es pas un garçon Assiba ». Mais pourquoi les filles et les garçons sont traités de manières différentes ?",
            "file": "image.png",
            "text": "<p>Le sexe a trait aux caractères physiques et biologiques de l’homme et de la femme, tandis que le genre concerne les caractères culturels et sociales attribués par la société a l’homme et à la femme.</p>" +
                    "<p>Le genre est un concept qui a trait aux qualités ou caractéristiques que la société fait jouer à chaque sexe. Il évolue selon les cultures et au fil du temps.</p>"
          },
          {
            "title": "Dans l’épisode on voit que le genre peut être lié à l’éducation. Assiba ne pourrait pas continuer ses études parce qu’elle est une fille.",
            "file": "image.png",
            "text": "<p>La Déclaration universelle des droits de l&#39;homme reconnait les droits des hommes et femmes. « Toute personne sans distinction de race, de sexe, de langue, de religion ou origine sociale ou autre, a droit à l’éducation. » art. 26.1 (DUDH)</p>" +
                    "<p>Autrement dit, filles comme garçons ont le même droit à l’éducation et le traitement en milieu scolaire doit être le même pour tous.</p>"
          }
        ],
        "quiz": {
          "title": "Indique si les déclarations font appel à la notion de genre ou à celle de sexe :",
          "questions": [
            {
              "id": 1,
              "question": "Les femmes allaitent les enfants.",
              "answers": [{
                  "key": "1",
                  "text": "Sexe",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Genre",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "Les hommes ne doivent pas pleurer.",
              "answers": [{
                  "key": "1",
                  "text": "Sexe",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Genre",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "Les hommes peuvent féconder.",
              "answers": [{
                  "key": "1",
                  "text": "Sexe",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Genre",
                  "value": "false"
                }
              ]
            },
            {
              "id": 4,
              "question": "Une femme doit assurer les tâches domestiques.",
              "answers": [{
                  "key": "1",
                  "text": "Sexe",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Genre",
                  "value": "true"
                }
              ]
            },
            {
              "id": 5,
              "question": "Toutes les charges familiales doivent revenir aux hommes.",
              "answers": [{
                  "key": "1",
                  "text": "Sexe",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Genre",
                  "value": "true"
                }
              ]
            },
            {
              "id": 6,
              "question": "Les hommes sont plus violents que les femmes.",
              "answers": [{
                  "key": "1",
                  "text": "Sexe",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Genre",
                  "value": "true"
                }
              ]
            },
            {
              "id": 7,
              "question": "Une femme instruite est une mauvaise épouse.",
              "answers": [{
                  "key": "1",
                  "text": "Sexe",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Genre",
                  "value": "true"
                }
              ]
            },
            {
              "id": 8,
              "question": "Les hommes peuvent avoir plusieurs épouses.",
              "answers": [{
                  "key": "1",
                  "text": "Sexe",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Genre",
                  "value": "true"
                }
              ]
            }
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E1/1-2.jpg",
        "imageLinkLocked": "~/data/videos/E1/1-2g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E1_low/FR_E1_S2.mp4",
          "fileFon": "~/data/fon/videos/FO_E1_low/FO_E1_S2.mp4"
        },

        "dialogue": {
          "id": 1,
          "question": "Si tu étais Lina, quelle proposition ferais-tu a Assiba ?",
          "answers": [{
              "key": "talk-to-me",
              "text": "Je proposerai à Assiba de parler moi-même avec les autorités compétentes."
            },
            {
              "key": "get-help",
              "text": "Je proposerai à Assiba de se rapprocher de la protection de l’enfance."
            },
            {
              "key": "talk-familly",
              "text": "Je proposerai à Assiba de parler avec un membre de la famille ou d’un enseignant."
            },
            {
              "key": "accept-her-fate",
              "text": "Je proposerai à Assiba d’accepter son destin."
            }
          ]
        },
        "thematic": [{
            "title": "Dans l’épisode on comprend pourquoi certaines idées sur le genre sont liées au mariage.",
            "file": "image.png",
            "text": "<p>Le mariage précoce est le fait de donner une fille ou un garçon mariage avant qu’ils ont 18 ans. Certaines pratiques traditionnelles, l’inégalité des sexes et la pauvreté sont des causes du mariage précoces.</p>" +
                    "<p>La Déclaration Universelle des Droits de l&#39;Homme a aussi un passage sur les droits concernant le mariage « A l’Age de la majorité, les hommes et les femmes ont le droit de se marier et de fonder une famille » art 16.1 DUDH) et aussi « Le mariage ne peut être conclu que avec le libre et plein consentement de leurs épouses » art 16.2 (DUDH)</p>" +
                    "<p>Autrement dit le mariage forcé des enfants, garçons ou filles, est interdit. La pratique est illégale aux Bénin et toute personne qui commettra cet acte subira les rigueurs de la loi.</p>"
          },
          {
            "title": "Il existe beaucoup de formes de violence, à savoir la violence physique, sociale, verbale, émotionnelle et celle basée sur le genre.",
            "file": "image.png",
            "text": "<p>Dans l’épisode le père d’Assiba veut la faire exciser. L’excision ou la mutilation génitale féminile (MGF) est un exemple de violence basée sur le genre. Ce n’est pas une pratique religieuse, plutôt une pratique culturelle qui est passée d’une génération à l’autre et qui vice à contrôler la sexualité de la fille. La MGF est douloureuse et a des conséquences sur la santé, la sexualité et la qualité de vie de celle qui la subisse.</p>" +
                    "<p>Dans la Déclaration Universelle des Droits de l&#39;Homme on lit « Nul ne sera soumis a des traitements cruels, inhumains ou dégradants » art. 5 (DUHD). Autrement dit nous sommes les maitres de notre propres corps, soit homme ou femme. Ainsi personne n’a le droit d’abuser du corps d’autrui, de blesser ou de les violer.</p>"
          }
        ],
        "quiz": {
          "title": "Répondez aux questions en indiquant vrai ou faux",
          "questions": [
            {
              "id": 1,
              "question": "La violence est nécessaire pour l’évaluation de la famille.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "La mutilation génitale est une pratique à décourager.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "Dans certaine famille au Bénin le sexe est un sujet tabou.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 4,
              "question": "Pour avoir une femme docile, il faut souvent l’agresser.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "Le mariage précoce est légal au Bénin.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 6,
              "question": "L’homme est l’égal de la femme dans la société.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 7,
              "question": "Le mariage précoce est nuisible à l’épanouissement des êtres qui le subit.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 8,
              "question": "Pour arrêter ou freiner l’évolution des pratiques de mutilation génitale on peut sensibiliser son entourage.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 9,
              "question": "Les hommes et les femmes doivent avoir les mêmes chances au travail.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 10,
              "question": "Les mutilations génitales augmentent le niveau de fertilité chez les femmes.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 11,
              "question": "L’excision féminine est interdite au Bénin.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E1/1-3.jpg",
        "imageLinkLocked": "~/data/videos/E1/1-3g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E1_low/FR_E1_S3.mp4",
          "fileFon": "~/data/fon/videos/FO_E1_low/FO_E1_S3.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "A leur place que ferais-tu pour bénéficier d’une bonne écoute de Koffi, le père de Assiba ?",
          "answers": [{
              "key": "give-exemple",
              "text": "Donner des exemples de personnes ayant finis en prison à cause de ces mauvaises pratiques."
            },
            {
              "key": "get-help-from-pbe",
              "text": "Venir avec le président du bureau des parents d’élèves du collège de Assiba."
            },
            {
              "key": "get-help-from-friend",
              "text": "Faire venir un sage du village ou de leur famille qui épouse l’opinion de Assiba et Lina."
            },
            {
              "key": "nothing",
              "text": "Je ne pense pas que je pourrais faire quelque chose pour convaincre Koffi."
            }
          ]
        },
        "thematic": [{
            "title": "La discrimination basée sur le genre.",
            "file": "image.png",
            "text": "<p>C’est le fait de minimiser l’importance de quelqu’un seulement parce que c’est un homme ou une femme. Par exemple : envoyer uniquement les filles à l’écoles est une discrimination a l’égard des garçons.</p>" +
                    "<ul><li>Penser que le post de secrétariat soit a priori réservés aux femmes est une discrimination à l’égard des hommes</li>" +
                    "<li>Dans les instances de prise de décision (gouvernements, mairie, présidence) il y a plus d’hommes que femmes. Ceci est une discrimination à l’égard des femmes.</li></ul>"
          }
        ],
        "quiz": {
          "title": "Il existe beaucoup de mythes sur les hommes et les femmes, et leurs rôles dans la société. Est-ce que tu sais distinguer les mythes des faits ?",
          "questions": [
            {
              "id": 1,
              "question": "L’excision augmente le plaisir chez la femme.",
              "answers": [{
                  "key": "1",
                  "text": "Mythe",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Fait",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "L’excision empêche la femme de ressentir du plaisir.",
              "answers": [{
                  "key": "1",
                  "text": "Mythe",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Fait",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "La femme soumise est plus épanouie.",
              "answers": [{
                  "key": "1",
                  "text": "Mythe",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Fait",
                  "value": "false"
                }
              ]
            },
            {
              "id": 4,
              "question": "Scolariser une fille c’est jeter de l’argent par la fenêtre.",
              "answers": [{
                  "key": "1",
                  "text": "Mythe",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Fait",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "Les hommes et les femmes ont les mêmes droits.",
              "answers": [{
                  "key": "1",
                  "text": "Mythe",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Fait",
                  "value": "true"
                }
              ]
            },
            {
              "id": 6,
              "question": "L’homme est le sexe fort et la femme le sexe faible.",
              "answers": [{
                  "key": "1",
                  "text": "Mythe",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Fait",
                  "value": "false"
                }
              ]
            },
            {
              "id": 7,
              "question": "La taille du sexe masculin détermine sa virilité.",
              "answers": [{
                  "key": "1",
                  "text": "Mythe",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Fait",
                  "value": "false"
                }
              ]
            },
            {
              "id": 8,
              "question": "La taille du sexe masculin dépend de la grosseur de son orteil.",
              "answers": [{
                  "key": "1",
                  "text": "Mythe",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Fait",
                  "value": "false"
                }
              ]
            },
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E1/1-4.jpg",
        "imageLinkLocked": "~/data/videos/E1/1-4g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E1_low/FR_E1_S4.mp4",
          "fileFon": "~/data/fon/videos/FO_E1_low/FO_E1_S4.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "Quel serait le sort de Assiba ?",
          "answers": [{
              "key": "understand-but-continue",
              "text": "Apres les interventions le père de Assiba comprend l’enjeu et laisse Assiba continuer ses études."
            },
            {
              "key": "understand-but-fail",
              "text": "Apres les interventions le père de Assiba comprend l’enjeu mais l’envoi se marier faute de moyen pour payer ses études."
            },
            {
              "key": "run-away",
              "text": "Assiba fugue de la maison."
            },
            {
              "key": "protection",
              "text": "La protection de l’enfance est intervenu pour que Koffi puisse laisser Assiba continuer ses études."
            }
          ]
        },
        "aretenir": [{
            "title": "La discrimination basée sur le genre.",
            "file": "image.png",
            "text": "<p>Il y a une différence entre sexe et genre : Le sexe est l’ensemble des différences biologiques et physiques, entre l’homme et la femme.</p>" +
                    "<p>Le genre concerne tous les rôles que la société fait jouer à l’homme ou à la femme.</p>" +
                    "<p>Les hommes et les femmes doivent être traités avec équité et respect.</p>" +
                    "<p>Souvent, les rôles féminins et masculins sont déterminés par la culture plutôt que par la biologie. Par conséquent, ils peuvent être changés.</p>" +
                    "<p>L’apparence sexuelle ne doit pas constituer un frein à la réalisation de nos ambitions.</p>"
          },
          {
            "title": "La discrimination basée sur le genre.",
            "file": "image.png",
            "text": "<p>Si cette lecon t’as captivé, tu trouveras encore plus d’infos et d’outils sur les violences basées sur le genre sur notre site web <strong>Tro Tro Ga Ho!</strong> (l’heure du changement a sonné, en Adja). " +
                    "Le site est <strong>spécialement adapté aux mobiles</strong> et a été <strong>consomme très peu de crédit</strong>.</p>" +
                    "<p><strong><a href='https://trotrogaho.com/?utm_source=mvmc-mobile-app&utm_medium=mobile-app&utm_campaign=trotrogaho'>Visite trotrogaho.com et ajoute le à tes favoris!*</a></strong></p>" +
                    "<p>*en cliquant tu vas sortir de  l’appli et ouvrir ton navigateur.</p>"
          }
        ],
        "engagement": [{
            "text": "<p>Etant voué à défendre la cause des jeunes, je m’engage a bannir toutes violences basés sur le genre (Rachel)</p>" +
                    "<p>Je m’engage à lutter contre toute forme de discrimination et violence basé sur le genre et a protéger mes pairs du mieux que je peux (Mercy).</p>" +
                    "<p>Je m’engage à dénoncer tous les cas de mariage forcés que j’apprendrai dans ma communauté. (Pierrette)</p>" +
                    "<p>Je m’engage a toujours partager des informations avec mes camarades et tous autres personnes afin de les faire découvrir ce qu’ils ignorent et les persuader sur les comportements et adopter pour mieux vivre (Chris)</p>"
          }
        ],
        "discussions": [{
            "text": "<p>Comment est-ce qu’on peut réagir quand un parent, mère ou père, montre la violence contre les enfants ?</p>" +
                    "<p>Comment on peut sensibiliser son entourage sur les thèmes du genre ?</p>" +
                    "<p>Comment on peut lutter contre la discrimination ?</p>"
          },
          {
            "text": "<p><strong>Rappel:</strong> Besoin d’aide pour te lancer dans tes engagements contre les violences basées sur le genre? Visite notre site web <strong>Tro Tro Ga Ho!</strong> Tu y trouveras:<br>" +
                    "<strong>•</strong> Des <strong>informations</strong> sur les <strong>droits</strong> des femmes et des filles<br>" +
                    "<strong>•</strong> La liste des <strong>services</strong> disponibles aux victimes<br>" +
                    "<strong>•</strong> Des <strong>messages éducatifs</strong> à partager à tes contacts pour les sensibiliser<br>" +
                    "<strong>•</strong> Des <strong>outils</strong> développés par des sources fiables qui peuvent être utilisés lors des activités de sensibilisation</p>" +
                    "Le site est <strong>spécialement adapté aux mobiles</strong> et a été <strong>consomme très peu de crédit</strong>.</p>" +
                    "<p><strong><a href='https://trotrogaho.com/?utm_source=mvmc-mobile-app&utm_medium=mobile-app&utm_campaign=trotrogaho'>Visite trotrogaho.com et ajoute le à tes favoris!*</a></strong></p>" +
                    "<p>*en cliquant tu vas sortir de  l’appli et ouvrir ton navigateur.</p>"
          }
        ]
      },
    ]
  },
  {
    "id": 2,
    "title": "Santé Reproductive et Droits Sexuels",
    "icon": "~/data/icons/icon-lesson-2.png",
    "episodes": [
      {
        "imageLink": "~/data/videos/E2/2-1.jpg",
        "imageLinkLocked": "~/data/videos/E2/2-1g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E2_low/FR_E2_S1.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "A la place d’Assiba, où irais-tu pour avoir des informations sur la santé sexuelle et reproductive ?",
          "answers": [{
              "key": "friends",
              "text": "Chez un ami ou une amie"
            },
            {
              "key": "familly",
              "text": "Chez mes parents"
            },
            {
              "key": "internet",
              "text": "Chercher les infos sur internet"
            },
            {
              "key": "an-advice",
              "text": "Chez une conseillère de santé ou une structure adéquate"
            }
          ]
        },
        "thematic": [{
            "title": "Dans l’épisode on voit que Assiba a des règles douloureuses, et qu’elle retient beaucoup d’informations différentes sur comment gérer ses menstruations.",
            "file": "image.png",
            "text": "<p>Chaque enfant et chaque jeune a le droit à l’information en matière de santé sexuelle et de la reproduction. La santé sexuelle et de la reproduction couvre la santé et le bien-être en traitant des questions liées aux rapports sexuels, aux grossesses et aux naissances. Cela suppose qu’une personne peut mener une vie sexuelle satisfaisante et en toute sécurité comme par exemples d’avoir les informations correctes par rapport à des méthodes contraceptives, avoir accès à des services de santé.</p>" +
                    "<p>L’école a la responsabilité d’éduquer les jeunes dans tous les domaines de la vie. Et cette responsabilité est inscrite dans la Convention relative aux Droits de l’Enfant !</p>" +
                    "<p>L’enfant a le droit de prendre des décisions éclairées et responsables. Pour cela l’enfant doit avoir les informations justes.</p>"
          }
        ],
        "quiz":{
          "title": "Indiquez si les déclarations sont vraies ou fausses ",
          "questions":[
            {
              "id": 1,
              "question": "L’accès à l’information est important pour autonomiser les jeunes",
              "response": "Quand les jeunes ont les informations dont ils ont besoin concernant la santé sexuelle et reproductive, ils peuvent mieux gérer leur vie sexuelle et prendre des décisions responsables",
              "answers": [{
                "key": "1",
                "text": "Vrai",
                "value": "true",
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "Des valeurs socio-culturelles peuvent être une barrière pour les jeunes d’accéder aux informations ?",
              "response": "Parfois des valeurs socio-culturelles véhiculent des informations fausses et constituent des obstacles pour les jeunes d’accéder aux informations justes.",
              "answers": [{
                "key": "1",
                "text": "Vrai",
                "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
            {
              "id": 3,
              "question": "Les jeunes ne devraient pas avoir accès aux informations sur l’internet",
              "response": "Il est important que les jeunes puissent rechercher des informations sur le net en tout anonymat sans éprouver de la gêne. Mais il faut dissocier le vrai du faux sur internet: ce n’est pas parce qu’on lit une information sur plusieurs sites qu’elle est forcément vraie.",
              "answers": [{
                "key": "1",
                "text": "Vrai",
                "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 4,
              "question": "Le tabou liant la sexualité peut-être une barrière pour les jeunes pour accéder aux informations.",
              "response": "Il est important de savoir que tous les jeunes ont des questions sur la santé sexuelle et reproductive et la sexualité, et qu’il n’y a aucune raison d’être embarrassé",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "Des informations adéquates peuvent améliorer la santé sexuelle et reproductive des jeunes",
              "response": "Avec les informations correctes les jeunes peuvent prendre les décisions adéquates sur leur santé sexuelle et reproductive.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
          ]
        },
      },
      {
        "imageLink": "~/data/videos/E2/2-2.jpg",
        "imageLinkLocked": "~/data/videos/E2/2-2g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E2_low/FR_E2_S2.mp4"
        },
        "dialogue": {
          "id": 2,
          "question": "Quels sont les avantages de faire recours à une personne ressource professionnelle ?",
          "answers": [{
              "key": "right-informations",
              "text": "Avoir les informations correctes."
            },
            {
              "key": "confidentiality",
              "text": "Pouvoir poser des questions en toute confidentialité."
            },
            {
              "key": "ask-questions",
              "text": "Avoir des informations spécifiques pour les jeunes."
            },
            {
              "key": "all-previous",
              "text": "Tout ce qui est mentionné ci-dessus."
            }
          ]
        },
        "thematic": [{
          "title": "Les jeunes ont le droit à l’accès aux services de santé pour mieux gérer leur sexualité ainsi que leur santé sexuelle et reproductive. L’offre de services en santé de la reproduction des adolescents et des jeunes est constituée de :",
          "file": "image.png",
          "text": "<p>1. Centre des jeunes (ados) pour les conseils</p>" +
                  "<ul>"+
                    "<li> Information ;</li>" +
                    "<li> Orientation ; ;</li>" +
                    "<li> Prise en charge psychosociale et sanitaire;</li>" +
                    "<li> Information ;</li>" +
                  "</ul>"+

                  "<p>2. Centre de dépistage volontaire et anonyme :</p>" +
                  "<ul>"+
                    "<li> Dépistage IST et VIH/Sida </li>" +
                  "</ul>"+

                  "<p>3. Centre de santé :</p>" +
                  "<ul>"+
                    "<li> Soins médicaux</li>" +
                    "<li> Services de planification familiale.</li>" +
                  "</ul>"+

                  "<p>Les services de qualité contiennent les aspects suivants :</p>" +
                  "<ul>"+
                    "<li> Environnement sûr où les jeunes se sentent à l’aise</li>" +
                    "<li> Adaptés aux besoins des jeunes</li>" +
                    "<li> Apportent des soutiens aux jeunes sans jugement</li>" +
                    "<li> Respectent la confidentialité</li>" +
                    "<li> Soins Gratuit ou à un coût bas </li>"+
                  "</ul>"
        }],
        "quiz":{
          "title": "Répondez aux questions par vrai ou faux",
          "questions":[
            {
              "id": 1,
              "question": "Les services pour les jeunes doivent les aider sans les juger.",
              "response": "Chaque personne qui nait a le droit de vivre et c’est aux parents et tuteurs d’entretenir et de protéger l’enfant.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 2,
              "question": "Les centres de jeunes (ados) sont aussi fréquentés par les adultes.",
              "response": "Toute personne doit être informée sur les questions de santé afin de prendre des décisions importantes sur ces questions. Ce rôle revient à l’Etat, aux parents, aux écoles, et ceci à travers les médias.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 3,
              "question": "L’anonymat est important dans les centres de dépistage.",
              "response": "Les enfants doivent être protégés contre tout type de violences ou d’abus. C’est pourquoi la loi a prévu plusieurs articles faisant référence aux droits d’être protégés. Les parents doivent bien traiter leurs enfants et les entretenir.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 4,
              "question": "La confidentialité est importante pour les services de santé des jeunes.",
              "response": "Chaque personne à sa manière de concevoir les choses d’où le droit à l’expression pour partager ses idées et ressentiments.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
          ]
        },
      },
      {
        "imageLink": "~/data/videos/E2/2-3.jpg",
        "imageLinkLocked": "~/data/videos/E2/2-3g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E2_low/FR_E2_S3.mp4"
        },
        "dialogue": {
          "id": 3,
          "question": "Un comportement responsable, selon toi, c’est quoi ?",
          "answers": [{
              "key": "sexual-life",
              "text": "Pouvoir mener une vie sexuelle saine."
            },
            {
              "key": "good-decision",
              "text": "pouvoir prendre des décisions éclairées et responsables."
            },
            {
              "key": "raise-awareness",
              "text": "Pouvoir sensibiliser mon entourage."
            },
            {
              "key": "know-body",
              "text": "Mieux connaitre mon propre corps."
            },
            {
              "key": "all-previous",
              "text": "Tout ce qui est décrit ci-dessus."
            }
          ]
        },
        "thematic": [{
          "title": "Les droits sexuels sont des droits humains relatifs à la sexualité. Les adolescents et jeunes sont des êtres humains sexués. Ils ont des besoins, désirs, fantasmes et rêves sexuels. Il est important pour tous les adolescent(e)s et jeunes dans le monde d’avoir la possibilité d’explorer, d’éprouver et d’exprimer leur sexualité de manière responsable, saine, positive, agréable et sûre.",
          "file": "image.png",
          "text": "<p>Il existe dix droits sexuels des adolescent(e)s et jeunes :</p>" +
                  "<ul>"+
                    "<li> le droit à l’information </li>" +
                    "<li> le droit à l’accès </li>" +
                    "<li> le droit au libre choix </li>" +
                    "<li> le droit à la sécurité </li>" +
                    "<li> le droit à l’intimité </li>" +
                    "<li> le droit à la confidentialité </li>" +
                    "<li> le droit à la dignité </li>" +
                    "<li> le droit au bien-être </li>" +
                    "<li> le droit au bien-être </li>" +
                    "<li> le droit à la continuité </li>" +
                    "<li> le droit à l’expression </li>"+
                  "</ul>"
          }
        ],
        "quiz": {
          "title": "De quel droit s’agit-il ? Choisissez la bonne réponse !",
          "questions":[
            {
              "id": 1,
              "question": "Les enfants n’ont pas demandé à venir au monde, aussi s’ils sont là, les parents doivent s’occuper d’eux avec le plus grand soin ! (R : 2)",
              "answers": [
                {
                  "key": "1",
                  "text": "1. Le droit à l’information",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "2. Le droit à la santé",
                  "value": "true"
                },
                {
                  "key": "3",
                  "text": "3. Le droit à l’expression",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "4. Protection contre les violences et les abus",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "b. Faire des émissions à la radio, à la télé, utiliser le théâtre populaire. Et dans les centres de santé, les centres de jeunes (ados), parler aux gens, les sensibiliser !",
              "answers": [{
                  "key": "1",
                  "text": "1. Le droit à l’information",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "2. Le droit à la santé",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "3. Le droit à l’expression",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "4. Protection contre les violences et les abus",
                  "value": "false"
                }
              ]
            },
            {
              "id": 3,
              "question": "c. Article 19 de la Convention sur les droits de l’enfant dit qu’il faut protéger l’enfant contre toutes formes de violences, qu’elles soient physiques, psychologiques, et contre la négligence et le mauvais traitement…",
              "answers": [{
                  "key": "1",
                  "text": "1. Le droit à l’information",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "2. Le droit à la santé",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "3. Le droit à l’expression",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "4. Protection contre les violences et les abus",
                  "value": "true"
                }
              ]
            },
            {
              "id": 4,
              "question": "d. Dans certaines familles un manque de responsabilité engendre des valeurs négatives : les femmes ne sont pas égales aux hommes et les enfants n’ont pas droit à la parole .",
              "answers": [{
                  "key": "1",
                  "text": "1. Le droit à l’information",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "2. Le droit à la santé",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "3. Le droit à l’expression",
                  "value": "true"
                },
                {
                  "key": "4",
                  "text": "4. Protection contre les violences et les abus",
                  "value": "false"
                }
              ]
            },
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E2/2-4.jpg",
        "imageLinkLocked": "~/data/videos/E2/2-4g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E2_low/FR_E2_S4.mp4"
        },
        "dialogue": {
          "id": 2,
          "question": "Quels sont les éléments qui influencent le plus ton comportement lié à la sexualité ?",
          "answers": [{
            "key": "friends-opinions",
            "text": "Les opinions de mes amis."
          },
          {
            "key": "school-learning",
            "text": "La pression de mes pairs."
          },
          {
            "key": "school-learning",
            "text": "Ce que j’apprends à l’école."
          },
          {
            "key": "community-opinions",
            "text": "Ce que j’entends dire dans ma communauté."
          },
          {
            "key": "parents-pressure",
            "text": "La pression des parents."
          },
          {
            "key": "my-good-thinks",
            "text": "Ce que je pense être juste."
          }
          ]
        },
        "aretenir": [{
          "file": "image.png",
          "text": "<p>Il n’y a pas d’avenir sans la santé et le droit à l’information est aussi reconnu à l’enfant par les articles 13 et 18 du Code de l’enfant du Bénin, ainsi que le droit à l’expression. Toutes ces lois existent pour l’intérêt de l’enfant. </p>" +
                    "<p>L’enfant a le droit de prendre des décisions éclairées et responsables. C’est pour cela que l’enfant doit avoir les informations justes.</p>" +
                    "<p>Les jeunes ont le droit à l’accès aux services de santé pour mieux gérer leur santé sexuelle et reproductive ainsi que leur sexualité.</p>" +
                    "<p>Chaque société, chaque groupe a ses propres valeurs et coutumes. Les rites d’initiation, l’excision, la dot, le mariage forcé, la polygamie, la religion, la famille, la culture, sont entre autres des valeurs socio-culturelles et coutumes qui influencent la santé des adolescent (e)s et jeunes et même leurs comportements.</p>"
          }
        ],
        "engagement": [{
            "file": "image.png",
            "text": "<p>Je m’engage à obtenir des informations justes en matière de SSR.</p>" +
                    "<p>Je m’engage à connaitre mes droits en SSR.</p>" +
                    "<p>Je m’engage à prendre des décisions responsables sur ma sexualité.</p>"

          }
        ],
        "discussions": [{
            "text": "<p>Comment est-ce qu’on peut gérer les valeurs socio-culturelles par rapport à la SSR ?</p>" +
                    "<p>Comment peut-on sensibiliser son entourage sur l’importance de la Santé Sexuelle et Reproductive ?</p>" +
                    "<p>Quelle est l’importance de la SSR dans le développement d’un individu, couple / pays ?</p>"
          }
        ],
      }
    ]
  },
  {
    "id": 3,
    "title": "IST VIH",
    "icon": "~/data/icons/icon-lesson-3.png",
    "episodes": [
      {
        "imageLink": "~/data/videos/E3/3-1.jpg",
        "imageLinkLocked": "~/data/videos/E3/3-1g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E3_low/FR_E3_S1.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "Qu’est-ce que tu ferais à la place d’Assiba ?",
          "answers": [{
              "key": "change-school",
              "text": "Je changerai d’école pour éviter d’être contaminé"
            },
            {
              "key": "try-help",
              "text": "J’essaierai d’aider la personne infectée"
            },
            {
              "key": "shared-informations",
              "text": "Je ne changerai pas d’école, car le VIH ne se transmet pas comme ça. Je parlerai de ce sujet avec des adultes et des amis"
            }
          ]
        },
        "thematic": [{
          "title": "Dans l’épisode on voit que Assiba a peur de contracter le VIH, car il y a quelqu’un dans son école qui est séropositif. Mais qui est un séropositif ?  Quels sont les modes de transmission ?",
          "file": "image.png",
          "text": "<p>Un séropositif est quelqu’un qui a le VIH dans son sang. Le VIH est le Virus de l’Immunodéficience Humaine ; c’est un microbe qui affaiblit et baisse la défense de l’organisme humain où il vit longtemps.</p>" +
                  "<p>Qu’est-ce que le sida ? le SIDA est le Syndrome de l’Immuno-Déficience Acquise ; c’est un ensemble de signes causés par la baisse de la défense de l’organisme. La maladie causée par le VIH peut évoluer juste au stade de  sida.</p>" +
                  "<p>Le VIH se transmet :</p>" +
                  "<ul>"+
                    "<li>par les rapports sexuels non protégés</li> "+
                    "<li>de la mère à l’enfant pendant la grossesse, au cours de l’accouchement et de l’allaitement </li>"+
                    "<li>par le sang infecté lors de la transfusion sanguine et en cas de partage des aiguilles, seringues et instruments tranchants souillés </li>"+
                  "</ul>"+
                  "<p>En revanche la salive, la sueur, les larmes et l’urine ne sont pas des liquides permettant la transmission du virus du sida.</p>"+
                  "<p>La stigmatisation et la discrimination contre les personnes infectées par le VIH est interdite par la loi.</p>"
          },
        ],
        "quiz": {
          "title": "Décidez quel risque correspond aux comportements suivants :",
          "questions":[
            {
              "id": 1,
              "question": "je n’ai pas eu de rapport sexuel ni de contact avec du sang infecté",
              "response": "il n’y a ni contact sexuel avec une personne infectée ni contact avec du sang infecté.",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "j’ai partagé des seringues pour l’utilisation de la drogue .",
              "response": ": Le partage d’objets piquants peut être une source de contamination (à cause du sang qui reste dans la seringue)",
              "answers": [
                {
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "J’ai fais la bise à une personne infectée par le VIH.",
              "response": "le SIDA ne se transmet pas avec l’échange d’embrassades. Il se transmet juste par voie sanguine, sexuelle ou de la mère à l’enfant. Il ne faut pas avoir peur de toucher des personnes infectées par le VIH",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 4,
              "question": "J’ai échangé une brosse à dents.",
              "response": "l’échange d’objet tel que la brosse à dents peut être source de contamination. Il peut y avoir contamination si les personnes qui échangent la brosse à dents ont des saignements au niveau de la  bouche",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "true"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "J’ai partagé les aiguilles pour me percer mes oreilles.",
              "response": "Le partage d’objet piquant ou tranchant avec des personnes infectées est une source de contamination. (À cause du risque du saignement)",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "true"
                }
              ]
            },
            {
              "id": 6,
              "question": "J’ai échangé des baisers.",
              "response": "Il peut y avoir contamination si les partenaires ont des saignements/blessures au niveau de la bouche",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "true"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 7,
              "question": "je me suis masturbé.",
              "response": "Il n’y a pas eu de rapport sexuel avec une autre personne infectée. ",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 8,
              "question": "J’ai reçu des postillons d’une personne infectée.",
              "response": "le virus ne se transmet pas à travers la salive. ",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 9,
              "question": "J’ai utilisé des toilettes publiques.",
              "response": "il n’y a ni contact sexuel ni partage d’objets piquants ou tranchants. ",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 10,
              "question": "j’ai salué une personne infectée par le VIH.",
              "response": "Le virus du SIDA ne se transmet pas en saluant une personne infectée",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 11,
              "question": "J’ai donné du sang à l’hôpital.",
              "response": "On ne peut pas être contaminé en donnant du sang et en utilisant des objets stérilisés mais plutôt en recevant du sang infecté. ",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 12,
              "question": "j’ai eu des relations sexuelles sans préservatif.",
              "response": "la piqûre d’un moustique ne transmet pas le virus du SIDA.",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "true"
                }
              ]
            },
            {
              "id": 13,
              "question": "J‘ai été piqué par un moustique.",
              "reponse": "On ne risque pas de contracter le virus du SIDA si on a des rapports sexuels protégés. Toutefois, mieux vaut se faire dépister régulièrement pour connaitre son état sérologique parce que le préservatif peut être percé.",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 14,
              "question": "J’ai eu des rapports sexuels avec quelqu’un qui a utilisé un préservatif.",
              "response": "on ne risque pas de contracter le virus du SIDA si on a des rapports sexuels protégés. Toutefois, mieux vaut se faire dépister régulièrement pour connaitre son état sérologique parce que le préservatif peut être percé.",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "true"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            },
            {
              "id": 15,
              "question": "Je suis allaité(e) par ma mère qui est séropositive .",
              "response": "Une mère séropositive peut transmettre le virus à son enfant lors de l’accouchement ou de l’allaitement. Cependant, la contamination peut être évitée si on les prend en charge très tôt.",
              "answers": [{
                  "key": "1",
                  "text": "Pas de risque",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Risque peu probable",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Risque probable",
                  "value": "true"
                },
                {
                  "key": "4",
                  "text": "Risque certain",
                  "value": "false"
                }
              ]
            }
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E3/3-2.jpg",
        "imageLinkLocked": "~/data/videos/E3/3-2g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E3_low/FR_E3_S2.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "Quel comportement peut faire contracter le virus ?",
          "answers": [{
              "key": "Kiss-infected-person",
              "text": "Faire la bise à une personne infectée par le VIH."
            },
            {
              "key": "receive-object",
              "text": "Recevoir des objets d’une personne infectée."
            },
            {
              "key": "public-toilets",
              "text": "Utiliser des toilettes publiques."
            },
            {
              "key": "have-sex",
              "text": "Avoir des relations sexuelles sans préservatif."
            }
          ]
        },
        "thematic": [{
            "title": "Se protéger :",
            "file": "image.png",
            "text": "<p>Le VIH/SIDA se transmet surtout par les rapports sexuels non protégés. Alors ça veut dire qu’on peut se prémunir contre le VIH en utilisant un préservatif. Il est important de connaitre son statut sérologique. Pour cela, il est nécessaire de se faire dépister.</p>"
          },
          {
            "title": "Le test du dépistage :",
            "file": "image.png",
            "text": "<p>Après une contamination, le corps produit des anticorps. Les anticorps sont une réaction à la présence du virus dans l’organisme. Le temps des développements des anticorps varie selon chaque personne : il varie de 3 semaines à 3 mois. Le test de dépistage sert à détecter les anticorps. Si des anticorps spécifiques du VIH sont détectés, le résultat du test est positif : tu es séropositif. Un suivi médical adapté peut être mis en place.</p>"+
            "<p>Le test de dépistage consiste à une simple prise de sang que tu peux faire à tout moment de la journée (pas besoin d’être à jeun). On peut faire le test dans des centres de jeunes (sociaux), ou aussi dans les hôpitaux.</p>"
          }
        ],
        "quiz": {
          "title": "Répondez aux questions avec vrai ou faux :",
          "questions":[
            {
              "question": "Le VIH est transmissible.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 2,
              "question": "On peut se préserver du VIH en ayant des rapports sexuels protégés.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "Le VIH se transmet par la sueur, la respiration ou les contacts physiques.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 4,
              "question": "On ne peut pas toucher une personne séropositive.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "On doit se faire dépister pour savoir si on est infecté.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 6,
              "question": "Ne jamais utiliser les mêmes objets qu’une personne infectée.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 7,
              "question": "le VIH se transmet par les piqures de moustiques.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 8,
              "question": "Les personnes atteintes du VIH doivent être isolées.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 9,
              "question": "Les mères porteuses du VIH ne peuvent pas avoir des enfants sains.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            }
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E3/3-3.jpg",
        "imageLinkLocked": "~/data/videos/E3/3-3g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E3_low/FR_E3_S3.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "D’après toi, doit-on craindre les séropositifs ? ",
          "answers": [{
              "key": "no",
              "text": "Non"
            },
            {
              "key": "yes",
              "text": "Oui"
            },
            {
              "key": "maybe",
              "text": "Je ne sais pas"
            }
          ]
        },
        "thematic": [{
          "title": "Le saviez vous !",
          "file": "image.png",
          "text": "<p>Les IST sont les Infections Sexuellement Transmissibles. Elles se transmettent essentiellement lors des rapports sexuels non protégés.</p>"+
                  "<p>Contracter une IST n’est plus une fatalité. Il existe des moyens de se préserver. Il suffit de s’abstenir ou d’utiliser le préservatif. Quand on a une IST, il faut consulter un agent de santé et demander au partenaire d’en faire autant. Suivre rigoureusement le traitement.</p>"+
                  "<p>D’une manière générale on peut avoir chez l’homme ou chez la femme les signes suivants :</p>"+
                  "<ul>"+
                    "<li>brûlures en urinant</li>"+
                    "<li>gonflement des ganglions (douloureux ou non)</li>"+
                    "<li>gonflement sous la peau</li>"+
                    "<li>plaies (ulcérations) sur les organes génitaux externes</li>"+
                    "<li>verrues sur les parties génitales (crêtes de coq)</li>"+
                    "<li>plaies dans la bouche</li>"+
                    "<li>démangeaisons sur le corps ou sur les parties génitales</li>"+
                    "<li> Boutons sur les organes génitaux</li>"+
                  "<ul>"
          },
          {
            "title": "les IST :",
            "file": "image.png",
            "text":
                    "<p>D’une manière générale on peut avoir chez l’homme ou chez la femme les signes suivants :</p>"+
                    "<p><strong>Blennorragie ou gonococcie</strong>. Symptômes : Brûlures et/ou écoulement jaune par e pénis (verge), le vagin, fièvre, douleur au bas-ventre. Mode de transmission par voie sexuelle </p>"+
                    "<p></strong>Hépatite B /strong>Symptômes : souvent pas de signes, fatigue, fièvre. Modes de transmission par voie sexuelle, par voie sanguine, par voie maternelle lors de l’accouchement, fluides corporels (salive, sueur…) </p>"+
                    "<p></strong>Chlamydioses</strong>Symptômes : le plus souvent aucun signe sinon brûlures, écoulement par le pénis ou le vagin, fièvre, douleur au bas-ventre, voire angine. Mode de transmission : par voie sexuelle</p>"+
                    "<p></strong>Herpès génitaux</strong>Symptômes : petits boutons douloureux en forme de bulles sur les organes génitaux, l’anus ou la bouche, démangeaisons. Mode de transmission : par contact sexuel entre les muqueuses même sans pénétration (contact bouche- sexe et bouche-anus), par contact direct avec des lésions</p>"+
                    "<p></strong>Le trichomonas</strong>Symptômes : Ecoulement par le pénis ou le vagin, brûlures, démangeaisons. Mode de transmission : par contact sexuel</p>"+
                    "<p></strong>Papillomavirus</strong>Symptômes : lésions ou petites verrues (condylomes) sur les organes génitaux ou l'anus. Modes de transmission : par voie sexuelle, par contact d'une muqueuse avec une zone du corps infectée (condylomes)</p>"+
                    "<p></strong>Syphilis</strong>Symptômes : chancre (petite plaie ou bouton indolore), éruptions sans démangeaisons sur la peau et les muqueuses. Modes de transmission par contact sexuel entre les muqueuses même sans pénétration (contact bouche-sexe et bouche-anus), par les baisers si présence d'un brûlure ou ulcération au niveau de la bouche.</p>"
          }
        ],
        "quiz": {
          "title": " Quelle IST est expliquée à l'aide des symptômes ?",
          "questions":[
            {
              "id": 1,
              "question": "Petite plaie ou brûlure au niveau génital",
              "response": "Les symptômes disparaissent également en l'espace de quelques semaines mais la bactérie de la syphilis reste dans l'organisme si l'infection n'est pas traitée. Des taches peuvent alors apparaitre sur la peau. Cela peut entraîner des mutilations, la stérilité, etc.",
              "answers": [{
                  "key": "1",
                  "text": "Papillomavirus",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Chlamydia",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Syphilis",
                  "value": "true"
                },
                {
                  "key": "4",
                  "text": "VIH",
                  "value": "false"
                },
              ]
            },
            {
              "id": 2,
              "question": "Le plus souvent aucun signe visible",
              "response": "Les signes apparaissent 1 à 2 semaines après la contamination. Si l’infection est non traitée on risque la stérilité, la grossesse extra-utérine et l’atteinte du nouveau-né si la mère est infectée.",
              "answers": [{
                  "key": "1",
                  "text": "Papillomavirus",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Chlamydia",
                  "value": "true"
                },
                {
                  "key": "3",
                  "text": "Syphilis",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Hépatite B",
                  "value": "false"
                },
              ]
            },
            {
              "id": 3,
              "question": "le plus souvent pas de symptôme et parfois syndrome grippal",
              "response": "Si l’infection n’est pas traité, on risque l’évolution possible vers le sida et  la  contamination de l’enfant par sa mère infectée pendant la grossesse, l’accouchement et au moment de l’allaitement.",
              "answers": [{
                  "key": "1",
                  "text": "Papillomavirus",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Chlamydia",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Syphilis",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "VIH",
                  "value": "true"
                },
              ]
            },
            {
              "id": 4,
              "question": "fièvre, fatigue et « jaunisse »",
              "response": "L’hépatite B est due à un virus qui provoque des lésions inflammatoires du foie. Si l’infection n’est pas traitée, on risque la cirrhose et le cancer du foie. Il y a un risque de récidive.",
              "answers": [{
                  "key": "1",
                  "text": "Papillomavirus",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Chlamydia",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Syphilis",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Hépatite B",
                  "value": "true"
                },
              ]
            },
            {
              "id": 5,
              "question": "écoulements par le vagin, le pénis ou l'anus",
              "response": "Les signes apparaissent à partir de 1 semaine après la contamination. Il y a risque de récidive.",
              "answers": [{
                  "key": "1",
                  "text": "Papillomavirus",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Chlamydia",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Le trichomonas",
                  "value": "true"
                },
                {
                  "key": "4",
                  "text": "Hépatite B",
                  "value": "false"
                },
              ]
            },
            {
              "id": 6,
              "question": "petits boutons douloureux en forme de bulles sur les organes génitaux, l’anus ou la bouche",
              "response": "Les signes apparaissent 1 semaine ou plus après la contamination.si l’infection est non traitée on risque une atteinte grave du nouveau-né si la mère est infectée. Cela peut entraîner une mutilation du pénis chez l’homme",
              "answers": [{
                  "key": "1",
                  "text": "Papillomavirus",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Chlamydia",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Syphilis",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Hépatite génitale)",
                  "value": "true"
                },
              ]
            },
            {
              "id": 7,
              "question": "Brulures en urinant",
              "response": "Il y a aussi des écoulements purulents. Les signes apparaissent 2 à 7 jours après la contamination. Si l’infection est non traitée, on risque la stérilité, surtout chez la femme et l’atteinte du nouveau-né au moment de l’accouchement, si la mère est infectée.",
              "answers": [{
                  "key": "1",
                  "text": "Blennorragie ou gonococcie",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Chlamydia",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "Hépatite B",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Syphilis",
                  "value": "false"
                },
              ]
            },
            {
              "id": 8,
              "question": "lésions ou petites verrues (condylomes) sur les organes génitaux ou l'anus",
              "response": "Les signes apparaissent 1 à 8 semaines après la contamination. Si l’infection est non traitée, on risque le cancer du col de l’utérus et l’atteinte du nouveau-né pendant la grossesse, si la mère est infectée. Il y a un risque de récidive.",
              "answers": [{
                  "key": "1",
                  "text": "Papillomavirus",
                  "value": "true"
                },
                {
                    "key": "2",
                    "text": "Chlamydia",
                    "value": "false"
                  },
                  {
                    "key": "3",
                    "text": "Syphilis",
                    "value": "false"
                  },
                  {
                    "key": "4",
                    "text": "Hépatite génitale",
                    "value": "false"
                  }
                ]
              },
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E3/3-4.jpg",
        "imageLinkLocked": "~/data/videos/E3/3-4g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E3_low/FR_E3_S4.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "Quels sont les différents moyens pour se préserver des IST/VIH ?",
          "answers": [{
              "key": "no-sex",
              "text": "L’abstinence"
            },
            {
              "key": "Condom",
              "text": "Utilisation du préservatif"
            },
            {
              "key": "maybe",
              "text": "Je ne sais pas"
            }
          ]
        },
        "aretenir": [{
          "title": "Le saviez vous !",
          "file": "image.png",
          "text": "<p>Le VIH est le virus responsable du sida. Il attaque et détruit progressivement les défenses du système immunitaire humain qui défendent l’organisme contre les maladies.</p>"+
                  "<p>Il est possible de se protéger contre le VIH/sida et les IST, en utilisant des préservatifs pendant les rapports sexuels.</p>"+
                  "<p>Essayer de se soigner tout seul est dangereux pour la santé.</p>"+
                  "<p>Dans les semaines et les mois suivant un rapport sexuelnon protégé, il est important de consulter un spécialiste pour faire un test sur les IST et/ou sur le VIH. Il faut aussi faire davantage attention lorsque certains signes apparaissent : </p>"+
                  "<ul>"+
                    "<li>Ecoulements</li>"+
                    "<li>pertes vaginales importantes et/ou malodorantes </li>"+
                    "<li>démangeaisons</li>"+
                    "<li>sensations de brûlures en urinant</li>"+
                    "<li>boutons, lésions ou ulcérations sur le sexe ou sur d’autres muqueuses (bouche, anus…)</li>"+
                    "<li>plaies dans la bouche</li>"+
                    "<li>douleurs au bas ventre </li>"+
                    "<li>autres manifestations inhabituelles</li>"+
                  "<ul>"+
                "<p>Toutes les IST ne se remarquent pas toujours. Aussi faut-il être vigilant.</p>"+
                "<p>La majorité des IST ne présente pas de symptômes apparents, surtout chez les femmes.</p>"
          },
        ],
        "engagement": [{
          "title": "Je m’engage à ;",
          "file": "image.png",
          "text": "<p>Avoir un comportement sexuel responsable pour prévenir les IST</p>"+
                  "<p>Prendre des décisions responsables pour me préserver du VIH/SIDA</p>"+
                  "<p>Faire un effort pour comprendre et assister les personnes infectées par le VIH/SIDA</p>"
          },
        ],

        "discussions": [{
          "text": "<p>Quels sont les avantages du dépistage ?</p>"+
                  "<p>Quels peuvent être les arguments contre le dépistage, les raisons de ne pas le faire ?</p>"+
                  "<p>Quand faut-il se faire dépister ?</p>"+
                  "<p>Quelles recommandations feriez-vous à un séropositif ?	</p>"
          },
        ]
      }
    ]
  },
  {
    "id": 4,
    "title": "Méthodes contraceptives",
    "icon": "~/data/icons/icon-lesson-4.png",
    "episodes": [
      {
        "imageLink": "~/data/videos/E4/4-1.jpg",
        "imageLinkLocked": "~/data/videos/E4/4-1g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E4_low/FR_E4_S1.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "Que faire, selon toi, pour éviter les grossesses rapprochées ?",
          "answers": [{
              "key": "speak",
              "text": "Parler avec son partenaire sur la planification familiale"
            },
            {
              "key": "yes",
              "text": "Utiliser des méthodes contraceptives"
            },
            {
              "key": "nother",
              "text": " On ne peut rien faire contre les grossesses rapprochées"
            },
            {
              "key": "maybe",
              "text": "Je ne sais pas"
            }
          ]
        },
        "thematic": [{
            "title": "Le saviez vous !",
            "file": "image.png",
            "text": "<p>La grossesse non désirée est une grossesse non souhaitée, non voulue par l’un ou par les deux partenaires pour de multiples raisons.</p>"+
            "<p>La grossesse rapprochée par contre, est une grossesse proche de la grossesse précédente (moins de 2ans).</p>"+
            "<p>La contraception est l’utilisation des moyens et techniques pour empêcher la survenue d’une grossesse.</p>"+
            "<p>La contraception peut être définie comme toute méthode utilisée pour prévenir la grossesse. Une personne (ou un couple) qui a l'intention d'avoir des rapports sexuels, mais ne veut pas une grossesse peut utiliser un contraceptif.</p>"+
            "<p>Les femmes, les hommes et les jeunes ont le droit de décider si et quand ils veulent commencer à avoir des enfants, l'espacement entre chacun, ainsi que la personne avec laquelle ils veulent fonder une famille. Les risques liés aux grossesses rapprochées nuit à la santé de la maman et</p>"+
            "<p>du bébé (par exemple retard de croissance, faible poids, complications néonatales). L'utilisation de contraceptifs permet à de nombreuses personnes d'avoir un plus grand contrôle sur leur corps, leurs relations et leur vie sociale et économique en général.</p>"
          },
        ],
        "quiz": {
          "title": "Répondez aux questions par vrai ou faux:",
          "questions":[
            {
              "id": 1,
              "question": "La grossesse rapprochée peut nuire à la santé de la femme et du bébé ?",
              "answers": [{
                  "key": "1",
                  "text": "vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                },
              ]
            },
            {
              "id": 2,
              "question": "La contraception est un ensemble de méthodes et de moyens utilisés pour prévenir une grossesse ou des IST ?",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "Les méthodes contraceptives font perdre le contrôle sur son propre corps  ?",
              "answers": [{
                  "key": "1",
                  "text": "vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 4,
              "question": "L’utilisation des méthodes contraceptives peut être discuté et choisit avec son partenaire ?",
              "answers": [{
                  "key": "1",
                  "text": "vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "faux",
                  "value": "false"
                }
              ]
            }
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E4/4-2.jpg",
        "imageLinkLocked": "~/data/videos/E4/4-2g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E4_low/FR_E4_S2.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "A ton avis, quelles sont les méthodes naturelles ?",
          "answers": [{
              "key": "",
              "text": "Les méthodes non inventées par les blancs "
            },
            {
              "key": "idiocy",
              "text": "Les méthodes qui ne nécéssitent pas un traitement médicamenteux et/ou chirurgical"
            },
            {
              "key": "helped-method",
              "text": "Les méthodes qui aident la femme à calculer sa période fertile"
            },
            {
              "key": "maybe",
              "text": "Je ne sais pas"
            }
          ]
        },
        "thematic": [{
          "title": "es méthodes de contraception ",
          "file": "image.png",
          "text": "<p>Les méthodes naturelles sont basées sur l’auto observation permettant de déterminer et de s’abstenir :</p>"+
          "<p>des rapports sexuels pendant la période de fertilité. Il s’agit de :</p>"+
          "<ul>"+
            "<li> La méthode de calendrier ; <li>"+
            "<li> La méthode de température ;<li>"+
            "<li> La méthode de glaire cervicale ;<li>"+
            "<li> La méthode symptothermique ;<li>"+
            "<li> La méthode des jours fixes ou collier de cycle ;<li>"+
            "<li> La méthode de l’allaitement maternel et de l’aménorrhée (MAMA).<li>"+
          "</ul>"+
          "<p> Au Bénin, plusieurs procédés et pratiques sont utilisés pour empêcher la survenue d’une grossesse non désirée. On les appelle des méthodes traditionnelles ;</p>"+
          "<ul>"+
            "<li> Les infusions et décoctions de plantes : jus de tamarin, graines de ricin, etc.… ; <li>"+
            "<li> L’utilisation de cordelette à la ceinture des femmes ;<li>"+
            "<li> L’utilisation des bagues, des amulettes ;<li>"+
            "<li> De simples formules magiques incantatoires ;<li>"+
            "<li> La méthode des jours fixes ou collier de cycle ;<li>"+
            "<li> L’abstinence totale.<li>"+
          "</ul>"+
          "<p> L’efficacité de ces méthodes n’est pas prouvée. C’est pourquoi ces méthodes sont déconseillées au profit des méthodes modernes.</p>"
        },
        ],
        "quiz":{
          "title": "Répondez aux questions en indiquant s’il s’agit d’une méthode traditionnelle ou naturelle ;",
          "questions":[
            {
              "id": 1,
              "question": "La méthode de calendrier",
              "answers": [{
                  "key": "1",
                  "text": "Traditionnelle",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "naturelle",
                  "value": "true"
                },
              ]
            },
            {
              "id": 2,
              "question": "De simples formules magiques incantatoires",
              "answers": [{
                  "key": "1",
                  "text": "Traditionnelle",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "naturelle",
                  "value": "false"
                },
              ]
            },
            {
              "id": 3,
              "question": "L’utilisation des bagues, des amulettes ",
              "answers": [{
                "key": "1",
                "text": "Traditionnelle",
                "value": "true"
              },
              {
                "key": "2",
                "text": "naturelle",
                "value": "false"
              },
              ]
            },
            {
              "id": 4,
              "question": "La méthode de température",
              "answers": [{
                "key": "1",
                "text": "Traditionnelle",
                "value": "false"
              },
              {
                "key": "2",
                "text": "naturelle",
                "value": "true"
              },
              ]
            },
            {
              "id": 5,
              "question": "La méthode de glaire cervicale",
              "answers": [{
                  "key": "1",
                  "text": "Traditionnelle",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "naturelle",
                  "value": "true"
                }
              ]
            },
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E4/4-3.jpg",
        "imageLinkLocked": "~/data/videos/E4/4-3g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E4_low/FR_E4_S3.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "Quelles sont les méthodes contraceptives que tu connais déjà ? ",
          "answers": [{
              "key": "pill",
              "text": "La pilule"
            },
            {
              "key": "condom",
              "text": "Le condom/femidom"
            },
            {
              "key": "all-previous",
              "text": "Toutes les méthodes citées par Bintou"
            },
            {
              "key": "maybe",
              "text": "Je ne sais pas"
            }
          ]
        },
        "thematic": [{
            "title": "Les méthodes modernes les plus utilisées au Bénin sont ;",
            "file": "image.png",
            "text":  "<p> L’efficacité de ces méthodes n’est pas prouvée. C’est pourquoi ces méthodes sont déconseillées au profit des méthodes modernes.</p>"+
            "<p><strong> Pilule. </strong> (COC et COP). Les pilules contiennent des hormones qui ressemblent à celles que fabriquent naturellement les ovaires. La femme doit prendre la pilule orale chaque jour pour que ça soit efficace.  </p>"+
            "<p><strong> Injectable. </strong> Un progestatif de synthèse est injecté par piqûre dans les deltoïdes de la femme tous les trois mois. Pendant 12 semaines (3 mois), le produit assure une contraception constante. Les injections doivent être faites à intervalles réguliers par un médecin ou une infirmière. </p>"+
            "<p><strong> Norplant. </strong> C’est une méthode efficace qui dure jusqu’à 5 ans. Elle se place sous la peu du  bras de la femme et peut entrainer une absence des règles ou des saignements irréguliers avec moins de crampes menstruelles.</p>"+
            "<p><strong> DIU.</strong> Le DIU (dispositif intra-utérin) est un dispositif contraceptif inséré dans l’utérus. Les DIU mesurent 3,5 cm de long, ils ont le plus souvent la forme de la lettre “T”, et sont en plastique, portant un ou plusieurs manchons de cuivre. Le DIU est inséré dans l’utérus par un médecin. </p>"+
            "<p><strong> Préservatifs.</strong> Les préservatifs (masculins et féminins) empêchent le passage des spermatozoïdes dans le vagin, et donc la fécondation. Le condom est en latex. Il est déroulé sur le sexe masculin en érection avant toute pénétration. Le préservatif féminin est inséré dans le vagin. C’est une méthode de double protection parce qu’il permet d’éviter aussi les IST et le VIH /SIDA.</p>"+
            "<p><strong> Spermicides.</strong> les spermicides sont des substances qui, comme leur nom l’indique, inactivent ou détruisent les spermatozoïdes. On les utilise sous forme d’ovules, de crèmes ou d’éponges insérés au fond du vagin. On peut les utiliser seuls ou, de préférence en association avec une méthode “ barrière “ (préservatif masculin ou féminin, diaphragme, cape cervicale).</p>"
          },
        ],
        "quiz": {
          "title": "Choix multiples (Choisir entre 4 options) , Indique quelle méthode est expliquée",
          "questions":[
            {
              "id": 1,
              "question": "Le fait de s’interdire un rapport sexuel ?",
              "answers": [{
                  "key": "1",
                  "text": "Abstinence,",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "pilule",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "préservatif",
                  "value": "true"
                },
                {
                  "key": "4",
                  "text": "DIU",
                  "value": "false"
                },
              ],
            },
            {
              "id": 2,
              "question": "Une enveloppe en caoutchouc mince conçue pour recouvrir le pénis en érection",
              "answers": [{
                  "key": "1",
                  "text": "Abstinence,",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "pilule",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "condom",
                  "value": "true"
                },
                {
                  "key": "4",
                  "text": "DIU",
                  "value": "false"
                },
              ],
            },
            {
              "id": 3,
              "question": "Une calotte en caoutchouc que la femme introduit au fond du vagin ?",
              "answers": [
                {
                  "key": "1",
                  "text": "Abstinence,",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "pilule",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "préservatif",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "Diaphragme",
                  "value": "true"
                },
              ],
            },
            {
              "id": 4,
              "question": "Une enveloppe en caoutchouc mince que l’on place dans le vagin lors du rapport sexuel ?",
              "answers": [{
                  "key": "1",
                  "text": "Abstinence,",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "pilule",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "femidom",
                  "value": "true"
                },
                {
                  "key": "4",
                  "text": "DIU",
                  "value": "false"
                },
              ],
            },
            {
              "id": 5,
              "question": "Les contraceptifs oraux qui empêchent la grossesse en bloquant la libération de l’ovule. ?",
              "answers": [{
                  "key": "1",
                  "text": "Spermicides",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "pilule",
                  "value": "true"
                },
                {
                  "key": "3",
                  "text": "préservatif",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "DIU",
                  "value": "false"
                },
              ],
            },
            {
              "id":6,
              "question": "Un petit appareil flexible en plastique avec des manchons de cuivre qu’on pose dans l’utérus de la femme ?",
              "answers": [{
                  "key": "1",
                  "text": "Spermicides,",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "pilule",
                  "value": "false"
                },
                {
                  "key": "3",
                  "text": "préservatif",
                  "value": "false"
                },
                {
                  "key": "4",
                  "text": "DIU",
                  "value": "true"
                }
              ]
            },
          ],
        }
      },
      {
        "imageLink": "~/data/videos/E4/4-4.jpg",
        "imageLinkLocked": "~/data/videos/E4/4-4g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E4_low/FR_E4_S4.mp4"
        },
        "dialogue": {
          "id": 1,
          "question": "Comment Bio pourrait convaincre son père de l’utilisation d’un contraceptif ?",
          "answers": [{
              "key": "explain",
              "text": "Lui expliquer ce qu’il a appris de Bintou"
            },
            {
              "key": "talk",
              "text": "Dire qu’il doit aller parler avec Bintou"
            },
            {
              "key": "talk",
              "text": "Parler à la jeune belle-mère et au père"
            },
            {
              "key": "maybe",
              "text": "Il ne peut pas le convaincre"
            }
          ]
        },
        "aretenir": [{
            "title": "Pour les jeunes, la contraception est un moyen sûr et efficace pour se protéger contre les IST, le VIH/SIDA, les grossesses non désirées.",
            "file": "image.png",
            "text": "<p> Une grossesse non désirée peut être évitée, il existe des moyens et méthodes contraceptifs sûrs et efficaces : </p>"+
            "<ul>"+
              "<li> Pilule <li>"+
              "<li> Injectable <li>"+
              "<li> Norplant <li>"+
              "<li> DIU <li>"+
              "<li> Préservatifs <li>"+
              "<li> Spermicides <li>"+
            "</ul>"+
            "<p> Les méthodes contraceptifs doivent êtres choisit selon les préférences de la personne qui l’utilise. Elle ne peut être passée à une amie car elle est personnelle à chaque femme. </p>"+
            "<p> Pour des produits contraceptifs sûrs et efficaces, adressez-vous aux structures de santé ou aux pharmacies. </p>"
          },
        ],
        "engagement": [{
          "title": "Je m’engage à ",
          "file": "image.png",
          "text": "<p> Une grossesse non désirée peut être évitée, il existe des moyens et méthodes contraceptifs sûrs et efficaces : </p>"+
          "<ul>"+
            "<li> Donner l’importance qu’il faut à la planification familiale. <li>"+
            "<li> Choisir la méthode de planification familiale appropriée à son temps. <li>"+
            "<li> Avoir une sexualité responsable, et faire respecter mes choix en toutes circonstances. <li>"+
          "</ul>"
          },
        ],
        "discussions": [{
          "text": "<ul>"+
            "<li> Pourquoi l’utilisation des contraceptifs est-elle importante pour les jeunes ?  <li>"+
            "<li> Quels sont les avantages d'une bonne planification familiale ? <li>"+
            "<li> Quelles sont les méthodes de contraception les plus utilisées dans votre communauté ? <li>"+
          "</ul>"
          },
        ],
      },
    ],
  },
  {
    "id": 5,
    "title": "Estime de soi",
    "icon": "~/data/icons/icon-lesson-5.png",
    "episodes": [
      {
        "imageLink": "~/data/videos/E5/5-1.jpg",
        "imageLinkLocked": "~/data/videos/E5/5-1g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E5_low/FR_E5_S1.mp4",
        },
        "dialogue": {
          "id": 1,
          "question": "De quoi pensez vous q’Assiba a besoin?",
          "answers": [{
              "key": "attention",
              "text": "Que John lui prête de l’attention"
            },
            {
              "key": "clothes",
              "text": "D’acheter des nouveaux habits"
            },
            {
              "key": "appreciation",
              "text": "S’apprécier telle qu’elle est"
            },
            {
              "key": "beauty",
              "text": "D’entendre de la part de ses amis qu’elle est belle"
            }
          ]
        },
        "thematic": [{
            "title": "Dans l’épisode on voit qu’Assiba se trouve moche, souffre du manque d’attention de John et décide de s'isoler de ses camarades.",
            "file": "image.png",
            "text": "<p>Nous passons tous par des phases difficiles ou on se sent mal dans notre peau, on imagine que le monde entier se moque de nous derrière notre dos. Cette phase est souvent liée à l’adolescence et est une période vulnérable qu’il faut prendre au sérieux. Il est aussi important de savoir que plus on renforce le sentiment de se sentir moche en se le disant souvent et en se sentant mal devant ses camarades, plus cette réalité devient la nôtre.</p>" +
                    "<p>En s’isolant on a tendance à aggraver la situation en s’apitoyant sur notre sort. Cela peut dans certains cas s’aggraver avec des syndromes de dépression.</p>" +
                    "<p>La meilleure chose à faire est de se confier a ceux qu’on aime et de réaliser ce qui nous rend heureux et dont on peut être fier.</p>"
          }
        ],
        "quiz": {
          "title": "Indiquez si les déclarations sont vraies ou fausses",
          "questions": [
            {
              "id": 1,
              "question": "Les hommes sont attirés par la beauté artificielle.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 2,
              "question": "La taille des seins détermine sa virginité.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "On peut se convaincre qu’on est moche et devenir dépressif.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
            {
              "id": 4,
              "question": "La crainte de ne pas plaire peut nous isoler de nos camarades.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "On ne travaille pas à l’école quand on est amoureux(se).",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            }
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E5/5-2.jpg",
        "imageLinkLocked": "~/data/videos/E5/5-2g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E5_low/FR_E5_S2.mp4",
        },

        "dialogue": {
          "id": 1,
          "question": "Qu’est-ce qu’Assiba et Paulin ont en commun dans cet épisode ?",
          "answers": [{
              "key": "heart",
              "text": "Ils ont des problèmes de cœur."
            },
            {
              "key": "distance",
              "text": "Ils s’isolent de leurs amis."
            },
            {
              "key": "quality",
              "text": "Ils sont incapables d’apprécier leurs qualités."
            },
            {
              "key": "depression",
              "text": "Ils dépriment."
            }
          ]
        },
        "thematic": [{
            "title": "Assiba et Paulin réalisent qu’ils souffrent du même mal et décident d’aller voir Lina qui semble ne pas en souffrir.",
            "file": "image.png",
            "text": "<p>Le manque d’estime de soi et l’impression de ne pas réussir à se conformer à la norme du groupe peut arriver à tout le monde dans des moments de faiblesse. L’estime de soi est la compétence clefs à exercer pour désamorcer ce mécanisme de spirale négative. Il y a trois types d’estime de soi :</p>" +
                    "<ul>" +
                      "<li>L’estime de soi sociale qui passe par ce que l&#39;on montre aux autres et ce que l&#39;on reçoit d’eux.</li>" +
                      "<li>L’estime de soi lié à la performance, par exemple au travail ou dans une activité sportive ou de loisirs...</li>" +
                      "<li>L’estime de soi inconditionnelle est la plus difficile à obtenir mais aussi la plus solide. S&#39;aimer inconditionnellement même lorsque l’on n’est pas parfait. C&#39;est elle qui nous protège dans les moments difficiles de notre vie, lors de nos échecs ou lorsque nous ne sommes pas approuvé par les autres.</li>" +
                    "</ul>"
          }
        ],
        "quiz": {
          "title": "Répondez aux questions en indiquant vrai ou faux",
          "questions": [
            {
              "id": 1,
              "question": "Les garçons qui manquent d’estime de soi sont impuissants.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "On peut être heureux et se sentir important sans petit(e) ami(e)s.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "La dépression rend courageux.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 4,
              "question": "L’isolement peut résoudre nos problèmes de cœur.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "C’est l’avis des autres qui m’indiquent si je suis beau ou belle.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E5/5-3.jpg",
        "imageLinkLocked": "~/data/videos/E5/5-3g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E5_low/FR_E5_S3.mp4",
        },
        "dialogue": {
          "id": 1,
          "question": "Selon toi que signifie le fait de se sentir bien dans sa peau ?",
          "answers": [{
              "key": "value",
              "text": "C’est porter des vêtements qui me mettent en valeur."
            },
            {
              "key": "admiration",
              "text": "C’est savoir que je suis admiré(e) pour ma beauté."
            },
            {
              "key": "unique",
              "text": "C’est reconnaitre ce qui fais de moi un être unique."
            },
            {
              "key": "selflove",
              "text": "C’est m’aimer comme je suis."
            },
            {
              "key": "notes",
              "text": "C’est avoir des bonnes notes en classe."
            }
          ]
        },
        "aretenir": [{
            "title": "A Retenir",
            "file": "image.png",
            "text": "<p>Pour entretenir son estime de soi il faut du temps comme pour les amitiés c’est une « relation avec soi » qui doit s’entretenir et recevoir de l’attention. Il faut savoir être bon et patients. Il faut aussi être capable de reconnaitre nos talents, afin et de prendre conscience de nos spécialités de renforcer notre personnalité.</p>" +
                    "<p>Apprendre à s’aimer et se donner de la valeur est probablement la chose la plus importante pour une bonne estime de soi. Cela peut paraitre ridicule mais dites-le-vous régulièrement devant le miroir, petit à petit vous réussirez à le sentir en vous-même. Ce qui est magique avec cela c’est que quand on réussit à ressentir un amour sincère pour nous même cela devient contagieux pour les gens que nous rencontrons.</p>"
          },
        ],
        "engagement": [{
            "title": "Engagement",
            "file": "image.png",
            "text": "<p>Je m’engage à apprendre à m’aimer comme je suis</p>" +
                    "<p>Je m’engage à me confier à ceux que j’aime</p>" +
                    "<p>Je m’engage à soutenir ceux qui en on besoin</p>"
          }
        ],
        "discussions": []
      },
    ]
  },
  {
    "id": 6,
    "title": "Relations Saines",
    "icon": "~/data/icons/icon-lesson-6.png",
    "episodes": [
      {
        "imageLink": "~/data/videos/E6/6-1.jpg",
        "imageLinkLocked": "~/data/videos/E6/6-1g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E6_low/FR_E6_S1.mp4",
        },
        "dialogue": {
          "id": 1,
          "question": "A votre avis qu’est-ce qu’une relation saine?",
          "answers": [{
              "key": "consideration",
              "text": "C’est considérer son partenaire comme son égale"
            },
            {
              "key": "respect",
              "text": "C’est respecter les limites que nos parents nous imposent"
            },
            {
              "key": "culture",
              "text": "C’est trouver un partenaire de la même culture"
            },
            {
              "key": "plans",
              "text": "C’est avoir des plans d’avenir ensemble"
            },
            {
              "key": "conflits",
              "text": "C’est être capable de résoudre des conflits ensemble"
            }
          ]
        },
        "thematic": [{
            "title": "Dans cet épisode Bio est tout fâché et n’est pas en accord avec les décisions de son père.",
            "file": "image.png",
            "text": "<p>Bien souvent nos parents veulent notre bien et communiquent d’une manière qui semble dire le contraire. La meilleure façon d’éviter le conflit est de parler de façon ouverte et respectueuse et surtout de ne rien cacher. Un dialogue ouvert entre parents et enfants permet bien souvent d’éviter les conflits et malentendus.</p>" +
                    "<p>Une relation de parents enfants est doit rester saine et respectueuse. Il y a des limites à ne pas dépasser des deux côtés. Et comme vous le savez à présent la meilleure façon de se mettre d’accord sur ces limites est d’en parler de façon ouverte.</p>" +
                    "<p>Nos parents ne sont pas plus différents que nous, ils ont aussi des moments de faiblesse et parfois ont aussi besoin d’aide et de votre patience. Si nous leur faites confiance nos parents peuvent nous aider à accomplir nos objectifs</p>"
          }
        ],
        "quiz": {
          "title": "Indiquez si les déclarations sont vraies ou fausses",
          "questions": [
            {
              "id": 1,
              "question": "Un adulte qui aborde le sujet de la sexualité avec un(e) jeune l’incite à la débauche.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 2,
              "question": "Les parents ne peuvent pas comprendre les relations entre jeunes.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "Une fille vierge ne va pas au ciel.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 4,
              "question": "Un dialogue ouvert peut résoudre la majorité des conflits.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "La sexualité est une affaire d’adultes.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            }
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E6/6-2.jpg",
        "imageLinkLocked": "~/data/videos/E6/6-2g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E6_low/FR_E6_S2.mp4",
        },

        "dialogue": {
          "id": 1,
          "question": "Que feriez-vous à la place de bio ?",
          "answers": [{
              "key": "autority",
              "text": "Porter plainte aux autorités contre son père."
            },
            {
              "key": "secret",
              "text": "Continuer à voir Pauline en secret sous le nez de son père."
            },
            {
              "key": "notalk",
              "text": "Ne plus parler à son père."
            },
            {
              "key": "quit",
              "text": "Quitter pauline pour faire plaisir a son père."
            }
          ]
        },
        "thematic": [{
            "title": "Dans cet épisode Bio nous explique la réaction de son père face à sa petite amie Paulette. Bio est alors tout remonté et veut porter plainte contre son père.",
            "file": "image.png",
            "text": "<p>Dans cet épisode Bio ne comprend pas la réaction de son père. Il interprète alors que son père n’est pas à la page et qu’il est arriéré en termes de relations amoureuses. La violence de la réaction du père de bio le laisse apparemment bouleversé, cela peut être une forme de violence psychologique.</p>" +
                    "<p>Bio quand a lui est en relation avec Paulette et semble tenir a elle. Il est important pour des jeunes couples de discuter de leur relation et d’établir ensemble les règles de jeux pour établir une sensation de sécurité pour les deux partenaires. À tout moment il faut avoir de l’empathie et être capables de se mettre à la place de l’autre en se demandant « est-ce que j’aimerais qu’on me fasse/me dise ça ? » C’est l’empathie qui est la base du respect et d’une relation égalitaire.</p>" +
                    "<p>Dans une relation saine il est important de se sentir bien et d’avoir du plaisir ensemble, de rire ensemble et de bien s’entendre. C’est la compatibilité de savoir s’offrir ces moments de plaisir l’un a l’autre qui rend une relation intéressante. Comprendre ce que son partenaire apprécie c’est aussi avoir de l’empathie.</p>"
          }
        ],
        "quiz": {
          "title": "Répondez aux questions par vrai ou faux",
          "questions": [
            {
              "id": 1,
              "question": "Une relation amoureuse sans sexe ne sert à rien.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "Se marier en plus jeune que 18 ans est interdit par la loi.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "Cela porte malchance de ne pas épouser ton premier amour.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 4,
              "question": "Se marier jeune c’est profiter de la vie.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "Ce sont les parents qui décident les mariages.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E6/6-3.jpg",
        "imageLinkLocked": "~/data/videos/E6/6-3g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E6_low/FR_E6_S3.mp4",
        },
        "dialogue": {
          "id": 1,
          "question": "Selon vous quelle est la chose la plus importante en amour ?",
          "answers": [{
              "key": "agree",
              "text": "Avoir l’accord des parents."
            },
            {
              "key": "attraction",
              "text": "L’attirance physique."
            },
            {
              "key": "romance",
              "text": "La romance."
            },
            {
              "key": "study",
              "text": "Le fait qu’on valorise tout les deux nos études."
            },
            {
              "key": "protection",
              "text": "La protection contre les IST."
            },
            {
              "key": "popularity",
              "text": "L’image et la popularité."
            }
          ]
        },
        "aretenir": [{
            "title": "Les mots clés des relations saines sont :",
            "file": "image.png",
            "text": "<ul>"+
            "<li>Le respect</li>"+
            "<li>La confiance</li>"+
            "<li>La communication</li>"+
            "<li>Être capable de résoudre des problèmes</li>"+
            "<li>Rester soi même</li>"+
            "<li>L’égalité</li>"+
            "<li>Le sentiment de sécurité</li>"+
            "<li>L’empathie</li>"+
            "<li>Le plaisir</li>"+
          "<ul>" +
          "<p>Si vous sentez que votre relation n’est pas en équilibre ou que vous avez des amis qui sont dans la même situation, n’hésitez pas à demander de l’aide a vos amis ou a des personnes en qui vous avez confiance. Les situations les plus graves sont celles qui sont ignorées.</p>"
          },
        ],
        "engagement": [{
            "title": "Engagement",
            "file": "image.png",
            "text": "<p>Je m’engage à respecter mon/ma partenaire comme il/elle est</p>" +
                    "<p>Je m’engage à apporter mon aide a un/une ami(e)</p>" +
                    "<p>Je m’engage à écouter les conseils de mes parents</p>"
          }
        ],
        "discussions": []
      },
    ]
  },
  {
    "id": 7,
    "title": "Toxicomanie",
    "icon": "~/data/icons/icon-lesson-7.png",
    "episodes": [
      {
        "imageLink": "~/data/videos/E7/7-1.jpg",
        "imageLinkLocked": "~/data/videos/E7/7-1g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E7_low/FR_E7_S1.mp4",
        },
        "dialogue": {
          "id": 1,
          "question": "Que ferriez vous a la place de Paulin ?",
          "answers": [{
              "key": "no-smoke",
              "text": "Essayer de convaincre Lina que fumer est dangereux pour la santé"
            },
            {
              "key": "denonciation",
              "text": "Aller la dénoncer aux surveillants"
            },
            {
              "key": "smoke",
              "text": "S’asseoir et fumer avec la bande"
            },
            {
              "key": "consequence-smoke",
              "text": "Rechercher les effets des narcotiques sur le corps"
            },
            {
              "key": "best-friend",
              "text": "Partir et aller prévenir Assiba la meilleure amie de Lina"
            }
          ]
        },
        "thematic": [{
            "title": "Dans cet épisode Paulin est choqué de retrouver Lina succomber au suivisme et de fumer et boire de l’alcool.",
            "file": "image.png",
            "text": "<p>Cela vous arrivera surement aussi, « ne t’inquiète pas ce n’est pas bien grave juste une cigarette » « vient prendre une taffe » « si je prends un verre tu dois aussi en prendre un ». La pression des pairs est forte surtout quand il s’agit de prendre les mauvaises décisions. Ça s’appelle le suivisme.</p>" +
                    "<p>Le suivisme c’est l’attitude de quelqu’un qui suit la mode ou les idées d’une autre personne sans esprit critique. C’est bien souvent la cause de la toxicomanie chez les jeunes. La toxicomanie est une consommation abusive de substances, par exemple l’alcool, le tabac, les amphétamines, la caféine, le cannabis, etc., ou une dépendance physique ou psychologique à ces substances.</p>" +
                    "<p>Les gens consomment de l’alcool ou des drogues pour toutes sortes de raisons, entre autres, pour se détendre, pour diminuer les inhibitions, pour des pratiques religieuses ou pour s’intégrer à un groupe. Souvent, ils ne développent pas de problème de consommation. Toutefois, la consommation devient problématique lorsqu’elle entraîne des conséquences néfastes et qu’il y a une perte de contrôle. Ces problèmes doivent être abordés et peuvent être traités.</p>"
          }
        ],
        "quiz": {
          "title": "Indiquez si les déclarations sont vraies ou fausses",
          "questions": [
            {
              "id": 1,
              "question": "Il est difficile d’arrêter de fumer une fois qu’on a commencé.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "Fumer renforce les nerfs.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 3,
              "question": "L’alcool donne de l’appétit.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            },
            {
              "id": 4,
              "question": "L’alcool peut rendre aveugle.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
            {
              "id": 5,
              "question": "Un verre d’alcool par jour est considéré comme une addiction.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "false"
                }
              ]
            },
            {
              "id": 6,
              "question": "L’alcool permet a la femme enceinte d’avoir de beaux enfants.",
              "answers": [{
                  "key": "1",
                  "text": "Vrai",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Faux",
                  "value": "true"
                }
              ]
            }
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E7/7-2.jpg",
        "imageLinkLocked": "~/data/videos/E7/7-2g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E7_low/FR_E7_S2.mp4",
        },

        "dialogue": {
          "id": 1,
          "question": "Comment faire pour être considéré comme quelqu’un de populaire ?",
          "answers": [{
              "key": "imitation",
              "text": "Il faut imiter le groupe des gens qui sont à la mode."
            },
            {
              "key": "must-smoke",
              "text": "Il faut fumer pour être cool."
            },
            {
              "key": "must-drink",
              "text": "Il faut boire un coup pour se détendre."
            },
            {
              "key": "respect-body",
              "text": "Il faut rester intègre et respecter son corps."
            },
            {
              "key": "good-friend",
              "text": "Ça ne sert à rien d’être populaire il vaut mieux avoir des amis qui t’apprécient comme tu es."
            }
          ]
        },
        "thematic": [{
            "title": "Dans cet épisode Paulin et Assiba sont étonnés d’entendre que Lina fume avec la bande de l’école.",
            "file": "image.png",
            "text": "<p>Pour Paulin il est important de prévenir Assiba la meilleure amie de Lina car il remarque qu’elle est victime de suivisme, cela ne lui ressemble pas et il est déterminé à lui apporter son aide. Assiba également n’en croit pas ses oreilles.</p>" +
                    "<p>Il est tentant de compromettre notre personnalité pour faire partie du groupe. On a tendance a dire « Où est le mal ? Beaucoup de gens n’essaient-ils pas les drogues ou l’alcool ? ». Beaucoup de jeunes qui ont essayé l&#39;alcool ou les drogues n&#39;en deviendront pas dépendants ; toutefois, un pourcentage non négligeable souffrira de problèmes de dépendance.</p>" +
                    "<p>Selon les sondages 61 % des élèves ont consommé de l&#39;alcool et 29 % ont pris des drogues illicites comme la marijuana. 19 % des élèves consomment des quantités dangereuses d&#39;alcool et 15 % consomment des drogues à un point qui pose problème.</p>"
          }
        ],
        "quiz": {
          "title": "Répondez aux questions par vrai ou faux",
          "questions": [
            {
              "id": 1,
              "question": "Consommer de l’alcool permet de rester en bonne santé.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "Consommer de l’alcool permet de rester en bonne santé.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 3,
              "question": "L’alcool tue le virus du sida.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 4,
              "question": "La cigarette contient du poison.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 5,
              "question": "Fumer empêchera la fille de tomber enceinte.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 6,
              "question": "Fumer et boire régulièrement coute beaucoup d’argent.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            }
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E7/7-3.jpg",
        "imageLinkLocked": "~/data/videos/E7/7-3g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E7_low/FR_E7_S3.mp4",
        },
        "dialogue": {
          "id": 1,
          "question": "Comment aideriez-vous Lina ?",
          "answers": [{
              "key": "bad-effect",
              "text": "En recherchant les effets de l’alcool et du canabis lur le corps."
            },
            {
              "key": "join-group",
              "text": "En rejoignant le groupe et en essayant comme elle."
            },
            {
              "key": "reject-her",
              "text": "En la rejettant."
            },
            {
              "key": "smoke-motivation",
              "text": "En essayant de comprendre ce qui la motive a consommer des drogues."
            }
          ]
        },
        "thematic": [{
            "title": "Explication",
            "file": "image.png",
            "text": "<p>L’adolescence est une période à risque vis-à-vis des nouvelles expérimentations. C’est souvent un ado du même âge, voire un copain, qui joue le rôle d’initiateur. De plus, l’adolescence est marquée par le recul vis-à-vis des parents et le besoin de s’identifier à ses pairs.</p>" +
                    "<p>Beaucoup d’ados se laissent tenter sans trop savoir pourquoi. En revanche, ceux qui refusent savent toujours pourquoi : parce qu’ils ont des projets, qu’ils tiennent à leur bonne santé, qu’ils font des compétitions sportives, qu’ils ont envie de s’inscrire en conduite accompagnée, qu’ils n’ont pas envie de passer des années en prison, etc.</p>" +
                    "<p>S’amuser à lister les bonnes raisons de ne pas y toucher entre amis, c’est éducatif !</p>"
          }
        ],
        "quiz": {
          "title": "Répondez aux questions par vrai ou faux",
          "questions": [
            {
              "id": 1,
              "question": "Pour lutter contre l’insomnie il faut boire de l’alcool régulièrement.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 2,
              "question": "Les risques de la toxicomanie sont faibles chez les jeunes.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            },
            {
              "id": 3,
              "question": "En moyenne chaque cigarette réduit votre espérance de vie de 10 minutes.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 4,
              "question": "Un(e) ami(e) qui t’amène à fumer ou à boire n’est pas un(e) bon(ne) ami(e).",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 5,
              "question": "Consommer de l’alcool agit négativement sur le rendement scolaire.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "false"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "true"
                }
              ]
            },
            {
              "id": 6,
              "question": "La consommation d’alcool augmente la puissance sexuelle.",
              "answers": [{
                  "key": "1",
                  "text": "Faux",
                  "value": "true"
                },
                {
                  "key": "2",
                  "text": "Vrai",
                  "value": "false"
                }
              ]
            }
          ]
        }
      },
      {
        "imageLink": "~/data/videos/E7/7-4.jpg",
        "imageLinkLocked": "~/data/videos/E7/7-4g.jpg",
        "video": {
          "file": "~/data/fr/videos/FR_E7_low/FR_E7_S4.mp4",
        },
        "dialogue": {
          "id": 1,
          "question": "Comment éviter de se laisser entrainer par le groupe ?",
          "answers": [{
              "key": "agree",
              "text": "En choisissant des amis pour qui ils sont et pas pour ce qu’ils représentent."
            },
            {
              "key": "who-iam",
              "text": "En étant sur de qui je suis."
            },
            {
              "key": "become-cool",
              "text": "En devenant plus cool qu’eux."
            },
            {
              "key": "no-ghettos",
              "text": "En évitant de trainer dans les ghettos."
            },
            {
              "key": "inform-risks",
              "text": "En m’informant sur les risques."
            }
          ]
        },
        "aretenir": [{
            "title": "A Retenir",
            "file": "image.png",
            "text": "<p>Les adolescents qui prennent des « cuites » (plus de cinq verres coup sur coup) sont plus à risque de mauvais résultats scolaires, de violence dans leurs fréquentations, de tentatives de suicide ou d’autres activités qui compromettent leur santé, par exemple, des rapports sexuels non protégés. L’alcoolisme et la toxicomanie ont des répercussions non seulement pour les personnes mais pour toute la société, en termes de baisse de productivité, de hausse de la criminalité et de progression de l’itinérance.</p>" +
                    "<p>La consommation de tabac comporte de graves risques de santé tels que le cancer du poumon de la langue ou de la gorge. Fumer diminue l’espérance de vie de 10 min par cigarette.</p>" +
                    "<p>L’alcool contient des substances qui peuvent rendre aveugle</p>" +
                    "<p>En résistant à la pression des pairs et en restant intègres par rapport à nos valeurs on arrive à avoir une meilleure estime de soi.</p>"
          },
        ],
        "engagement": [{
            "title": "Engagement",
            "file": "image.png",
            "text": "<p>Je m’engage à ne pas commencer à fumer</p>" +
                    "<p>Je m’engage à rester intègre par rapport a mes valeurs</p>" +
                    "<p>Je m’engage à résister à la pression du groupe</p>"
          }
        ],
        "discussions": []
      },
    ]
  },
];

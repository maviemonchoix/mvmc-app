module.exports = [
  {
    "id": 1,
    "title": "CONTRACEPTION/PLANNING FAMILIAL",
    "questions": [
      {
        "question": "La contraception : qu’est-ce que c’est ?",
        "answer": "La contraception est l’ensemble des moyens qui empêchent la survenue d’une grossesse. La contraception est ainsi une composante de planning familiale. Elle utilise des moyens naturels et modernes. Tous les moyens de contraception évitent la grossesse mais pas lesIST dont fait partie le sida. LesIST peuvent être pour la plupart évitées par les préservatifs masculin et féminin."
      },
      {
        "question": "C’est quoi la pilule contraceptive ?",
        "answer": "La pilule est un moyen de contraception qui empêche l'ovulation grâce à des hormones de synthèse. Elle évite ainsi une grossesse non désirée mais ne protège pas des <a href=\"http://www.ciao.ch/f/sexualite/infos/ab15eb0c8ef35c191c0aead0a6ffa46f/le-safer_sex--ou_sexe_a_moindre_risque\">IST</a>. Seul le préservatif permet de se protéger des grossesses et des IST."
      },
      {
        "question": "La pilule contraceptive protège-t-elle d'une grossesse tous les jours du cycle ?",
        "answer": "Oui, la pilule contraceptive prise selon les prescriptions, sans oublis, te protège d'une grossesse non planifiée tous les jours du cycle, y compris pendant la pause. Rappelle-toi encore une fois que la pilule ne protège pas des infections sexuellement transmissibles, pour ce faire il est nécessaire d'utiliser le préservatif."
      },
      {
        "question": "Où acheter le préservatif ?",
        "answer": "<p>Les préservatifs peuvent s'acheter soit en pharmacie, soit dans la plupart des boutiques et kiosques. Le prix n'est pas forcément un gage de qualité.</p>" +
                  "Tu peux aussi en acheter dans les centres de santé et de plannings à des prix spécialement abordables. Tu peux te sentir un peu gêné à l'idée d'acheter des préservatifs et c'est normal. Pas besoin de montrer une carte d'identité, tu as le droit de le faire même si tu n'as pas 16 ans au contraire c'est une attitude responsable."
      },
      {
        "question": "Comment conserver le préservatif ?",
        "answer": "Les préservatifs n'aiment ni le soleil, ni la chaleur, ni être trop serrés comme dans un porte-monnaie ou dans une poche. À la maison ils sont très bien dans un tiroir, et pour le transport, si la boîte entière est trop encombrante, une petite boîte de bonbons vide ou un étui de la bonne forme peut être pratique."
      },
      {
        "question": "Les règles irrégulières sont-elles normales ?",
        "answer": "<p>Rassure-toi, avoir des règles irrégulières est tout à fait normal. Cette irrégularité est très courante durant les deux premières années des règles, c'est normal. Il n'y a aucun risque pour le vagin, ni pour l'ensemble des organes génitaux.<br>Avec le temps ton cycle devrait se régulariser de lui-même.</p>" +
                  "Toutefois, bien que cette irrégularité soit normale, elle peut être \"inconfortable\" et elle peut susciter un certain nombre de questions (ce qui semble être ton cas puisque tu nous écris).<br>Donc, bien que tout à fait normale et naturelle, il peut être utile de prendre un rdv chez ton pédiatre/médecin ou chez une gynécologue pour en discuter."
      },
      {
        "question": "Une PVVIH peut-elle faire le planning familial?",
        "answer": "<p>C’est indispensable qu’une PVVIH fasse la PF pour préserver sa santé et celle de ses enfants. La déficience immunitaire l’affaiblit et elle n’a plus les capacités de supporter facilement une grossesse. En cas de grossesse, elle risque des avortements à répétition, ce qui la fragilise davantage et si elle ne se fait pas suivre par du personnel qualifié pour l’ETME, elle accroit le risque de transmission du virus à son enfant en augmentant ainsi le nombre de PVVIH. Elle doit éviter tout ce qui peut la fatiguer et affecter son système immunitaire.</p>"
      },
      {
        "question": "Le retour de la fertilité après l'utilisation des contraceptives",
        "answer": "<p>Toutes les méthodes contraceptives sont réversibles à des  durées variables dès l’arrêt de la méthode. Certaines sont immédiatement réversibles après arrêt ou dans un temps très bref (Collier, DIU, pilule, implants),  et d’autres prennent un peu plus de temps (injectables). Dans les cliniques les centres de santé, les cliniques affiliées, des prestataires de santé, qualifiées sont formées pour conseiller sur les moments opportuns pour interrompre la méthode dès que la femme sous méthode désire une grossesse.</p>"
      },
      {
        "question": "Les pilules, les préservatifs  ne sont-ils pas des formes de dépravation?",
        "answer": "<p>Non, car c’est des méthodes qui permettent d’être protégé contre les grossesses non désirées, les IST et le VIH/Sida.</p>"
      },
      {
        "question": "Est-ce qu'une femme peut prendre  des pilules si elle n'a pas encore eu d'enfants?",
        "answer": "<p>Oui, une jeune fille peut prendre des pilules pour éviter les grossesses non désirées; mais elle doit le faire sur le conseil d’un médecin. De plus en tant que jeune fille, il est préférable de s’abstenir ou d’utiliser régulièrement le préservatif pendant les rapports sexuels.</p>"
      },
      {
        "question": "La pilule cause-elle des malformations chez les bébés?",
        "answer": "<p>Non, elle ne cause pas des malformations ; mais elle provoque des effets secondaires qui d’ailleurs sont temporaires et sans gravité.</p>"
      },
      {
        "question": "Quelles sont les conséquences de l'abstinence à longue durée?",
        "answer": "<p>Pas de conséquences. Elle demeure le meilleur moyen de prévention contre les IST et le VIH/Sida.</p>"
      },
      {
        "question": "Préoccupation sur le retour de la fertilité après l'utilisation des contraceptives?",
        "answer": "<p>Toutes les méthodes contraceptives sont réversibles à des durées variables dès l’arrêt de la méthode. Certaines sont immédiatement réversibles après arrêt ou dans un temps très bref (Collier, DIU, pilule, implants),  et d’autres prennent un peu plus de temps (injectables). Dans les cliniques et les centres de santé, des prestataires de santé, qualifiées sont formées pour conseiller sur les méthodes contraceptives.</p>"
      },
      {
        "question": "Que dois-je faire pour retirer les dispositifs d’implant restant sous ma peau ?",
        "answer": "<p>Toute femme dans le besoin de retirer l’implant, peut se rendre dans toutes les cliniques. Mais le retrait est subordonné à un coût.</p>"
      },
      {
        "question": "J’ai fait un rapport sexuel avec ma partenaire et j’aimerai que vous me conseilliez un médicament qu’elle peut prendre pour ne pas tomber enceinte?",
        "answer": "<p> Aller à la pharmacie et demander à  l’agent de vous proposer une pilule d’urgence que vous pouvez prendre dans les 24 heures qui suivent les rapports sexuels. Mais les prochaines fois utiliser le préservatif ou adopter une méthode de PF pour éviter tout risque de grossesse car les pilules d’urgences ne doivent pas être prises régulièrement pour éviter que l’organisme ne s’habitue.</p>"
      },
      {
        "question": "Mon mari ne veut pas que je fasse le planning familial; comment puis-je le convaincre?",
        "answer": "<p>Vous devez lui exposer les avantages liés à l’adoption d’une méthode de PF :</p>" +
                  "- Mère : elle a le temps de récupérer entre deux grossesses et elle peut vaquer à ses occupations et s’occuper correctement de son foyer <br>" +
                  "- Enfant : il sera bien nourrit au lait maternel et donc pourra bien grandir <br>" +
                  "- Père : il a le temps de bien planifier les besoins de la famille  pour le bonheur de tous. <br>"
      },
      {
        "question": "Pourquoi aller à l'hôpital pour adoption de la méthode de la pilule?",
        "answer": "<p>Parce que là-bas vous aurez droit à un counseling fait par un spécialiste  pour vous expliquer en détail le mode d’utilisation et aussi vous serez préparé psychologiquement aux éventuels effets secondaires liés à l’utilisation des pilules.</p>"
      },
      {
        "question": "Devrais-je accompagner ma femme au centre de santé pour l'adoption d'une méthode de planning familial?",
        "answer": "<p>Vous devez accompagner votre femme pour l’adoption d’une méthode de PF parce qu’elle a besoin de votre soutien et c’est pour le bonheur de toute la famille. L’accompagner est non seulement un signe d’amour mais aussi un geste de consentement.</p>"
      }
    ]
  },
  {
    "id": 2,
    "title": "IST/VIH/SIDA",
    "questions": [
      {
        "question": "Quelles sont les pratiques sexuelles à risque de transmission du VIH ?",
        "answer": "<p>Les pratiques sexuelles à risque sont :</p>" +
                  "- les pénétrations vaginales et anales non protégées par un préservatif lubrifié par un gel à base d’eau ou de silicone,<br>" +
                  "- les fellations, surtout avec éjaculation dans la bouche,<br>" +
                  "- les échanges de godemichets, ou autres objets, sans utilisation de préservatifs pour chaque partenaire. Le baiser, même avec échange de salive, n’est pas une pratique à risque de transmission du VIH. Pas de risque non plus, pour les papouilles, les chatouilles, les caresses de la peau."
      },
      {
        "question": "Qu’est-ce que le test de dépistage du VIH ?",
        "answer": "Le test de dépistage est un examen paramédical réalisé dans une structure de santé avec un personnel qualifié qui permet de connaitre ton statut sérologique, c’est-à-dire si tu n’as pas le VIH. Le test est volontaire, anonyme, confidentiel et gratuit. C’est un acte responsable."
      },
      {
        "question": "Les IST hors VIH peuvent-elles être graves ?",
        "answer": "Même si les IST sont moins « impressionnantes » que le VIH, il faut savoir deux choses à leur sujet : sans traitement efficace, elles peuvent avoir des conséquences graves (comme la stérilité) ; la présence d’IST accroît le risque de transmission du VIH en cas de rapport non protégé. Attention, ces dernières années, la fréquence des IST a augmenté au Bénin."
      },
      {
        "question": "Quand doit-on se faire consulter pour une IST ?",
        "answer": "Dès l’apparition de signes comme des boutons sur le sexe, des douleurs au bas-ventre, une sensation de brûlure en urinant, un écoulement anormal ou malodorant au niveau du sexe, tu peux aller dans un centre de conseil jeune ou dans une structure de santé afin d’être pris en charge."
      },
      {
        "question": "Le fait de se retirer avant éjaculation permet-il d’éviter la contamination ?",
        "answer": "Non. Le risque existe pendant la pénétration, d’une part en raison de la possible sécrétion de liquide pré séminal (liquide incolore, qui précède l’éjaculation) contenant potentiellement du VIH, et d’autre part en raison du frottement des muqueuses. Ce frottement induit des microlésions qui peuvent constituer une voie de contamination. Définitivement, seul le préservatif est efficace pour éviter la transmission du VIH."
      },
      {
        "question": "Peut-on être vierge et séropositif ?",
        "answer": "Oui, cela est possible. Le VIH peut se transmettre d’une mère séropositive à son enfant en l’absence de prise en charge médicale optimale. Des personnes naissent donc avec le VIH. Il est également possible d’avoir été contaminé durant l’enfance par d’autres voies que les rapports sexuels : transfusions de sang non sécurisées (cela n’est en théorie plus possible au Bénin, tous les dons sont extrêmement sécurisés), accidents d’exposition graves et non pris en charge médicalement. Heureusement, ces situations sont très rares."
      },
      {
        "question": "Etre vierge protège-t-il du sida ?",
        "answer": "Non. Il faut utiliser un préservatif dès le premier rapport. Ne pratiquer que des pénétrations anales (pour préserver une virginité par exemple) ne protège pas non plus du sida et des IST."
      },
      {
        "question": "Quelle est l’origine du virus VIH du sida?",
        "answer": "L’origine du sida n’est pas encore connue. Il est plus important de chercher les moyens d’empêcher l’infection que de savoir son origine spécifique."
      },
      {
        "question": "Quels sont les pays les plus touchés par le virus VIH du sida?",
        "answer": " L’épidémie est le plus grave en Afrique Centrale et Australe avec près de 40% de la population infectée dans plusieurs régions.  En Afrique de l’Ouest, l’impact négatif du sida augmente très vite à cause de l’immigration entre les  pays."
      },
      {
        "question": "Le sida n’existe pas. Il était fabriqué par les blancs pour augmenter les ventes de préservatifs?",
        "answer": "Le sida est une réalité mondiale ; c’est une infection grave qui tue beaucoup de monde dans notre pays.  Les préservatifs qui sont vendus au Bénin spécifiquement par l’ABMS sont subventionnés; ils sont vendus moins chers que leur coût réel.  (Par exemple, le préservatif Prudence Plus est vendu à 100FCFA pour un paquet de 4 préservatifs et le préservatif Koool condom à 150 FCFA pour un paquet de 3 préservatifs)"
      },
      {
        "question": "Le sida peut être guéri en faisant des rapports avec une vierge?",
        "answer": "Non.  Si vous êtes porteur du virus VIH sida et vous faites des rapports sexuels avec une fille qui est vierge, au lieu de vous guérir, vous transmettez le virus à cette fille (et vous serez toujours séropositif).  Il n’y a pas de remède. </br>" +
                  "Pour le virus VIH sida, il faut simplement se protéger pour éviter de le transmettre aux autres.  Si vous êtes déjà porteur du virus, il faut se rapprocher d'un agent de santé pour des conseils et des options de prise en charge."
      },
      {
        "question": "Les rapports sexuels protégés (utilisation préservatif) ne me donnent pas de plaisir?",
        "answer": " Au début, la sensibilité du pénis peut diminuer, mais après quelques temps, vous vous habituerez aux rapports sexuels agréables avec le préservatif. La plupart des stimulations sexuelles sont psychologiques ce qui veut dire qu’il vient de la tête."
      },
      {
        "question": "Les préservatifs ne sont pas de bonne qualité. Ils se déchirent facilement?",
        "answer": "Les préservatifs se déchirent quand vous ne les utilisez pas correctement. (Les étapes de port de préservatif n’ont pas été bien suivies)."
      },
      {
        "question": "Les préservatifs moins chers ne sont pas de bonne qualité?",
        "answer": "Il faut comprendre que les stratégies de marketing social rendent les produits accessibles aux populations locales, et vulnérables à un prix très abordable.  Ils sont toujours de la même qualité que les produits plus chers (ceux qui ne sont pas subventionnés). Par exemple, Prudence Plus , Koool Condoms, le Manix etc."
      },
      {
        "question": "Les préservatifs ont été créés pour faire arrêter des rapports sexuels et pour réduire la population Africaine?",
        "answer": "Les personnes qui sont mariées ou qui sont fidèles l’une à l’autre ne sont pas obligées d’utiliser des préservatifs, sauf comme une méthode de contraception. L’usage des préservatifs est un choix personnel. Ils sont efficaces contre les grossesses non désirées et la transmission des infections sexuellement transmissibles (IST)."
      },
      {
        "question": "Les femmes ne veulent pas utiliser de préservatifs parce qu’elles croient que leur utilisation leur donne la réputation d’une prostituée?",
        "answer": "L’utilisation du préservatif ne veut pas dire qu´on est prostituée ou n’indique pas qu’une femme est porteuse de maladies.  Un préservatif est pour la protection des deux partenaires contre les infections sexuellement transmissibles ou des grossesses non planifiées.  Il faut avoir un dialogue entre les deux partenaires pour faire comprendre à la femme que sa santé est également importante et doit être protégée aussi."
      },
      {
        "question": "L’Eglise me conseille de ne pas utiliser les préservatifs…?",
        "answer": "L’église Catholique est contre l’utilisation des contraceptifs artificiels. Même si c’est vrai, beaucoup de recherches et expérimentations scientifiques ont démontré que l’utilisation des préservatifs est le meilleur mode de prévention contre le VIH/sida quand l’abstinence sexuelle n’est pas possible. Par ailleurs, la même église nous demande de ne pas avoir de rapport sexuel avant le mariage. Pourtant nous en avons en dehors du mariage et peut être un ou plusieurs personnes.  Pourquoi prendre des risques et ne pas se protéger au moins."
      },
      {
        "question": "Les préservatifs contiennent le virus du sida pour exterminer la race Africaine?",
        "answer": "Un préservatif ne peut pas contenir le virus VIH parce qu’il est très fragile et ne peut pas vivre longtemps - plus de trois heures - hors du corps humain.  Quand on considère le délai entre la fabrication du préservatif et son arrivée au Bénin, au moins trois mois, il est clair que cette éventualité est complètement impossible"
      },
      {
        "question": "Quelle est la substance huilée du préservatif ?",
        "answer": "La substance à l’extérieur du préservatif n’est pas de l’huile.  C´est un lubrifiant à base d’eau.  Les lubrifiants à base d’eau sont ajoutés aux préservatifs pour faciliter l’acte sexuel.  Les lubrifiants à base d’autres substances ne sont pas conseillés ; ils peuvent affaiblir le latex du préservatif en le rendant facile à se déchirer."
      },
      {
        "question": "Est-ce que vous utilisez des préservatifs vous-même ?",
        "answer": "Bien sûr que oui, j’utilise les préservatifs depuis longtemps. Au début, comme vous, j’avais des difficultés avec les sensations réduites, mais maintenant ça va.  Ou, en tant que femme, je demande toujours qu’un homme porte son préservatif chaque fois au cours des rapports sexuels.  Je préfère l’assurance qu’ils me donnent en protégeant nos rapports sexuels que d’avoir des rapports sexuels sans préservatif et à risque.  Je garde aussi le plaisir."
      },
      {
        "question": "Est-ce que les Blancs utilisent la même qualité de préservatifs ?",
        "answer": "Oui, les blancs utilisent les mêmes préservatifs en matière latex."
      },
      {
        "question": "Est-ce qu’il y a un préservatif pour les femmes ?",
        "answer": "Oui, il y a un préservatif féminin appelé Préservatif Féminin qui sont disponibles dans les pharmacies du Bénin"
      },
      {
        "question": "Si le préservatif se déchire pendant le rapport sexuel, est-ce que les débris rentrent dans le vagin de la femme?",
        "answer": "Non. Les débris ne peuvent pas entrer dans le vagin parce que le col de l’utérus est le seul orifice qui peut laisser passer quelque chose.  Le col de l’utérus est très bien protéger et ne laisse que des spermatozoïdes entrer dans l’utérus de la femme. Aussi le préservatif étant en latex ne s’émiette pas il ne laisse donc pas de débris après déchirure."
      },
      {
        "question": "Les préservatifs sont trop épais.  Je n’ai pas de sensations?",
        "answer": "Les préservatifs en latex sont très fins aussi fins pour maximiser le plaisir sexuel en gardant la plus haute sûreté possible.  Au fait, il y a des individus qui croient que les préservatifs sont trop fins.  Ainsi, ils décident d’utiliser deux préservatifs à la fois.  Ceci est déconseillé, parce que le préservatif peut se déchirer à cause du frottement créé entre les deux préservatifs.  Un seul préservatif en latex utilisé correctement vous donne la protection suffisante contre les grossesses non désirées, la transmission de VIH et la transmission d’autres infections sexuellement transmissibles (IST)."
      },
      {
        "question": "Les préservatifs produisent des réactions allergiques?",
        "answer": "Ceci est possible, comme pour les  autres produits médicaux, la peau de certains individus est sensible au latex ou au lubrifiant à base d’eau. Ce problème est très rare. Si vous croyez que vous êtes allergique au latex ou aux lubrifiants à base d’eau, faites une consultation chez votre médecin pour avoir d’autres options."
      },
      {
        "question": "Il me semble que quelqu’un qui utilise des préservatifs n’est protégé qu’à 90%.  Et le 10% restant ?",
        "answer": "C’est vrai que les préservatifs ne sont pas efficaces à cent pour cent comme toute chose, mais si vous les utilisez correctement, les préservatifs réduisent énormément le risque de transmission des infections sexuellement transmissibles (IST). Déjà, cette réduction est considérable.  Seule l’abstinence sexuelle vous protège à 100%."
      },
      {
        "question": "Est-ce qu’on peut attraper le sida par des piqûres de moustiques ?",
        "answer": "Non, les moustiques ne peuvent pas transmettre le virus VIH/Sida parce qu’il n’y a pas d’échange de sang quand le moustique pique une personne.  De plus, la recherche nous montre que le virus ne peut pas vivre dans la salive des moustiques. Seuls les plasmodiums, les microbes qui donnent le paludisme, peuvent être transmis par la salive des moustiques."
      },
      {
        "question": "Il y a des gens qui disent que le liquide sur les préservatifs (le lubrifiant) rend stérile.  Est-ce vrai ?",
        "answer": "Jusqu´à présent, la recherche nous montre qu’il n'y a aucun cas de stérilité dû à l’utilisation des préservatifs.  Les préservatifs vous protègent contre les grossesses non désirées en créant une barrière entre le sperme de l’homme et le vagin de la femme ; ils n’affaiblissent ni le sperme de l’homme ni le rendent stérile.  Ils ne rendent pas la femme stérile non plus."
      },
      {
        "question": "Comment se transmet le virus de la mère à l'enfant?",
        "answer": "Pendant l'accouchement au cours de la traversée de la filière pelvienne; au cours de l'allaitement et pendant la grossesse."
      },
      {
        "question": "Le préservatif sert  à se protéger du VIH ; est ce que quand on le perce pour pouvoir engrosser  une  femme est ce qu'on peut la contaminer?",
        "answer": "Oui, parce que le virus est présent dans le sperme."
      },
      {
        "question": "Y-a-t-il un vaccin contre le virus du VIH/Sida?",
        "answer": "<p>Non, pour le moment. Mais il y a les moyens de prévention :</p>" +
                  "- Abstinence" +
                  "- Bonne fidélité" +
                  "- Condom" +
                  "- Dépistage"
      },
      {
        "question": "Peut-on attraper le virus du VIH/Sida au cours d'un rapport sexuel  sans éjaculation?",
        "answer": "Oui, à travers le liquide séminal et les lésions au cours des rapports sexuels, des cunnilingus et la pipe."
      },
      {
        "question": "La différence entre VIH1 et VIH2?",
        "answer": "VIH 1 est plus virulent que le VIH2; ce qui est important c'est d’éviter les comportements à risque."
      },
      {
        "question": "Vu la taille du virus est-ce qu'il ne traverse pas le préservatif?",
        "answer": "Non, car les pores du préservatif ne laissent pas passer le virus. Et les préservatifs sont testés au laboratoire autant de les mettre sur le marché."
      },
      {
        "question": "La femme est-elle plus exposée au virus du VIH/Sida que l'homme? Est- ce à dire que c'est la femme qui contamine l'homme?",
        "answer": "Oui, la femme est exposée car elle est plus vulnérable que l’homme du point de vue morphologique ; la muqueuse du vagin de la femme est très fragile et favorise la pénétration du virus et des infections.  Or chez l'homme, il n'y a pas une grande ouverture qui facilite l'entrée du virus VIH/Sida."
      },
      {
        "question": "Le virus reste-t-il dans le sperme? Si oui, comment les spermatozoïdes survivent-ils?",
        "answer": "Oui; les spermatozoïdes survivent parce que le virus ne s’attaque pas à eux ni aux ovules."
      },
      {
        "question": "Quel est le rôle des ARV dans l'organisme?",
        "answer": "Empêchent la multiplication du virus et ainsi l'évolution de la maladie."
      },
      {
        "question": "Est-ce qu'une femme en menstrues peut tomber enceinte?",
        "answer": "Oui, c'est possible car les émotions peuvent entraîner l'ovulation."
      },
      {
        "question": "Comment placer le préservatif masculin/féminin?",
        "answer": "<p>Masculin</p>" +
        "<ul><li>Vérifier la date de péremption du préservatif</li>" +
        "<li>Triturer le préservatif pour vérifier qu’il y a de l’air dans l’emballage</li>" +
        "<li>Ouvrir délicatement l’emballage sans utiliser des objets coupant comme la lame, le ciseau</li>" +
        "<li>Sortir le préservatif</li>" +
        "<li>Pincer le bout du préservatif afin de chasser l’air</li>" +
        "<li>Poser le préservatif en forme de chapeau gobi sur la tête du pénis en érection après avoir vérifié le sens</li>" +
        "<li>Dérouler le préservatif jusqu’à couvrir le pénis entièrement</li>" +
        "<li>Passer au rapport sexuel</li>" +
        "<li>Se retirer du vagin juste après l’éjaculation</li>" +
        "<li>Utiliser un mouchoir pour enlever le préservatif qui ne doit pas être en contact avec la main nue</li>" +
        "<li>Attacher le bout du préservatif et ensuite le jeter dans les fosses septiques ou l’enterrer.</li></ul>" +
        "<p>Féminin</p>" +
        "<ul><li>Vérifier l’étanchéité de l’enveloppe du condom</li>" +
        "<li>Triturer le condom pour bien répartir le lubrifiant</li>" +
        "<li>Sortir le condom de l’enveloppe</li>" +
        "<li>Maintenir l’anneau du condom et le pincer pour avoir un huit</li>" +
        "<li>Poser un pied sur une chaise ou se mettre en position couchée sur le dos, introduire la bague ainsi pincée aussi loin que possible dans le vagin</li>" +
        "<li>Introduire deux doigts dans le condom ainsi placé pour vérifier qu’il n’est pas tordu et qu’il est bien fixé autour du col de l’utérus. La bague externe de l’utérus couvre l’entrée du vagin</li>" +
        "<li>Garder l’anneau externe avec la main pour faciliter la pénétration du pénis</li>" +
        "<li>Après la pénétration enlever la main</li>" +
        "<li>Après l’acte, retirer le préservatif avec délicatesse.</li></ul>"
      },
      {
        "question": "Peut-on attraper le virus du VIH/Sida par la sodomie?",
        "answer": " Oui; car tout rapport sexuel non protégé favorise la contamination aux IST et VIH/Sida."
      },
      {
        "question": "Quelles sont les conséquences des piercings et des tatouages?",
        "answer": "Au cas où ces objets coupant sont souillés, le risque est de contracter les IST et le VIH/sida."
      },
      {
        "question": "Le préservatif s’est éclaté en plein acte sexuel; que doit-elle faire?",
        "answer": "Ne pas paniquer, aller dans un centre de santé pour faire son test de dépistage de sida et des IST."
      },
      {
        "question": "Comment reconnait- on quelqu’un qui a le SIDA?",
        "answer": "Il n’a pas de signe pour reconnaître une PVVIH. Seul le test de dépistage permet de le savoir. C’est à la phase terminale de la maladie qu’on peut noter quelques signes tels  que : la toux, l’amaigrissement, l’apparition des boutons sur le corps et le visage, la diarrhée. L’organisme devient faible et n’arrive plus à se défendre et la personne tombe régulièrement malade. Ces signes peuvent aussi manifester pour d’autres maladies."
      },
      {
        "question": "Quels sont les précautions à prendre pour vivre avec une PVVIH?",
        "answer": "<p>Il faut éviter les rapports sexuels non protégés et le partage des objets coupants et tranchants par contre voici une liste d’attitudes qui ne transmet pas le virus du sida :</p>" +
                  "<ul><li>Serrer la main à une personne séropositive</li>" +
                  "<li>Partager les habits et les toilettes</li>" +
                  "<li>S’asseoir côte à côte</li>" +
                  "<li>Partager le même lieu de travail</li>" +
                  "<li>La toux et les éternuements</li>" +
                  "<li>La Piqûre d’insectes (moustiques)</li>" +
                  "<li>Les contacts au travail ou à l’école</li>" +
                  "<li>Les embrassades</li>" +
                  "<li>L’utilisation de toilette</li>" +
                  "<li>L’eau ou les aliments</li>" +
                  "<li>Manger ensemble</li>" +
                  "<li>Les piscines</li>" +
                  "<li>Les bancs publics</li></ul>"
      },
      {
        "question": "Quels sont les causes, les symptômes et les modes de contamination des hépatites?",
        "answer": "<p>Les hépatites virales, nommées A, B, C, D ou E, sont les plus fréquentes. </p>" +
                  "<p>L’hépatite A : est causée par le virus A. La contamination se fait par la voie digestive, notamment par les aliments et l’eau contaminés par les matières fécales ainsi que par la consommation de fruits de mer. Le virus de l'hépatite A s'attrape par de l'eau, des jus ou des aliments insuffisamment cuits (salades, fruits non pelés, fruits de mer, glaçons) dans des conditions d'hygiène insuffisantes.</p>" +
                  "<p>Symptômes pseudo-grippaux : fièvre, douleurs articulaires et musculaires une très grande fatigue</p>" +
                  "<p>L’hépatite B : est causée par le virus B, transmis par voie sanguine (transfusion sanguine de sang contaminé ou lors d’usage de seringues contaminées chez les toxicomanes notamment), sexuelle et salivaire. en général, seules 10 à 25 % des hépatites B sont symptomatiques. Les symptômes sont équivalents à ceux présents dans l’hépatite A.</p>" +
                  "<p>L’hépatite C : est causée par le virus C, transmis par voie sanguine (transfusions, hémophiles, toxicomanes, hémodialysés, transmission sexuelle ou par voie placentaire possible, mais rare). En général, seules 5 à 10% des hépatites C aigües sont symptomatiques. Les symptômes sont équivalents à ceux présents dans l’hépatite A.</p>" +
                  "<p>L’hépatite D : est causée par le virus D, transmis par voie sanguine ou sexuelle. La présence du virus de l’hépatite B est nécessaire pour que l’hépatite D puisse se développer. Il peut s’agir d’une co-infection (la personne s’infecte simultanément avec le virus B et le virus D) ou d’une surinfection (la personne est déjà porteuse du virus B et s’infecte avec le virus D, d’une manière tardive). L’incubation est de 45 à 180 jours. Cette forme d’hépatite touche de manière presque exclusive, les toxicomanes.</p>" +
                  "<p>L’hépatite E : est causée par le virus E, transmis par voie orofécale. Elle est souvent aiguë et bénigne sans forme chronique. Chez l’homme, elle se manifeste par une hépatite souvent asymptomatique et habituellement bénigne (grande fatigue, signes digestifs, jaunisse et parfois de la fièvre). Des formes graves peuvent être observées chez les femmes enceintes, les personnes immunodéprimées et les personnes présentant déjà une maladie du foie.</p>"
      },
      {
        "question": "Quels sont les IST qui existe?",
        "answer": "Les Infections Sexuellement Transmissibles (IST) sont des Maladies que l’on attrape au cours d’un rapport sexuel non protégé avec une personne infectée. Les IST les plus courantes sont : (Gonococcie = Blennorragie = chaude pisse – Syphilis - Chancre mou - Herpès génital - Candidose Chlamydiae trichomonas…)"
      },
      {
        "question": "Quels sont les symptômes des IST?",
        "answer": "<p>Il existe des IST asymptomatiques (sans signe) et symptomatiques. Les manifestations les plus courantes sont :</p>" +
                  "<ul><li>Les plaies et boutons sur le sexe de l’homme ou de la femme<li>" +
                  "<li>Les écoulements ou pertes de liquide anormal du sexe de l’homme et de la femme (Les pertes peuvent être abondantes, avoir une mauvaise odeur, être jaunes, vertes, ou blanchâtres; elles peuvent s’accompagner de démangeaison des organes génitaux)<li>" +
                  "<li>Douleur ou brûlure en urinant chez l’homme<li>" +
                  "<li>Démangeaison au niveau du pénis<li>" +
                  "<li>Douleurs au bas ventre chez la femme<li>" +
                  "<li>Douleurs lors des rapports sexuels chez la femme<li>" +
                  "<li>Les crêtes de coq ou condylomes<li>" +
                  "<li>Les gonflements du scrotum<li>" +
                  "<li>Augmentation du volume des testicules<li></ul>"
      }
    ]
  },
  {
    "id": 3,
    "title": "GENRE ET VIOLENCES",
    "questions": [
      {
        "question": "Quelle est la différence entre Sexe et Genre ?",
        "answer": "Les 2 thèmes évoquent la même chose mais différemment. Le sexe est une distinction biologique alors que le genre est une construction sociale et culturelle."
      },
      {
        "question": "Un ami est victime de violence. Comment l’aider ?",
        "answer": "<p>La première chose importante est de continuer à discuter avec lui et de continuer à lui offrir ton soutien.</p>" +
                  "<p>Quand un jeune est victime de harcèlement, il est très précieux et aidant de se sentir soutenu et aimé par d’autres. Ton soutien n’empêchera pas le harcèlement dont il est victime, mais cela l’aidera certainement beaucoup.</p>" +
                  "<p>Il est aussi important que tu lui dises que ce qu’il vit n’est pas juste et inacceptable. Il arrive que les victimes pensent mériter ce qu’elles subissent, c’est pourquoi il est précieux de leur répéter que ce qu’elles vivent n’est pas correct et qu’elles ont le droit de se rendre à l’école sans y être agressées.</p>" +
                  "<p>Il serait bien aussi que tu l’encourages à parler de ce qu’il vit à un adulte de confiance. Très souvent les jeunes victimes de harcèlement n’osent pas en parler de peur que cela empire les choses. Mais dans la grande majorité des situations, en parler aide beaucoup.</p>" +
                  "<p>Si ce qu’il vit se passe dans le cadre scolaire, il serait bien qu’il puisse en parler à un adulte de l’école. Au Bénin, il y a les CPS (Centres de Promotion Sociale) ou d’autres adultes ressources comme les psychologues ou infirmières scolaires, qui devraient pouvoir lui venir en aide. Encore une fois, l’intervention d’un adulte dénoue très souvent ce type de situation.</p>" +
                  "Il peut aussi bien sûr en parler à ses parents. Très souvent les parents ne savent pas que leurs enfants vivent des choses difficiles à l’école. Leur parler permet d’être soutenu et aidé à la maison."
      },
      {
        "question": "DIFFERENTES FORMES DE VIOLENCES FAITES AUX FEMMES",
        "answer": "<p>Ces différentes formes de violence sont généralement classées de la manière suivante :<p>" +
                  "<p>Les violences physiques : Les violences physiques se résument pour l’essentiel à des atteintes corporelles exercées le plus souvent sur les filles et les femmes. Cette forme de violence est la plus visible et se traduisent par : des gifles, coup de poings et de pieds, bastonnades, morsures, brûlures (avec le feu, l’acide), l’arrachement des cheveux, des strangulations, etc…<p>" +
                  "<p>La violence morale ou psychologique: le fait de soumettre toute personne à des agissements ou paroles répétés ayant pour objet ou pour effet une dégradation des conditions de vie susceptibles de porter atteinte à ses droits et à sa dignité, d’altérer sa santé physique ou mentale ou de compromettre ses projets ou son avenir. C’est aussi un acte ou une négligence portant préjudice à la stabilité psychologique, un abandon, une inattention réitérée, une jalousie excessive, des insultes et humiliations, une dévalorisation, une marginalisation, un manque d’affection, une indifférence, l’infidélité, des comparaisons destructives, le rejet, la manque de détermination , des menaces…  autant de situations pouvant amener la victime à sombrer dans la dépression, à s’isoler, à perdre l’estime de soi, voire à se suicider ;<p>" +
                  "<p>Les violences coutumières : ce sont des pratiques traditionnelles préjudiciables aux femmes, actes tirés des usages et coutumes qui portent atteinte à la femme. Il s’agit notamment :<p>" +
                  "<ul><li>mariage forcé,<li>" +
                  "<li>mutilations génitales féminines,<li>" +
                  "<li>des interdits alimentaires en cas de grossesse ou d’accouchement,<li>" +
                  "<li>du gavage qui consiste à nourrir exagérément les filles mineures en vue de  les rendre physiquement aptes au mariage,<li>" +
                  "<li>des rites de veuvage dégradants,<li>" +
                  "<li>le mariage par enlèvement,<li>" +
                  "<li>le lévirat qui consiste à donner en mariage la veuve au frère du défunt,<li>" +
                  "<li>des atteintes à la liberté de mouvement de la femme, lors de la sortie de certaines divinités,<li>" +
                  "<li>des pressions sur la femme par le biais des enfants.<li></ul>" +
                  "<p>La violence économique : privation de moyens ou de biens essentiels, contrôle ou spoliation, parfois même lorsque la femme a une activité rémunérée.<p>" +
                  "<p>Les violences sexuelles : relations sexuelles sans consentement et/ou sous la contrainte.<p>"
      },
      {
        "question": "TYPOLOGIE DES VIOLENCES SEXUELLES A L’EGARD DES FEMMES ET DES FILLES",
        "answer": "<p>Les violences sexuelles sont multiformes.  Au nombre de celles couramment rencontrées on peut citer:<p>" +
                  "<ul><li>Le harcèlement sexuel : «c’est le fait de soumettre autrui à des attaques incessantes en usant d’ordre, de menaces ou de contraintes, dans le but d’obtenir des faveurs de nature sexuelle par une personne abusant de l’autorité (…) »<li>" +
                  "<li>Le viol est un acte de violence par lequel un homme (violeur) impose des relations sexuelles avec pénétration à une personne contre sa volonté. Il se défini également comme tout acte de pénétration vaginale, anale ou buccale par le sexe d’autrui ou la pénétration vaginale ou anale par un quelconque objet sans le consentement intelligent et volontaire de la personne pénétrée.  Notons que le consentement n’est pas valable chez les femmes mineures de moins de seize (16) ans. La personne pénétrée n’est pas obligée de se battre contre son agresseur. Le fait d’être marié à la personne pénétrée n’est pas une excuse au crime de viol ;<li>" +
                  "<li>Incitation des mineurs à la débauche : le fait d’agir sur des mineurs en vue de satisfaire les passions d’autrui ou en tout cas comme agent intermédiaire de corruption et de la débauche.<li>" +
                  "<li>Avortement : le fait d’employer des moyens ou substances destinés à provoquer clandestinement l’expulsion prématurée du fœtus ou, plus généralement, l’interruption artificielle et forcée de la grossesse chez la femme/ fille.<li>" +
                  "<li>Proxénétisme : l’activité de celui ou celle qui favorise la débauche d’autrui en servant d’intermédiaire, tirant de ce fait bénéfice des fruits de cette activité ;<li>" +
                  "<li>Les rapports sexuels incestueux : quand un père ou un frère a des rapports sexuelles avec sa fille ou sa sœur. C’est un rapport sexuel perpétré sur une personne avec qui on a des liens de parenté jusqu’au troisième degré inclus ;<li>" +
                  "<li>L’attentat à la pudeur : Ce sont des actes contraires aux bonnes mœurs (exhibitions sexuelles) commis par une personne sur un tiers, généralement un mineur non émancipé.<li>" +
                  "<li>Abus sexuel sur les adolescents : quand une femme adulte oblige un adolescent à avoir des rapports sexuels ou lorsqu’un homme adulte contraint un adolescent à avoir des rapports sexuels (la sodomie).<li>" +
                  "<li>La séquestration consiste à saisir une personne, de l’empêcher de continuer sa route, de le priver de la faculté d’aller et de venir à son gré, ou de l’enfermer dans un lieu quelconque, qu’il soit public ou privé, dans le but d’obtenir d’elle, des faveurs sexuelles.<li>" +
                  "<li>La pédophilie désigne une attirance érotique ou préférence sexuelle d’un adulte envers les enfants ou adolescents. Plus spécifiquement l’adulte qui fait des attouchements ou entretient des rapports sexuels avec un(e) mineur(e).<li>" +
                  "<li>La traite et la pornographie impliquant les mineurs<li></ul>"
      },
      {
        "question": "Quelles sont les punitions prévues par la loi ?",
        "answer": "<p>Les punitions prévues par la loi sont :<p>" +
                  "<ul><li>Un emprisonnement de 5 à 10 ans lorsque pour la gravité de  la situation (Art 30)<li>" +
                  "<li>Un emprisonnement d’1 an à 3 ans et d’une amende de 500 000 à 2 000 000 FCFA pour toute personne impliquée dans un mariage forcé (Art 31)<li>" +
                  "<li>Une amende de 1 000 000 FCFA pour les auteurs de violences psychologiques (Art 32)<li>" +
                  "<li>Une amende de 500 000 à 2 000 000 FCFA pour les auteurs de violences économiques  (Art 33 et CPF)<li>" +
                  "<li>Un emprisonnement d’1 an à 5 ans et une amende de 1 000 000 à 10 000 000 FCFA pour les auteurs de prostitution forcée et de stérilisation forcée (Art 34 et 35)<li>" +
                  "<li>Un emprisonnement d’au moins 10 ans et une amende d’au moins 25 000 000 FCFA pour les auteurs de zoophilie (Art 36)<li></ul>"
      },
      {
        "question": "Qu’est –ce que la mutilation génitale ?",
        "answer": "<p>Selon la Loi n° 2011-26  du 27 septembre 2011 portant prévention et répression des violences faites aux femmes, on définit  Mutilation génitale féminine  comme  toutes les interventions incluant l’ablation partielle ou totale des organes génitaux ou externes de la femme ou la lésion des organes génitaux féminins pratiqués pour des raisons culturelles ou religieuses ou pour toute autre raison non thérapeutique ;<p>"
      },
      {
        "question": "Quels sont les types de mutilations génitales féminines ?",
        "answer": "<p>Le type 1 ou clitoridectomie est l’ablation partielle ou totale du clitoris et/ou du capuchon du clitoris.<p>" +
                  "<p>Le type 2 ou excision concerne l’ablation partielle ou totale du clitoris et des petites lèvres, avec ou sans excision des grandes lèvres.<p>" +
                  "<p>Le type 3 ou infibulation est le rétrécissement de l’orifice vaginal avec recouvrement par l’ablation et l’accolement des petites lèvres et/ou des grandes lèvres, avec ou sans excision du clitoris.<p>" +
                  "<p>Le type 4 comprend toutes les autres interventions nocives pratiquées sur les organes génitaux féminins à des fins non thérapeutiques, comme la ponction, le percement, l’incision, la scarification et la cautérisation.<p>"
      },
      {
        "question": "Quelles sont les conséquences de la mutilation génitale ?",
        "answer": "<p>Les mutilations génitales ont un impact considérable sur la santé des femmes qui les subissent, tant sur le plan physique que psychologique et sexuel.<p>" +
                  "<p>Les complications immédiates (au moment de l’excision ou dans les heures qui suivent) sont : la douleur aiguë, l’hémorragie, l’infection, des lésions accidentelles des organes voisins, des fractures, ….Ces complications peuvent, dans certains cas, entraîner le décès.<p>" +
                  "<p>Les complications à moyen et long termes, plus fréquentes après infibulation, comprennent des douleurs chroniques, des cicatrices hypertrophiques, des kystes, des troubles urinaires chroniques, des troubles menstruels, l’infertilité.<p>" +
                  "<p>L’incidence de l’infibulation sur le déroulement de l’accouchement et le pronostic vital des nouveau-nés est également non négligeable.<p>" +
                  "<p>Des complications psychologiques sont observées ; il est question de névrose, d’anxiété, de dépression, de repli sur soi, du syndrome de stress post-traumatique.<p>" +
                  "<p>Les complications sexologiques comportent des douleurs lors des rapports sexuels, la diminution du désir, l’anorgasmie. L’absence de plaisir lié à la sexualité engendre des difficultés conjugales.<p>"
      },
      {
        "question": "Quelles sont les dispositions de répression ?",
        "answer": "<p>Article 30 : Pour toute infraction pénale qui réprime des violences physiques ou sexuelles, le fait que la victime et l’auteur jouissent d’une relation domestique, définie à l’article 3 de la présente loi, sera retenu comme circonstance aggravante. La peine maximale en matière délictuelle est aggravée par cinq (05) ans d’emprisonnement et celle en matière criminelle est aggravée d’au moins dix (10) ans.<p>" +
                  "<p>Article 37 : Toutes les pratiques traditionnelles préjudiciables aux femmes sont réprimées par les infractions pénales de droit commun. Tous les autres faits de violences non spécifiquement prévus par la présente loi sont punis conformément à la législation en vigueur.<p>"
      },
      {
        "question": "Qu’est-ce que le Harcèlement sexuel ?",
        "answer": "<p>Le harcèlement sexuel : «c’est le fait de soumettre autrui à des attaques incessantes en usant d’ordre, de menaces ou de contraintes, dans le but d’obtenir des faveurs de nature sexuelle par une personne abusant de l’autorité.<p>" +
                  "<p>Exemple : Cas des jeunes filles harcelées en milieu scolaire et en apprentissage.<p>"
      }
    ]
  },
  {
    "id": 4,
    "title": "SANTE DE LA REPRODUCTION",
    "questions": [
      {
        "question": "Qu’est-ce que la Santé Sexuelle ?",
        "answer": "La santé sexuelle est un état de bien-être physique, émotionnel, mental et social relié à la sexualité. Elle ne saurait être réduite à l’absence de maladies, de dysfonctions ou d’infirmités. La santé sexuelle exige une approche positive et respectueuse de la sexualité et des relations sexuelles, ainsi que la possibilité d’avoir des expériences plaisantes et sécuritaires sans discrimination et violence et coercition."
      },
      {
        "question": "Quels sont les risques d’avoir plusieurs partenaires sexuels ?",
        "answer": "Avoir des rapports sexuels non protégés avec beaucoup de partenaires sexuels occasionnels, simultanés ou successifs t’expose au risque de contracter une IST, le VIH ou une grossesse non désirée."
      },
      {
        "question": "Mon copain me demande d’avoir des rapports sexuels. Que faire ?",
        "answer": "Ne laisse personne t’exiger de faire quelque chose si tu ne le veux pas, surtout en matière de sexualité. Ne cède pas à des avances alors que tu n’es pas prête à en assumer et supporter les conséquences. Discute avec toi copain en lui expliquant tu n’es pas prête et que vous pouvez attendre. S’il t’aime vraiment, il comprendra et respectera ta décision."
      },
      {
        "question": "Qu’est-ce que la puberté ?",
        "answer": "C’est le passage de l’enfance à l’adolescence. Elle correspond à d’importantes transformations physiques, psychologiques et physiologiques que chacun vit à son propre rythme et qui provoquent de nombreuses émotions. Le corps devient peu à peu celui d’un adulte. Les organes sexuels, par exemple, se développent pour permettre la reproduction."
      },
      {
        "question": "La puberté, comment ça se passe chez les garçons ?",
        "answer": "Tout le corps subit de multiples transformations. La peau devient moins lisse et des boutons d’acné peuvent apparaître ; les poils se développent sur les jambes et les bras, aux aisselles et sur le pubis, et parfois sur le torse et le dos ; un duvet de poil recouvre la lèvre supérieure puis pousse sur le menton jusqu’à se développer sur les joues ; la musculature augmente progressivement ; avec la croissance du larynx et des cordes vocales, la voix devient de plus en plus grave ; le pénis commence à se développer ; les bourses (scrotum) qui contiennent les testicules deviennent plus foncées ; les testicules deviennent plus gros et commencent à fabriquer des spermatozoïdes ; les premières éjaculations apparaissent."
      },
      {
        "question": "La puberté, comment ça se passe chez les filles ?",
        "answer": "La peau devient moins lisse, des boutons d’acné peuvent apparaître ; les poils se développent sur les jambes et les bras, aux aisselles et sur le pubis, parfois autour des tétons et dans le bas du dos ; il arrive que le duvet de poil qui recouvre la lèvre supérieure fonce et s’épaississe ; les seins commencent à gonfler ; les hanches s’arrondissent et la taille se marque ; la vulve commence à se développer, petites et grandes lèvres changent d’aspect et de couleur ; dans les ovaires, les ovules commencent à mûrir et l’appareil génital se transforme pour se préparer à une éventuelle maternité ; les premières règles apparaissent et les pertes vaginales deviennent plus fréquentes."
      },
      {
        "question": "Saigne-t-on obligatoirement lors du 1er rapport sexuel ?",
        "answer": "La présence ou non de saignements lors de cet événement n'est en aucun cas une preuve de virginité. Chez certaines femmes on peut avoir des saignements et Chez d’autres aucun saignement et ceci est dû aux activités sportives- toilettes intimes ou autres……qui fragilise l’hymen."
      },
      {
        "question": "Est-ce qu'une fille vierge peut tomber enceinte?",
        "answer": "Une fille vierge peut avoir une grossesse si elle se fait introduire les spermatozoïdes dans les voies génitales par un moyen autre que la pénétration (si  pendant les préludes les cellules s’échappent dans le liquide séminal et qu’il a des manipulations ; le partenaire ou même la fille  en introduisant les doigts dans le vagin par exemple,  peut transporter les spermatozoïdes vers les voies génitales.)"
      },
      {
        "question": "Comment déterminer le  jour d'ovulation chez la femme?",
        "answer": "<ul><li>Suivre le cycle pendant 3 ou 6 mois (noter les différentes dates de vos menstrues ou règles durant cette période d’observation. Exemple : 26/01, 27/02, 30/03, 12/04, 15/05, 26/06,<li>" +
                  "<li>Ensuite rendez-vous chez une sage-Femme dans un hôpital ou dans un Centre Jeune Amour et Vie. L’agent de santé va vous aider à calculer votre cycle mensuel et à déterminer votre période de fécondité et votre date d’ovulation.<li></ul>"
      },
      {
        "question": "Les effets de la masturbation sur l'organisme?",
        "answer": "Permet d'éviter les IST, les grossesses non désirées mais Psychologiquement, on se dit qu'on n'a pas besoin d'un homme ou d'une femme, on développe l'égoïsme."
      },
      {
        "question": "Quelles sont les étapes d'un rapport sexuel?",
        "answer": "Caresses, préliminaires érection/la femme bien mouillée, port de condom (pour éviter les IST et les grossesses non désirées), pénétration, frottement, jouissance, orgasme, relâchement (conduire le rapport de telle façon que la femme jouisse d'abord et l'homme après), retrait de l'homme avant que le pénis ne devienne flasque; insister sur la protection des rapports sexuels. Pour finir, le référer vers le Centre Jeunes amour & vie + de sa localité."
      },
      {
        "question": "Comment draguer une fille?",
        "answer": "Observer la fille pour mieux connaître cette fille. Identifier ses besoins, ses attentes, ses aspirations et communiquer avec elle dans ce sens. Aller vers la fille, demander ses coordonnées, lui donner rendez-vous pour mieux faire connaissance; par la suite, insister sur les rapports sexuels protégés pour éviter les IST et les grossesses non désirées. Pour finir le référer vers le Centre Jeunes amour & vie + de sa localité."
      },
      {
        "question": "Comment savoir que votre ami vous aime?",
        "answer": "Ce sont de petits gestes qui vont vous permettre de savoir que votre ami vous aime; il n'y a pas de règles en la matière car c'est à la femme même d'observer son ami pour voir comment il est attaché à elle, comment il est attentionné par rapport à ces préoccupations."
      },
      {
        "question": "Quelles sont les conséquences de l'abstinence à longue durée?",
        "answer": "Pas de conséquences. Elle demeure le meilleur moyen de prévention contre les IST et le VIH/Sida."
      },
      {
        "question": "Une femme en âge de procréer qui ne fait pas des rapports sexuels pendant longtemps va-t-elle tomber malade?",
        "answer": "Non! Elle ne court aucun risque."
      },
      {
        "question": "Je ne sens rien à chaque fois que je fais l'amour avec n'importe quel homme, pourquoi?",
        "answer": "Votre homme doit chercher à connaître vos points de sensibilité."
      },
      {
        "question": "Est-ce que le bicarbonate est conseillé pour les toilettes intimes?",
        "answer": "Oui. Mais celle vendue à la pharmacie. Mais sur prescription médicale"
      },
      {
        "question": "Comment reconnaître une fille vierge?",
        "answer": "A l’ œil nu on ne peut pas reconnaitre une fille vierge c’est une sage-femme ou un gynécologue qui peut à travers un examen clinique confirmé la virginité d’une fille."
      },
      {
        "question": "Pourquoi le sperme ressort-il après les rapports sexuels?",
        "answer": "Ce phénomène est tout à fait naturel car le sperme est un liquide et c’est normal qu’il coule. Imaginez une bouteille de miel retournée vers le bas le miel coule et sort de la bouteille c’est pareil pour le sperme."
      }
    ]
  }
]

"use strict";

var nsAppium = require("nativescript-dev-appium");
var chai = require("chai");
var assert = chai.assert;

describe("App tests", function () {
    
    var defaultWaitTime = 5000;
    var driver;

    before(async function () {
      driver = await nsAppium.createDriver();
    });

    after(async function () {
        await driver.quit();
        console.log("Quit driver!");
    });

    afterEach(async function () {
        if (this.currentTest.state === "failed") {
            await driver.logScreenshot(this.currentTest.title);
        }
    });

    it("should see start button", async function () {
        var startBtn = await driver.findElementByAccessibilityId("com.etrilabs.mvmc/slides:start_btn");
        assert.equal(await startBtn.text(), "Démarrer !");
        await startBtn.click();
      });
     
});
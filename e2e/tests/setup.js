"use strict";
var nsAppium = require("nativescript-dev-appium");

before("start server", async function () {
    await nsAppium.startServer();
});

after("stop server", async function () {
    await nsAppium.stopServer();
});